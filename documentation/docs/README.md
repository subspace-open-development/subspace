# About Subspace

## Introduction

Subspace enables smart group decision-making in software development. It is a workflow that sits on top of git.

There are two main components:
* **Meritocracy** via voting and reputation
* **Task hierarchy** and branch management

#### Meritocracy

Many actions that a developer takes in Subspace change their reputation score. These scores work more like skill ratings than brownie points, and are primarily driven by group consensus. If you contribute decent code (and it gets approved by the group) then your reputation goes up. If you contribute poorly written code (and it gets rejected by the group) then your reputation goes down. The higher your reputation, the more influence you have in group decision-making.

#### Task hierarchy

Raise your hand if you think one layer of Jira sub-tasks isn't quite cutting it. We believe that our workflows should reflect our thought processes, and in Subspace you can create arbitrarily deep nested tasks. Every task created in the task tree automatically generates a git branch. As tasks get completed, the branches get merged up the tree. All child branches get automatically rebased with changes merged into a parent task.

<img src="./task_branch_process.png">

## Use Cases

  * [**Enterprise**](/#enterprise)
  * [**Open-source**](/#open-source)
  * [**Hackathons**](/#hackathons)

### Enterprise

#### Coordination
As software teams grow, more and more energy is spent on coordination among developers. Most of you are probably familiar with the 'Mythical Man Month', which more or less states that project velocity slows down as more developers are added. The Subspace protocol enables instant development decisions, no matter how large the team.

#### Visibility
It takes time to tell the good developers from the bad ones. It takes even more time to tell the outstanding developers from the good ones. With Subspace, skill and productivity metrics are natural outcomes of the development process. These metrics aren't based on line counting or computer algorithms, but are instead based on peer review.

If you're a team lead you could even give your developers a bonus based on their Subspace ranking, to really align incentives.

#### Amazing code
Subspace gives more influence to better coders. We've all had our time wasted listening to developers argue the finer points of [insert any technology here]. Subspace turns every decision into a seamless consensus-driven approval process. The Subspace protocol also helps to ensure that tech debt doesn't make it's way into your code base.

### Open-source

#### Lift the burden
Open-source projects are primarily built and maintained by a very very small number of developers. They work tirelessly, and with a sense of pride. Subspace enables anyone to contribute to an open-source project, while still ensuring that the code goes through a rigorous review process from the community. More developers will enjoy contributing to open-source if they get credit by increasing their Subspace ranking. Open-source? Try open-development.

#### Governance
The direction of many open-source projects can have implications which go beyond the technology itself. Cryptocurrency projects (for example) are governed by loose organizations, whose members try to balance various competing interests (such as private financial interests vs. the public good). Subspace can provide a layer of software governance and give control directly to the community.

### Hackathons
Subspace enables any number of developers to self-organize. It works really well for large-scale remote hackathons, and we look forward to seeing what the community will build next. See project settings for hackathon-specific configurations.
