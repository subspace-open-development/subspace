# Screenshots

### Login
<img src="./login.png" width="583" height="383" border="1">

### Tasks
<img src="./tasks.png" width="1173" height="624">

### Contribution
<img src="./contribution.png" width="1307" height="1253">

### Submit Contribution
<img src="./submit_contribution.png" width="1314" height="883">

### Suggestion
<img src="./suggestion.png" width="1090" height="713" border="1">

### Submit Suggestion
<img src="./submit_suggestion.png" width="1110" height="855">

### Branches
<img src="./branches.png" width="1175" height="676">

### Settings
<img src="./settings.png" width="1176" height="658">
