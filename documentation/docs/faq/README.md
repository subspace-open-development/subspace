# FAQs

#### Do I need to have a GitLab account to use Subspace?
Yes.

#### Something isn't working correctly.
Please make sure you have accepted GitLab's latest TOS, here: https://gitlab.com/-/users/terms

#### Wouldn't it be better to incorporate Subspace into an existing code hosting platform (i.e. Gitlab / GitHub)?
Yes, it would be. Though this was far quicker from a development standpoint, and in the future it will enable Subspace to work with any code hosting platform. We admit it is a bit clunky jumping back and forth between Subspace and GitLab, though we think the advantages are well worth it.

#### When will you support GitHub?
We recognize that GitHub is more popular, though GitLab's API (and their entire ethos..) is much better. GitHub support is on our medium-term roadmap, though priority will be given to improving our product before expanding our audience.

#### Would you consider turning Subspace into a dApp?
Yes. But we think the tooling to make that a simple process isn't quite there yet. We are excited by the progress being made with Ethereum Swarm.
