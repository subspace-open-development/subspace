# Getting Started

#### Gitlab account
Subspace uses Gitlab's API, and Gitlab is currently the only code-hosting back-end we support. To use Subspace you will need a Gitlab account, and the repository you want to work with must be hosted on Gitlab. We currently do not support self-hosted Gitlab instances. Github is on our near-term roadmap.

## Create a New Project

#### Repository onboarding

To onboard a project in Subspace, you need a Gitlab account with `maintainer` or higher access privileges. This is the account that will interface with your project through Gitlab's API, and will make merges/rebases on behalf of the community after review and consensus. Some users choose to create a service account specifically for this purpose.

#### Setting up the Task Tree

The task tree should reflect your project's standard workflow. Typically this means that there will be a `master` task at the very top of the tree, followed by a `develop` task just beneath it. So far these tasks are just mirroring your existing branches. To get started assigning these existing branches to the task tree, please visit the `branches` section of the project. From here, tasks can be created with `develop` (or similar) as their parent task.

## Join an Existing Project
If a repository has already been onboarded, and you are already a member of the project, you can join it in the `Onboarding` tab in the Subspace application. Open-source projects can be viewed in the `Browse Projects` tab, and you may request membership. Depending on the settings configured by the project owner, this join may be automatic or you may need to wait for approval.

## Create a Task
Creating a task will create a new protected branch in the repository. You will choose the task parent, and this determines which branch is copied in the new branch creation. When creating a branch there are a few configurations available.

* `auto-rebase children` - Whenever this branch is changed, children tasks will be automatically rebased with new code changes. This can cascade down the tree. [default `true`]
* `auto-merge parent` - Whenever this branch is changed, the changes will be automatically merged up to the parent task. To avoid merge conflicts it uses the `--ours` argument. We believe that the time saved in taking an opinionated stance on merging will make up for the occasional mistake and fix-commit. This can cascade up the tree (and cause potential rebasing of children task branches). [default `true`]

## Submit a Contribution
Similar to a `merge request` or `pull request`.

You should have pushed your own working branch to the project on Gitlab, complete with your commits.

> **Note:** There are many terrific features within Gitlab, and we welcome you to continue using them as you would. In building Subspace we did not wish to re-invent the proverbial code-hosting wheel, but instead chose our favorite hosting site and are working to build vertically. We admit that navigating between Subspace and Gitlab can be clunky at times, and we hope to make this aspect of the workflow more seamless in the future.

From there you can navigate to the `contributions` section of the project, and click `submit contribution`. Within the menu you can:

* select your branch
* select the task you'd like to contribute to
* write a description of your work

## Review a Contribution

There are two actions you can take when reviewing a contribution.

* Support (aka approval)
* Suggestion (aka feedback)

These are *not* mutually exclusive. If you think that a contribution is excellent, except for one or two things, then show your support and make a suggestion (just be sure to make the suggestion first, so that it doesn't accidentally get approved before you get the chance to give helpful feedback). Ideally all supporters will play active roles in shepherding the contribution across the finish line. From a reputation perspective: supporting a contribution puts yourself in the same boat as the original contributor (though the resulting reputation changes will be of a lower magnitude).

## Contribution Acceptance
The dynamics of contribution acceptance typically play out over the course of a few minutes (on the fastest configuration) to a week or more (on the slowest configuration), and the time directly depends on the amount of review activity. The threshold bar for acceptance is very high initially when the contribution is created, and then gets lowered exponentially as time goes on. At some point the amount of support and suggestions will push the contribution into either an `accepted` or `pending` state. If accepted, the contribution is automatically merged. Contributions in the pending state will need some changes (either more support or addressing reviewer suggestions). If a contribution is in the pending state for too long (usually a week, given no further commits) it will eventually be declined. The dynamics are discussed in greater detail in the following documentation.
