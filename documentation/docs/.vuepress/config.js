module.exports = {

  base: '/',
  dest: 'public',

  locales: {
    '/': {
      lang: 'en-US',
      title: 'Subspace Documentation',
      description: 'Subspace developer documentation',
    }
  },

  themeConfig: {
    docsDir: 'docs',
    docsBranch: 'master',
    search: false,
    displayAllHeaders: true,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        lastUpdated: 'Last Updated',
        // service worker is configured but will only register in production
        serviceWorker: {
          updatePopup: {
            message: 'New content is available.',
            buttonText: 'Refresh'
          }
        },
        nav: [
          { text: 'About', link: '/' },
          { text: 'Getting Started', link: '/getting-started/' },
          { text: 'Deep Dive', link: '/deep-dive/' },
          { text: 'Back to Subspace', link: 'https://app.subspace.net' }
        ],
        sidebar: {
          '/': [
              '',
              'getting-started/',
              'deep-dive/',
              'faq/',
              'screenshots/',
              'contact/'
          ]
        }
      }
    }
  },

  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-108905818-3'
      }
    ]
  ]
}
