import Model from './Model'

export default class Onboarding extends Model {
  resource() {
    return 'api/onboardRepo'
  }
}
