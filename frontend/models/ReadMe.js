import Model from './Model'

export default class ReadMe extends Model {
  resource() {
    return 'api/readMe'
  }
}
