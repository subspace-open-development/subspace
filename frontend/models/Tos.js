import Model from './Model'

export class Tos extends Model {
  resource() {
    return 'api/gitlabtos'
  }
}
