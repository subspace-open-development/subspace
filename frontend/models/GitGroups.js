import Model from './Model'

export default class GitGroups extends Model {
  resource() {
    return 'api/gitGroups'
  }
}
