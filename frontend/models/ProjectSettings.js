import Model from './Model'

export class ProjectSettings extends Model {
  resource() {
    return 'api/projectSettings'
  }
}
