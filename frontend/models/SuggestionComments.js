import Model from './Model'

export class NewSuggestionComment extends Model {
  resource() {
    return 'api/newSuggestionComment'
  }
}

export class EditSuggestionComment extends Model {
  resource() {
    return 'api/editSuggestionComment'
  }
}

export class SuggestionCommentList extends Model {
  resource() {
    return 'api/suggestionCommentList'
  }
}
