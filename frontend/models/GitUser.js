import Model from './Model'

export default class GitUser extends Model {
  resource() {
    return 'api/gitUser'
  }
}
