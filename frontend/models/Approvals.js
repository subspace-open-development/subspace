import Model from './Model'

export class Approval extends Model {
  resource() {
    return 'api/approvals'
  }
}
