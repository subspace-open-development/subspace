import Model from './Model'

export default class Comments extends Model {
  resource() {
    return 'api/comments'
  }
}
