import Model from './Model'

export default class GitRepoBranches extends Model {
  resource() {
    return 'api/gitRepoBranches'
  }
}
