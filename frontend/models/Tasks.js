import Model from './Model'

export class NewTask extends Model {
  resource() {
    return 'api/newTask'
  }
}

export class EditTask extends Model {
  resource() {
    return 'api/editTask'
  }
}

export class TaskDetails extends Model {
  resource() {
    return 'api/task'
  }
}

export class TaskList extends Model {
  resource() {
    return 'api/taskList'
  }
}

export class TaskTree extends Model {
  resource() {
    return 'api/taskTree'
  }
}
