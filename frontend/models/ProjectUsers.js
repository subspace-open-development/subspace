import Model from './Model'

export default class ProjectUsers extends Model {
  resource() {
    return 'api/projectUsers'
  }
}
