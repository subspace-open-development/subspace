import Model from './Model'

export default class SubspaceUser extends Model {
  resource() {
    return 'api/subspaceUser'
  }
}
