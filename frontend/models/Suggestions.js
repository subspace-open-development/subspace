import Model from './Model'

export class NewSuggestion extends Model {
  resource() {
    return 'api/newSuggestion'
  }
}

export class EditSuggestion extends Model {
  resource() {
    return 'api/editSuggestion'
  }
}

export class SuggestionDetails extends Model {
  resource() {
    return 'api/suggestion'
  }
}

export class SuggestionList extends Model {
  resource() {
    return 'api/suggestionList'
  }
}

export class SuggestionAction extends Model {
  resource() {
    return 'api/suggestionAction'
  }
}
