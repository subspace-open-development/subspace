import Model from './Model'

export default class Commits extends Model {
  resource() {
    return 'api/commits'
  }
}
