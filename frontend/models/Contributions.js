import Model from './Model'

export class NewContribution extends Model {
  resource() {
    return 'api/submitContribution'
  }
}

export class EditContribution extends Model {
  resource() {
    return 'api/editContribution'
  }
}

export class ContributionDetails extends Model {
  resource() {
    return 'api/contributions'
  }
}

export class ContributionList extends Model {
  resource() {
    return 'api/contributionList'
  }
}
