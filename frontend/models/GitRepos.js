import Model from './Model'

export default class GitRepos extends Model {
  resource() {
    return 'api/gitRepos'
  }
}
