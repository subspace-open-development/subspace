import Model from './Model'

export default class LatestComment extends Model {
  resource() {
    return 'api/comment'
  }
}
