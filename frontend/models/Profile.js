import Model from './Model'

export default class Profile extends Model {
  resource() {
    return 'api/user/profile'
  }
}


