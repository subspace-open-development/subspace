import Model from './Model'

export default class Projects extends Model {
  resource() {
    return 'api/projects'
  }
}

export class PublicProjects extends Model {
  resource() {
    return 'api/publicProjects'
  }
}

export class PublicProject extends Model {
  resource() {
    return 'api/publicProject'
  }
}

export class JoinPublicProject extends Model {
  resource() {
    return 'api/joinPublicProject'
  }
}
