/* eslint-disable */


const pkg = require('./package')

const prodConfigs = { // some variables might be over-written through gitlab CI/CD from env vars
  backendUrl: 'https://api.subspace.net',
  frontendUrl: 'https://app.subspace.net',
  gitlab: {
    url: 'https://gitlab.com',
    clientId: '5e47615165e781a46e4b3c02de5fe597515fd3926d2a440d15787fabe2576513'
  },
  stripe: {
    url: 'https://connect.stripe.com/oauth/authorize',
    clientId: 'ca_'
  },
  stackOverflow: {
    url: 'https://stackoverflow.com/oauth',
    clientId: '14559'
  }
}

const devConfigs = { // some variables might be over-written through gitlab CI/CD from env vars
  backendUrl: 'https://api.dev.subspace.net',
  frontendUrl: 'https://app.dev.subspace.net',
  gitlab: {
    url: 'https://gitlab.com',
    clientId: '5e47615165e781a46e4b3c02de5fe597515fd3926d2a440d15787fabe2576513'
  },
  stripe: {
    url: 'https://connect.stripe.com/oauth/authorize',
    clientId: 'ca_'
  },
  stackOverflow: {
    url: 'https://stackoverflow.com/oauth',
    clientId: '14559'
  }
}

const localConfigs = {
  backendUrl: 'http://localhost:9000',
  frontendUrl: 'http://localhost:3000',
  gitlab: {
    url: 'http://localhost:10080',
    clientId: '2f4cb74f53a0a2530e3514acf15a3132c44328fc00edbb10adc4bb71e801fc59'
  },
  stripe: {
    url: 'https://connect.stripe.com/oauth/authorize',
    clientId: 'ca_'
  },
  stackOverflow: {
    url: 'https://stackoverflow.com/oauth',
    clientId: '14559'
  }
}

console.log('process.env.NODE_ENV: ', process.env.NODE_ENV)

const local = (process.env.NODE_ENV !== 'production')
const configs = local ? localConfigs : devConfigs


export default {
  mode: 'universal',

  env: {
    WS_URL: process.env.WS_URL || 'ws://localhost:9000/ws',
    backendUrl: process.env.BACKEND_URL || configs.backendUrl,
    gitlab: {
      url: process.env.GITLAB_URL || configs.gitlab.url,
      clientId: process.env.GITLAB_CLIENT_ID || configs.gitlab.clientId
    },
    stripe: {
      url: process.env.STRIPE_URL || configs.stripe.url,
      clientId: process.env.STRIPE_CLIENT_ID || configs.stripe.clientId
    },
    stackOverflow: {
      // https://stackapps.com/users/49627/joec
      // 1) send user to URL with: client_id, scope, redirect_uri, state
      // 2) user is returned to the redirect_uri with the 'code' and 'state' as query parameters
      url: process.env.STACKOVERFLOW_URL || configs.stackOverflow.url,
      clientId:
        process.env.STACKOVERFLOW_CLIENT_ID ||
        configs.stackOverflow.clientId,
      scope: 'no_expiry', // "no_expiry", "read_inbox", "write_access", "private_info"
      redirectUri:
        (process.env.FRONTEND_URL || configs.frontendUrl) +
        '/account/callback/stackoverflow'
    }
  },

  router: {
    middleware: ['auth']
  },

  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    strategies: {
      gitlab: {
        _scheme: 'oauth2',
        authorization_endpoint:
          (process.env.GITLAB_URL || configs.gitlab.url) +
          '/oauth/authorize',
        userinfo_endpoint: undefined,
        // (process.env.GITLAB_URL || configs.gitlab.url) +
        // '/oauth/userinfo',
        scope: ['api'], //     , 'write_repository', 'openid', 'sudo'
        response_type: 'token',
        token_type: 'Bearer',
        redirect_uri: (process.env.FRONTEND_URL || configs.frontendUrl) + '/account/callback/gitlab',
        client_id:
          process.env.GITLAB_CLIENT_ID || configs.gitlab.clientId,
        token_key: 'access_token'
      }
      // stripe: {
      //   _scheme: 'oauth2',
      //   authorization_endpoint: 'https://connect.stripe.com/oauth/authorize',
      //   userinfo_endpoint: undefined,
      //   scope: ['read_write'],
      //   response_type: 'code',
      //   token_type: 'Bearer',
      //   redirect_uri: 'http://localhost:3000/auth/stripe/callback', //'http://localhost:3000/auth/callback',
      //   client_id: 'ca_',
      //   token_key: 'access_token'
      // }
    }
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'Subspace',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#63cdff' },
  /*
   ** Global CSS
   */
  css: [
    'normalize.css/normalize.css',
    'ant-design-vue/dist/antd.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/ant-design-vue',
    '~/plugins/vue-api-query',
    '~plugins/vuex',
    // { src: '~/plugins/vue-native-websocket', mode: 'client' },
    { src: '~plugins/vue-simplemde', mode: 'client' },
    '~plugins/vue-countdown'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    ['@nuxtjs/google-analytics', {
      id: 'UA-108905818-3'
    }]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    https: !local,
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    babel: {
      plugins: [
        '@babel/plugin-proposal-optional-chaining',
        '@babel/plugin-syntax-dynamic-import'
      ]
    },
    extend(config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push(
          {
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
          },
          {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /(node_modules)/
          }
        )
      }
    }
  },

  link: [
    {
      rel: 'icon',
      type: 'image/x-icon',
      href: 'favicon.ico',
    },
  ]
}
