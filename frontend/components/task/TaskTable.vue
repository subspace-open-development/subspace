<template>
  <div>
    <span style="display: inline-flex; margin-left: 15px; margin-bottom: 15px">
      <EditTaskDrawer :is-new="true" v-if="$auth.loggedIn" />
    </span>
    <a-row :gutter="16">
      <a-col :span="5">
        <span style="display: inline-flex; margin: 0px 0px 10px 15px">
          <a-button
            @click="handleExpandAll"
            type="secondary"
            size="small"
            style="margin: 5px"
          >
            Expand all
          </a-button>
          <a-button
            @click="handleCollapseAll"
            type="secondary"
            size="small"
            style="margin: 5px"
          >
            Collapse all
          </a-button>
        </span>
      </a-col>
      <a-col :span="6">
        <a-slider
          :min="1"
          :max="maxDepth"
          v-model="collapseLevel"
          @change="handleLevelChange"
        />
      </a-col>
      <a-col :span="2">
        <a-input-number
          :min="1"
          :max="maxDepth"
          v-model="collapseLevel"
          @change="handleLevelChange"
          style="margin-left: 16px"
        />
      </a-col>
    </a-row>
    <a-table
      :data-source="getTaskTree"
      :pagination="false"
      @expand="(expanded, record) => handleRowExpand(record)"
      :expandedRowKeys="expandedRowKeys"
    >
      <a-table-column
        key="iid"
        title="Task #"
        data-index="task.iid"
        width="25%"
      >
        <template slot-scope="text">
          <span>
            <router-link :to="getTasksUrl({ taskIid: text })">
              {{ text }}
            </router-link>
          </span>
        </template>
      </a-table-column>
      <a-table-column key="title" title="Title" data-index="task">
        <template slot-scope="text">
          <a-popover :title="text.title" placement="rightTop">
            <template slot="content">
              <MarkdownPreview :content="JSON.parse(text.description)" />
            </template>
            <span>{{ text.title }}</span>
          </a-popover>
        </template>
      </a-table-column>
      <!--      <a-table-column key="owner" title="Owner" data-index="owner" />-->
      <a-table-column key="branch" title="Branch" data-index="task.branch" />
      <!--      <a-table-column key="commit" title="Commit" data-index="task.commit" />-->
      <!--      <a-table-column-->
      <!--        key="acceptedContributions"-->
      <!--        title="Accepted Contributions"-->
      <!--        data-index="acceptedContributions"-->
      <!--      />-->
      <!--      <a-table-column-->
      <!--        key="pendingContributions"-->
      <!--        title="Pending Contributions"-->
      <!--        data-index="pendingContributions"-->
      <!--      />-->
    </a-table>
  </div>
</template>
<script>
import { mapActions, mapGetters, mapMutations } from 'vuex'
import MarkdownPreview from '../utils/MarkdownPreview'
import EditTaskDrawer from './EditTaskDrawer'

export default {
  name: 'TaskTable',
  components: {
    EditTaskDrawer,
    MarkdownPreview
  },
  data() {
    return {
      collapseLevel: 1,
      expandedRowKeys: [],
      maxDepth: 10
    }
  },
  computed: {
    ...mapGetters({
      getTaskList: 'tasks/getTaskList',
      getTaskTree: 'taskTree/getTaskTree',
      getTaskParentsArray: 'taskTree/getTaskParentsArray',
      getCurrentProjectId: 'projects/getCurrentProjectId'
    }),
    group() {
      return this.$route.params?.group
    },
    project() {
      return this.$route.params?.name
    },
    getHomeUrl() {
      return `/gitlab/${this.group}/${this.project}`
    }
  },
  watch: {
    getCurrentProjectId: {
      handler(newValue) {
        if (newValue !== undefined) {
          this.fetchTaskTree(this.getCurrentProjectId)
        }
      }
    },
    getTaskTree: {
      deep: true,
      handler(newValue) {
        if (newValue !== undefined) {
          this.maxDepth = this.getTreeDepth()
        }
      }
    }
  },
  mounted() {
    this.fetchTaskTree(this.getCurrentProjectId)
  },
  methods: {
    ...mapMutations({
      updateTaskTree: 'taskTree/updateTaskTree'
    }),
    ...mapActions({
      fetchTaskTree: 'taskTree/fetchTaskTree',
      fetchTaskList: 'tasks/fetchTaskList'
    }),
    getTasksUrl(taskIdObj) {
      return this.getHomeUrl + '/tasks/' + taskIdObj.taskIid
    },
    handleRowExpand(record) {
      this.expandedRowKeys = this.expandedRowKeys.includes(record.key)
        ? this.expandedRowKeys.filter((key) => key !== record.key)
        : [...this.expandedRowKeys, record.key]
    },
    handleCollapseAll() {
      this.expandedRowKeys = []
      this.collapseLevel = 1
    },
    handleExpandAll() {
      this.expandedRowKeys = this.getTaskList.map((obj) => obj.key)
      this.collapseLevel = this.maxDepth
    },
    handleLevelChange(level) {
      let objs = this.getTaskTree
      const keys = objs.map((obj) => obj.key)

      if (level > 1) {
        let i
        for (i = 0; i < level - 2; i++) {
          objs = objs.map((x) => x?.children).flat()
          objs.map((x) => {
            if (x?.key !== undefined) {
              keys.push(x?.key)
            }
          })
        }

        this.expandedRowKeys = keys
      } else {
        this.expandedRowKeys = []
      }
    },
    getTreeDepth() {
      let depth = 1
      let proceed = true

      let objs = this.getTaskTree

      while (proceed) {
        objs = objs
          .map((x) => x?.children)
          .flat()
          .filter((x) => x !== undefined)
        if (objs.length === 0) {
          proceed = false
        } else {
          depth = depth + 1
        }
      }

      return depth
    }
  }
}
</script>

<style>
.ant-table-pagination {
  margin: 16px !important;
}
.task-button {
  margin: 10px 5px 20px 5px;
}
.CodeMirror,
.CodeMirror-scroll {
  min-height: 50px;
}
.ant-table-thead > tr > th,
.ant-table-tbody > tr > td {
  padding-top: 9px;
  padding-bottom: 9px;
}
</style>
