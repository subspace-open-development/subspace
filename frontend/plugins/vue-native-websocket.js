import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

export default ({ store }, inject) => {
  Vue.use(VueNativeSock, process.env.WS_URL, {
    store,
    format: 'json',
    reconnection: true,
    reconnectionAttempts: Infinity,
    reconnectionDelay: 2000
  })
}
