import Vue from 'vue'
import VueSimplemde from 'vue-simplemde'
import 'simplemde/dist/simplemde.min.css'
import 'github-markdown-css'

Vue.component('vue-simplemde', VueSimplemde)
