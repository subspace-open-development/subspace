export const GITLAB = 'GITLAB'

export function getPostTokenUrl() {
  // return (
  //   process.env.backendUrl +
  //   '/api/auth/gitlab/sync?' +
  //   'access_token=' +
  //   this.$auth.getToken('gitlab')
  // )
  return process.env.backendUrl + '/syncToken'
}

export function getPostData() {
  return {
    HTTP_CONTENT_LANGUAGE: self.language
  }
}

export function getPostConfig(host) {
  return {
    headers: {
      OAuthHost: host
    }
  }
}
