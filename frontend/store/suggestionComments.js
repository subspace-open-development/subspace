import {
  NewSuggestionComment,
  SuggestionCommentList
} from '../models/SuggestionComments'

export const state = () => ({
  suggestionComments: {} // organized like suggestions[suggestionIid]
})

export const mutations = {
  updateSuggestionComments(state, { suggestionId, suggestionComments }) {
    state.suggestionComments = Object.assign(state.suggestionComments, {
      [suggestionId]: suggestionComments
    })
  }
}

export const actions = {
  submitNewSuggestionComment({ context, commit, dispatch }, params) {
    const newSuggestionComment = new NewSuggestionComment({})

    Object.keys(params).forEach(function(item) {
      newSuggestionComment[item] = params[item]
    })

    return newSuggestionComment.save().then((suggestionComment) => {
      dispatch('fetchSuggestionComments', params.suggestionId)
      return suggestionComment
    })
  },
  // submitEditSuggestion({ context, commit, dispatch }, params) {
  //   const editSuggestion = new EditSuggestion({})
  //
  //   Object.keys(params).forEach(function(item) {
  //     editSuggestion[item] = params[item]
  //   })
  //
  //   editSuggestion.save().then((suggestionTree) => {
  //     dispatch('fetchSuggestionList', params.projectId)
  //     return suggestionTree
  //   })
  // },
  fetchSuggestionComments(context, suggestionId) {
    if (suggestionId !== undefined) {
      return SuggestionCommentList.params({
        suggestionId
      })
        .get()
        .then((suggestionComments) => {
          context.commit('updateSuggestionComments', {
            suggestionId,
            suggestionComments
          })
          return suggestionComments
        })
    }
  }
}

export const getters = {
  getSuggestionComments: (state) => {
    return state.suggestionComments
  }
}
