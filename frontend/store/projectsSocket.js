export const state = () => ({
  socket: {
    isConnected: false,
    message: '',
    reconnectError: false
  }
})

export const mutations = {
  SOCKET_ONOPEN(state, event) {
    state.socket.isConnected = true
  },
  SOCKET_ONCLOSE(state, event) {
    state.socket.isConnected = false
  },
  SOCKET_ONERROR(state, event) {
    // eslint-disable-next-line
    console.error(state, event)
  },
  // default handler called for all methods
  SOCKET_ONMESSAGE(state, message) {
    state.socket.message = message
  },
  // mutations for reconnect methods
  SOCKET_RECONNECT(state, count) {
    // eslint-disable-next-line
    console.info(state, count)
  },
  SOCKET_RECONNECT_ERROR(state) {
    state.socket.reconnectError = true
  }
}

export const actions = {
  printMessage(state, message) {
    // eslint-disable-next-line
    console.log(JSON.stringify(message))
  }
}

export const getters = {}

// import Vuex from 'vue'
//
// const store = new Vuex.Store({
//   state: {
//     socket: {
//       isConnected: false,
//       message: '',
//       reconnectError: false
//     }
//   },
//   mutations: {
//     SOCKET_ONOPEN(state, event) {
//       state.socket.isConnected = true
//     },
//     SOCKET_ONCLOSE(state, event) {
//       state.socket.isConnected = false
//     },
//     SOCKET_ONERROR(state, event) {
//       console.error(state, event)
//     },
//     // default handler called for all methods
//     SOCKET_ONMESSAGE(state, message) {
//       // eslint-disable-next-line
//       console.log(message)
//       state.socket.message = message
//     },
//     // mutations for reconnect methods
//     SOCKET_RECONNECT(state, count) {
//       console.info(state, count)
//     },
//     SOCKET_RECONNECT_ERROR(state) {
//       state.socket.reconnectError = true
//     }
//   },
//   actions: {
//     printMessage(message) {
//       // eslint-disable-next-line
//       console.log(message)
//     }
//   },
//   getters: {}
// })
//
// export default store
