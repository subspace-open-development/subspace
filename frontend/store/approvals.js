import { Approval } from '../models/Approvals'

export const actions = {
  submitNewApproval({ context, commit, dispatch }, params) {
    const approval = new Approval({})

    Object.keys(params).forEach(function(item) {
      approval[item] = params[item]
    })

    return approval.save().then(() => {
      return dispatch('user/fetchSubspaceUser', params.userId, { root: true })
    })
  },
  submitEditApproval({ context, commit, dispatch }, params, id) {
    const approval = new Approval({ id })

    Object.keys(params).forEach(function(item) {
      approval[item] = params[item]
    })

    return approval.save()
  },
  submitRemoveApproval({ context, commit, dispatch }, id) {
    const approval = new Approval({ id })
    return approval.delete()
  }
}
