import { ProjectSettings } from '../models/ProjectSettings'

export const state = () => ({
  projectSettings: {}
})

export const mutations = {
  updateProjectSettings(state, projectSettings) {
    state.projectSettings = projectSettings
  }
}

export const actions = {
  submitUpdateProjectSettings({ context, commit, dispatch }, params, id) {
    const settings = new ProjectSettings({ id })

    Object.keys(params).forEach(function(item) {
      settings[item] = params[item]
    })

    return settings.save()
  },
  fetchProjectSettings(context, projectId) {
    return ProjectSettings.find(projectId).then((settings) => {
      context.commit('updateProjectSettings', settings)
      return settings
    })
  }
}

export const getters = {
  getProjectSettings: (state) => {
    return state.projectSettings
  }
}
