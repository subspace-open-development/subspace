import { TaskTree } from '../models/Tasks'

export const state = () => ({
  taskTree: []
})

export const mutations = {
  updateTaskTree(state, taskTree) {
    state.taskTree = taskTree
  }
}

export const actions = {
  fetchTaskTree(context, projectId) {
    if (projectId !== undefined) {
      return TaskTree.params({
        projectId
      })
        .get()
        .then((taskTree) => {
          context.commit('updateTaskTree', taskTree)
          return taskTree
        })
    }
  }
}

export const getters = {
  getTaskTree: (state) => {
    return state.taskTree
  },
  getTaskParentsArray: (state) => (taskIid) => {
    return (
      state.taskTree.length &&
      state.taskTree
        .map((x) => {
          return getParents(taskIid, x, [x.task])
        })
        .flat()
    )
  },
  getTaskChildrenArray: (state) => (taskIid) => {
    const taskArray =
      state.taskTree.length &&
      state.taskTree
        .map((x) => {
          return getChildren(taskIid, x)
        })
        .flat()
    return (taskArray.length && taskArray.map((x) => x?.task)) || []
  },
  getTaskPeerArray: (state) => (taskIid) => {
    return (
      state.taskTree.length &&
      state.taskTree
        .map((x) => {
          return getPeers(taskIid, x, undefined, state.taskTree)
        })
        .flat()
    )
  }
}

function getParents(taskIid, taskObj, parentArray) {
  if (taskObj.task.iid === taskIid) {
    return parentArray.slice(0, -1).flat()
  } else if (taskObj?.children) {
    return taskObj?.children
      .map((x) => getParents(taskIid, x, parentArray.concat(x.task)))
      .flat()
  } else {
    return []
  }
}

function getChildren(taskIid, taskObj) {
  if (taskObj.task.iid === taskIid) {
    return taskObj?.children || []
  } else if (taskObj?.children) {
    return taskObj?.children.map((x) => getChildren(taskIid, x)).flat()
  } else {
    return []
  }
}

function getPeers(taskIid, taskObj, parentTask, topPeers = []) {
  if (taskObj.task.iid === taskIid) {
    if (!parentTask) {
      return topPeers.filter((x) => x.task.iid !== taskIid).map((x) => x.task)
    } else {
      return (parentTask?.children || [])
        .filter((x) => x.task.iid !== taskIid)
        .map((x) => x.task)
    }
  } else if (taskObj?.children) {
    return taskObj?.children.map((x) => getPeers(taskIid, x, taskObj)).flat()
  } else {
    return []
  }
}
