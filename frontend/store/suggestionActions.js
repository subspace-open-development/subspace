import { SuggestionAction } from '../models/Suggestions'

export const actions = {
  submitSuggestionAction(
    { context, commit, dispatch },
    { suggestionId, action }
  ) {
    return SuggestionAction.params({
      suggestionId,
      action
    })
      .get()
      .then((response) => {
        return response
      })
  }
}
