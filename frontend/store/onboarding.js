import Onboarding from '~/models/Onboarding'

export const state = () => ({
  project: {}
})

export const mutations = {
  updateOnboardedProject(state, project) {
    state.project = project
  }
}

export const actions = {
  submitOnboardProject({ context, commit, dispatch }, { host, hostProjectId }) {
    Onboarding.params({
      host,
      hostProjectId
    })
      .get()
      .then((projectArray) => {
        const project = projectArray[0]
        commit('updateOnboardedProject', project)
        return project
      })
      .then(() => {
        dispatch('gitRepos/fetchGitRepos', null, { root: true })
        dispatch('projects/fetchProjects', null, { root: true })
      })
  }
}

export const getters = {
  getOnboardedProject: (state) => {
    return state.project
  },
  getOnboardedError: (state) => {
    return state.project?.msg
  }
}
