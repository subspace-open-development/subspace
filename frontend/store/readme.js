import ReadMe from '~/models/ReadMe'

export const state = () => ({
  readmes: {}
})

export const mutations = {
  updateReadMe(state, { projectId, readMe }) {
    state.readmes[projectId] = readMe[0]?.content
  }
}

export const actions = {
  fetchReadMe(context, { projectId }) {
    return ReadMe.params({
      projectId
    })
      .get()
      .then((readMe) => {
        context.commit('updateReadMe', { projectId, readMe })
        return readMe
      })
  }
}

export const getters = {
  getReadMe: (state) => (projectId) => {
    return state.readmes[projectId]
  }
}
