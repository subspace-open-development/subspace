import {
  NewContribution,
  EditContribution,
  ContributionDetails,
  ContributionList
} from '../models/Contributions'

export const state = () => ({
  contributionList: [],
  contributions: {}, // organized like contributions[projectId][contributionIid]
  currentContribution: undefined
})

export const mutations = {
  updateContributionList(state, contributionList) {
    state.contributionList = contributionList
  },
  updateContributionDetails(state, { projectId, taskIid, contribution }) {
    if (!state.contributions[projectId]) {
      state.contributions[projectId] = {}
    }
    state.contributions[projectId][taskIid] = contribution[0]
  },
  updateCurrentContribution(state, { contribution }) {
    state.currentContribution = contribution[0]
  }
}

export const actions = {
  submitNewContribution({ context, commit, dispatch }, params) {
    const newContribution = new NewContribution({})

    Object.keys(params).forEach(function(item) {
      newContribution[item] = params[item]
    })

    return newContribution.save()
  },
  submitEditContribution({ context, commit, dispatch }, params) {
    const editContribution = new EditContribution({})

    Object.keys(params).forEach(function(item) {
      editContribution[item] = params[item]
    })

    return editContribution.save().then((taskTree) => {
      return taskTree
    })
  },
  submitRemoveContribution({ context, commit, dispatch }, id) {
    const contributionDetails = new ContributionDetails({ id })
    return contributionDetails.delete()
  },
  fetchContributionList(context, projectId) {
    if (projectId !== undefined) {
      return ContributionList.params({
        projectId
      })
        .get()
        .then((contributionList) => {
          context.commit('updateContributionList', contributionList)
          return contributionList
        })
    }
  },
  fetchContributionDetails(context, { projectId, contributionIid }) {
    if (projectId !== undefined && contributionIid !== undefined) {
      return ContributionDetails.params({
        projectId,
        contributionIid
      })
        .get()
        .then((contribution) => {
          context.commit('updateContributionDetails', {
            projectId,
            contributionIid,
            contribution
          })
          context.commit('updateCurrentContribution', { contribution })
          return contribution
        })
    }
  }
}

export const getters = {
  getContributionList: (state) => {
    return state.contributionList
  },
  getCurrentContribution: (state) => {
    return state.currentContribution
  },
  getAllContributions: (state) => {
    return state.contributions
  }
}
