import {
  NewSuggestion,
  EditSuggestion,
  SuggestionList,
  SuggestionDetails
} from '../models/Suggestions'

export const state = () => ({
  suggestionList: [],
  suggestions: {}, // organized like suggestions[projectId][suggestionIid]
  currentSuggestion: undefined
})

export const mutations = {
  updateSuggestionList(state, suggestionList) {
    state.suggestionList = suggestionList
  },
  updateSuggestionDetails(state, { projectId, suggestionIid, suggestion }) {
    if (!state.suggestions[projectId]) {
      state.suggestions[projectId] = {}
    }
    state.suggestions[projectId][suggestionIid] = suggestion[0]?.suggestion
  },
  updateCurrentSuggestion(state, { suggestion }) {
    state.currentSuggestion = suggestion[0]?.suggestion
  }
}

export const actions = {
  submitNewSuggestion({ context, commit, dispatch }, params) {
    const newSuggestion = new NewSuggestion({})

    Object.keys(params).forEach(function(item) {
      newSuggestion[item] = params[item]
    })

    return newSuggestion.save().then((suggestionTree) => {
      dispatch('fetchSuggestionList', params.projectId)
      dispatch('user/fetchSubspaceUser', params.userId, { root: true })
      return suggestionTree
    })
  },
  submitEditSuggestion({ context, commit, dispatch }, params) {
    const editSuggestion = new EditSuggestion({})

    Object.keys(params).forEach(function(item) {
      editSuggestion[item] = params[item]
    })

    return editSuggestion.save().then((suggestionTree) => {
      dispatch('fetchSuggestionList', params.projectId)
      return suggestionTree
    })
  },
  submitRemoveSuggestion({ context, commit, dispatch }, id) {
    const suggestionDetails = new SuggestionDetails({ id })
    return suggestionDetails.delete()
  },
  fetchSuggestionList(context, projectId) {
    if (projectId !== undefined) {
      return SuggestionList.params({
        projectId
      })
        .get()
        .then((suggestionList) => {
          context.commit('updateSuggestionList', suggestionList)
          return suggestionList
        })
    }
  },
  fetchSuggestionDetails(context, { projectId, suggestionIid }) {
    if (projectId !== undefined && suggestionIid !== undefined) {
      return SuggestionDetails.params({
        projectId,
        suggestionIid
      })
        .get()
        .then((suggestion) => {
          context.commit('updateSuggestionDetails', {
            projectId,
            suggestionIid,
            suggestion
          })
          context.commit('updateCurrentSuggestion', { suggestion })
          return suggestion
        })
    }
  }
}

export const getters = {
  getSuggestionList: (state) => {
    return state.suggestionList
  },
  getCurrentSuggestion: (state) => {
    return state.currentSuggestion
  },
  getAllSuggestions: (state) => {
    return state.suggestions
  }
}
