import GitGroups from '~/models/GitGroups'

export const state = () => ({
  gitGroups: []
})

export const mutations = {
  updateGitGroups(state, gitGroupList) {
    // eslint-disable-next-line no-unused-vars
    const { gitlabGroups, githubGroups } = gitGroupList[0]
    state.gitGroups = gitlabGroups
  }
}

export const actions = {
  fetchGitGroups(context) {
    GitGroups.get().then((gitGroups) => {
      context.commit('updateGitGroups', gitGroups)
      return gitGroups
    })
  }
}

export const getters = {
  getGitGroups: (state) => {
    return state.gitGroups
  }
}
