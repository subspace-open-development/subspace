import { Tos } from '~/models/Tos'

export const state = () => ({
  tos: {
    gitlab: {}
  }
})

export const mutations = {
  updateGitlabAcceptedTOS(state, tos) {
    state.tos.gitlab = tos[0]
  }
}

export const actions = {
  fetchGitlabAcceptedTOS(context) {
    return Tos.get().then((tos) => {
      context.commit('updateGitlabAcceptedTOS', tos)
      return tos
    })
  }
}

export const getters = {
  getGitlabAcceptedTOS: (state) => {
    return state.tos.gitlab?.accepted
  }
}
