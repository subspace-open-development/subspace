// import Profile from '~/models/Profile'

export const state = () => ({
  account: {
    profile: {
      id: 0,
      gitlab_username: '',
      reputation: 0,
      stripe_connected: false
    },
    settings: {},
    history: {}
  }
})

export const mutations = {
  updateProfile(state, profile) {
    state.account.profile = profile
  }
}

export const actions = {
  // fetchProfile(context) {
  //   return Profile.get().then((profileInArray) => {
  //     context.commit('updateProfile', profileInArray[0])
  //     return profileInArray[0]
  //   })
  // }
}

export const getters = {
  getGitlabUsername: (state) => {
    return state.account.profile.gitlab_username
  },
  getReputation: (state) => {
    return (
      state.account.profile.reputation &&
      Math.round(state.account.profile.reputation * 100) / 100
    )
  },
  getStripeConnected: (state) => {
    return state.account.profile.stripe_connected
  }
}
