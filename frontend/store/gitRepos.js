import GitRepos from '~/models/GitRepos'

export const state = () => ({
  gitRepos: []
})

export const mutations = {
  updateGitRepos(state, gitRepoList) {
    // eslint-disable-next-line no-unused-vars
    const { gitlabRepos, githubRepos } = gitRepoList[0]
    state.gitRepos = gitlabRepos
  }
}

export const actions = {
  fetchGitRepos(context) {
    GitRepos.get().then((gitRepos) => {
      context.commit('updateGitRepos', gitRepos)
      return gitRepos
    })
  }
}

export const getters = {
  getGitRepos: (state) => {
    return state.gitRepos
  }
}
