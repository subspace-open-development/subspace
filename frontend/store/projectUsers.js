import ProjectUsers from '~/models/ProjectUsers'

export const state = () => ({
  projectUsers: []
})

export const mutations = {
  updateProjectUsers(state, projectUsers) {
    state.projectUsers = projectUsers.sort((a, b) =>
      a.reputation < b.reputation ? 1 : -1
    )
  }
}

export const actions = {
  fetchProjectUsers(context, projectId) {
    if (projectId !== undefined) {
      return ProjectUsers.params({
        projectId
      })
        .get()
        .then((projectUsers) => {
          context.commit('updateProjectUsers', projectUsers)
          return projectUsers
        })
    }
  },
  addProjectUser({ context, commit, dispatch }, { host, hostProjectId }) {
    return new ProjectUsers({
      host,
      hostProjectId
    })
      .save()
      .then(() => {
        return dispatch('projects/fetchProjects', null, { root: true })
      })
  }
}

export const getters = {
  getProjectUsers: (state) => {
    return state.projectUsers
  }
}
