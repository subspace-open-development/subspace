import GitRepoBranches from '../models/GitRepoBranches'

export const state = () => ({
  gitRepoBranches: {}
})

export const mutations = {
  updateGitRepoBranches(state, gitRepoBranches) {
    state.gitRepoBranches = gitRepoBranches[0]
  }
}

export const actions = {
  fetchGitRepoBranches(context, projectId) {
    if (projectId !== undefined) {
      return GitRepoBranches.params({
        projectId
      })
        .get()
        .then((gitRepoBranches) => {
          context.commit('updateGitRepoBranches', gitRepoBranches)
          return gitRepoBranches
        })
    }
  }
}

export const getters = {
  getGitRepoBranches: (state) => {
    return state.gitRepoBranches
  }
}
