import GitUser from '~/models/GitUser'
import SubspaceUser from '~/models/SubspaceUser'

export const state = () => ({
  gitUser: {
    host: '',
    git_user_email: '',
    git_user_username: ''
  },
  subspaceUser: {
    id: 0,
    email: '',
    created_at: ''
  }
})

export const mutations = {
  updateGitUser(state, gitUser) {
    state.gitUser = gitUser
  },
  updateSubspaceUser(state, subspaceUser) {
    state.subspaceUser = subspaceUser
  }
}

export const actions = {
  fetchGitUser(context) {
    GitUser.get().then((gitUser) => {
      context.commit('updateGitUser', gitUser[0])
      return gitUser
    })
  },
  fetchSubspaceUser(context) {
    SubspaceUser.get().then((subspaceUser) => {
      context.commit('updateSubspaceUser', subspaceUser[0])
      return subspaceUser
    })
  }
}

export const getters = {
  getGitUser: (state) => {
    return state.gitUser
  },
  getSubspaceUser: (state) => {
    return state.subspaceUser
  }
}
