import Commits from '../models/Commits'

export const state = () => ({
  commits: {} // organized like commits[projectId][branch]
})

export const mutations = {
  updateCommits(state, { projectId, branch, commitArray }) {
    if (!state.commits[projectId]) {
      state.commits[projectId] = {}
    }
    state.commits[projectId][branch] = commitArray
  }
}

export const actions = {
  fetchCommits(context, { projectId, contributionId, suggestionId, branch }) {
    if (
      projectId !== undefined &&
      contributionId !== undefined &&
      suggestionId !== undefined &&
      branch !== undefined
    ) {
      return Commits.params({
        projectId,
        contributionId,
        suggestionId,
        branch
      })
        .get()
        .then((commitArray) => {
          context.commit('updateCommits', {
            projectId,
            branch,
            commitArray
          })
          return commitArray
        })
    }
  }
}

export const getters = {
  getCommits: (state) => (projectId, branch) => {
    return (state.commits[projectId] && state.commits[projectId][branch]) || []
  }
}
