import { NewTask, EditTask, TaskList, TaskDetails } from '../models/Tasks'

export const state = () => ({
  taskList: [],
  tasks: {}, // organized like tasks[projectId][taskIid]
  currentTask: undefined
})

export const mutations = {
  updateTaskList(state, taskList) {
    state.taskList = taskList.sort((a, b) => (a.key > b.key ? 1 : -1))
  },
  updateTaskDetails(state, { projectId, taskIid, task }) {
    if (!state.tasks[projectId]) {
      state.tasks[projectId] = {}
    }
    state.tasks[projectId][taskIid] = task[0]
  },
  updateCurrentTask(state, { task }) {
    state.currentTask = task[0]
  }
}

export const actions = {
  submitNewTask({ context, commit, dispatch }, params) {
    const newTask = new NewTask({})

    Object.keys(params).forEach(function(item) {
      newTask[item] = params[item]
    })

    newTask.save().then((taskTree) => {
      dispatch('taskTree/fetchTaskTree', params.projectId, { root: true })
      dispatch('fetchTaskList', params.projectId)
      return taskTree
    })
  },
  submitEditTask({ context, commit, dispatch }, params) {
    const editTask = new EditTask({})

    Object.keys(params).forEach(function(item) {
      editTask[item] = params[item]
    })

    return editTask.save().then((taskTree) => {
      dispatch('taskTree/fetchTaskTree', params.projectId, { root: true })
      dispatch('fetchTaskList', params.projectId)
      return taskTree
    })
  },
  submitRemoveTask({ context, commit, dispatch }, id) {
    const taskDetails = new TaskDetails({ id })
    return taskDetails.delete()
  },
  fetchTaskList(context, projectId) {
    if (projectId !== undefined) {
      return TaskList.params({
        projectId
      })
        .get()
        .then((taskList) => {
          context.commit('updateTaskList', taskList)
          return taskList
        })
    }
  },
  fetchTaskDetails(context, { projectId, taskIid }) {
    if (projectId !== undefined && taskIid !== undefined) {
      return TaskDetails.params({
        projectId,
        taskIid
      })
        .get()
        .then((task) => {
          context.commit('updateTaskDetails', { projectId, taskIid, task })
          context.commit('updateCurrentTask', { task })
          return task
        })
    }
  },
  fetchTaskDetailsById(context, { taskId }) {
    if (taskId !== undefined) {
      return TaskDetails.params({
        taskId
      })
        .get()
        .then((task) => {
          context.commit('updateTaskDetails', {
            projectId: task[0].task.projectId,
            taskIid: task[0].task.iid,
            task
          })
          context.commit('updateCurrentTask', { task })
          return task
        })
    }
  }
}

export const getters = {
  getTaskList: (state) => {
    return state.taskList
  },
  getCurrentTask: (state) => {
    return state.currentTask
  },
  getAllTasks: (state) => {
    return state.tasks
  },
  getTaskById: (state) => (id) => {
    return state.taskList.filter((t) => t.task.id === id)
  }
}
