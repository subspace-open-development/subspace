import {
  GITLAB,
  getPostTokenUrl,
  getPostData,
  getPostConfig
} from '~/utils/auth'

export const state = () => ({
  host: '',
  gitlabToken: ''
})

export const mutations = {
  updateHost(state, host) {
    state.host = host
  },
  updateGitlabToken(state, token) {
    mutations.updateHost(state, GITLAB)
    state.gitlabToken = token
  }
}

export const actions = {
  syncGitlabToken(context) {
    if (this.$auth.getToken('gitlab')) {
      this.$axios.$post(getPostTokenUrl(), getPostData(), getPostConfig(GITLAB))
      context.commit('updateGitlabToken', this.$auth.getToken('gitlab'))
    } else {
      this.$auth.loginWith('gitlab').then(() => {
        if (this.$auth.getToken('gitlab')) {
          context.commit('updateGitlabToken', this.$auth.getToken('gitlab'))
        }
        return this.$axios.$post(
          getPostTokenUrl(),
          getPostData(),
          getPostConfig(GITLAB)
        )
      })
    }
  }
}

export const getters = {
  getGitlabToken: (state) => {
    return state.gitlabToken
  }
}
