import Projects, {
  PublicProjects,
  PublicProject,
  JoinPublicProject
} from '~/models/Projects'

export const state = () => ({
  currentProjectRouteParams: {
    host: undefined,
    group: undefined,
    name: undefined
  },
  currentProjectId: undefined,
  projects: [],
  publicProjects: []
})

export const mutations = {
  updateProjects(state, projects) {
    state.projects = projects
  },
  addProjects(state, projects) {
    state.projects = state.projects.concat(projects)
  },
  updatePublicProjects(state, projects) {
    state.publicProjects = projects
  },
  updateCurrentProject(state, projectId) {
    state.currentProjectId = projectId
  },
  updateCurrentProjectParams: (state, { host, group, name }) => {
    state.currentProjectRouteParams = { host, group, name }

    const currentProject = state.projects
      .filter((p) => p.namespace === state.currentProjectRouteParams?.group)
      .filter((p) => p.name === state.currentProjectRouteParams?.name)[0]
    state.currentProjectId = currentProject?.id
  },
  updateCurrentProjectFromParams: (state) => {
    const currentProject = state.projects
      .filter((p) => p.namespace === state.currentProjectRouteParams?.group)
      .filter((p) => p.name === state.currentProjectRouteParams?.name)[0]
    state.currentProjectId = currentProject?.id
  }
}

export const actions = {
  fetchProjects(context) {
    return Projects.get().then((projects) => {
      context.commit('updateProjects', projects)
      context.commit('updateCurrentProjectFromParams')
      return projects
    })
  },
  fetchPublicProjects(context, { limit, offset }) {
    return PublicProjects.params({
      limit,
      offset
    })
      .get()
      .then((projects) => {
        context.commit('updatePublicProjects', projects)
        return projects
      })
  },
  fetchPublicProject(context, { host, namespace, name }) {
    return PublicProject.params({
      host,
      namespace,
      name
    })
      .get()
      .then((projects) => {
        context.commit('addProjects', projects)
        return projects
      })
  },
  joinPublicProject(context, { projectId }) {
    return JoinPublicProject.params({ projectId }).get()
  }
}

export const getters = {
  getProjects: (state) => {
    return state.projects
  },
  getPublicProjects: (state) => {
    return state.publicProjects
  },
  getCurrentProject: (state) => {
    return state.projects.filter((p) => p.id === state.currentProjectId)[0]
  },
  getCurrentProjectHomeUrl: (state) => {
    const project = state.projects.filter(
      (p) => p.id === state.currentProjectId
    )[0]
    return project.web_url
  },
  getCurrentProjectId: (state) => {
    return state.currentProjectId
  }
}
