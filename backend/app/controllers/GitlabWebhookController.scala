package controllers

import com.google.inject.Inject
import models._
import models.webhooks.gitlab.{CommentWebhook, IssueWebhook, MergeRequestWebhook}
import play.api.libs.json.{JsString, Json}
import play.api.mvc._
import services.gitlab.{GitlabWebhookService}

import scala.concurrent.{ExecutionContext, Future}
import play.api.{Configuration, Logger => PlayLogger}

class GitlabWebhookController @Inject()(gitlabWebhookService: GitlabWebhookService,
                                        cc: MessagesControllerComponents
                                      )(implicit ec: ExecutionContext) extends MessagesAbstractController(cc) {

  val playLogger = PlayLogger("application")

  import models.webhooks.gitlab.GitlabWebhooks._

  def receiveWebhook = Action.async(parse.json) { implicit request =>

    val kind = (request.body \ "object_kind").getOrElse(JsString("")).as[String]

    playLogger.info("Action: " + (request.body \ "object_attributes" \ "action").getOrElse(JsString("")).as[String])

    playLogger.info(request.body.toString())

    kind match {
//      case "merge_request" => {
//        request.body.validate[MergeRequestWebhook].fold(
//          error => Future.successful(InternalServerError("JSON did not validate: " + error.toString())),
//          obj => gitlabWebhookService.processMergeRequest(obj).map{ _ => Ok(Json.toJson(obj)) }
//        )
//      }
//      case "note" => {
//        request.body.validate[CommentWebhook].fold(
//          error => Future.successful(InternalServerError("JSON did not validate: " + error.toString())),
//          obj => gitlabWebhookService.processComment(obj).map{ _ => Ok(Json.toJson(obj)) }
//        )
//      }
//      case "issue" => {
//        request.body.validate[IssueWebhook].fold(
//          error => Future.successful(InternalServerError("JSON did not validate: " + error.toString())),
//          obj => gitlabWebhookService.processIssue(obj).map{ _ => Ok(Json.toJson(obj)) }
//        )
//      }
//      case "push" => {
//        request.body.validate[PushWebhook].fold(
//          error => Future.successful(InternalServerError("JSON did not validate")),
//          obj => gitlabWebhookService.processPush(obj).map{ _ => Ok(Json.toJson(obj)) }
//        )
//      }
      case _ => Future.successful(InternalServerError("Could not recognize event type"))
    }
  }

}