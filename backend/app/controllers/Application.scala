package controllers

import java.sql.Timestamp
import java.util.Date

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.google.inject.Inject
import jobs.AllScheduledJobs
import models.util.TimestampFormat
import models.{HostProject, _}
import play.api.cache.SyncCacheApi
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.{Configuration, Logger => PlayLogger}
import repositories._
import services._
import services.config.{GitlabConfigService, SubspaceConfigService}
import services.gitlab._
import utils.Logger
import utils.Token._
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}


class Application @Inject()(gitlabUserService: GitlabUserService,
                            gitlabProjectService: GitlabProjectService,
                            websocketTopicsService: WebsocketTopicsService,
                            userRepository: UserRepository,
                            userReputationRepository: UserReputationRepository,
                            userManaRepository: UserManaRepository,
                            projectRepository: ProjectRepository,
                            projectSettingsRepository: ProjectSettingsRepository,
                            dynamicsAndRatingService: DynamicsAndRatingService,
                            groupUserRepository: GroupUserRepository,
                            projectUserRepository: ProjectUserRepository,
                            groupProjectRepository: GroupProjectRepository,
                            gitlabGroupService: GitlabGroupService,
                            gitlabRepositoryService: GitlabRepositoryService,
                            gitlabCommitsService: GitlabCommitsService,
                            gitlabRepositoryFileService: GitlabRepositoryFileService,
                            hostUserRepository: HostUserRepository,
                            hostRepository: HostRepository,
                            backRepository: BackRepository,
                            taskRepository: TaskRepository,
                            hostUserService: HostUserService,
                            taskService: TaskService,
                            permissionsService: PermissionsService,
                            suggestionRepository: SuggestionRepository,
                            suggestionService: SuggestionService,
                            suggestionCommentRepository: SuggestionCommentRepository,
                            contributionRepository: ContributionRepository,
                            contributionService: ContributionService,
                            cc: ControllerComponents,
                            cache: SyncCacheApi,
                            ws: WSClient,
                            gitlabConf: GitlabConfigService,
                            scheduledJobs: AllScheduledJobs,
                            subspaceConfigService: SubspaceConfigService,
                            conf: Configuration)(implicit ec: ExecutionContext, system: ActorSystem, mat: Materializer) extends AbstractController(cc) with Logger {

  val AuthTokenHeader = "Authorization"
  val OAuthHostHeader = "OAuthHost"

  case class AuthorizedRequest[A](maybeHostUser: Option[HostUser], request: Request[A]) extends WrappedRequest[A](request)

  class AuthorizedAction(hostUserService: HostUserService) extends ActionBuilder[AuthorizedRequest, AnyContent] {
    override protected def executionContext: ExecutionContext = cc.executionContext

    override def parser: BodyParser[AnyContent] = cc.parsers.defaultBodyParser

    override def invokeBlock[A](request: Request[A], f: AuthorizedRequest[A] => Future[Result]): Future[Result] = {

      val maybeToken = request.headers.get(AuthTokenHeader)
      val maybeHost = request.headers.get(OAuthHostHeader)

      if (maybeToken.isDefined) {
        val token = trimToken(maybeToken.get)

        for {
          hostUser <- hostUserService.getOrCreateHostUserFromToken(token)
          result <- f(AuthorizedRequest(Some(hostUser), request))
        } yield result
      } else {
        f(AuthorizedRequest(None, request))
        // Future.successful(Results.Unauthorized)
      }
    }
  }

  val authorizedAction = new AuthorizedAction(hostUserService)

  val defaultToken = "0da0e5b8c265e08fa1ecf015361df2f5217105d4eb87a4214dbd5e511dd5b15a" // joecorey30@gmail.com -- an empty account

  val playLogger = PlayLogger("application")

  //  def stripeCallback(code: Option[String], scope: Option[String]) = Action { implicit request =>
  //    playLogger.info("stripeCallback()")
  //
  //    val appId = stripeConfigService.appId
  //    val appSecret = stripeConfigService.appSecret
  //
  //    val parameters = s"?client_secret=$appSecret&code=${code.get}&grant_type=authorization_code"
  //    val urlAndParameters = s"https://connect.stripe.com/oauth/token$parameters"
  //
  //    val tokenRequest = ws.url(urlAndParameters).addHttpHeaders(("Content-Type", "application/json")).post("")
  //    val jsonResultFuture = tokenRequest.map {
  //      response => {
  //        (
  //          (response.json \ "token_type").as[String],
  //          (response.json \ "stripe_publishable_key").as[String],
  //          (response.json \ "scope").as[String],
  //          (response.json \ "livemode").as[Boolean],
  //          (response.json \ "stripe_user_id").as[String],
  //          (response.json \ "refresh_token").as[String],
  //          (response.json \ "access_token").as[String]
  //        )
  //      }
  //    }
  //
  //    import scala.language.postfixOps
  //    val jsonResult = Await.result(jsonResultFuture, 5 seconds)
  //
  //    val (token_type, stripe_publishable_key, theScope, livemode, stripe_user_id, refresh_token, access_token) = jsonResult
  //    val tokenCallback = StripeTokenCallback(token_type, stripe_publishable_key, theScope, livemode, stripe_user_id, refresh_token, access_token)
  //
  //    for {
  //      maybeRawToken <- Future.successful(request.headers.get(GitlabAuthTokenHeader))
  //      maybeUser <- maybeRawToken match {
  //        case Some(rawToken) => userRepository.findByGitlabToken(trimGitlabToken(rawToken))
  //        case None => Future.successful(None)
  //      }
  //      maybeStripeUser <- maybeUser match {
  //        case Some(user: User) => Future.successful(Some(StripeUser.createFromCallback(user.id, appId, tokenCallback)))
  //        case None => Future.successful(None)
  //      }
  //      _ <- maybeStripeUser match {
  //        case Some(su: StripeUser) => stripeUserRepository.create(su)
  //        case None => Future.successful(None)
  //      }
  //      _ <- maybeStripeUser match {
  //        case Some(su: StripeUser) => userRepository.updateStripeConnectedById(su.user_id, connected = true)
  //        case None => Future.successful(None)
  //      }
  //    } yield maybeStripeUser
  //
  //    val redirectURL = s"${subspaceConfigService.frontendUrl}/account"
  //    Redirect(redirectURL)
  //  }

  //  def getGitGroups() = {
  //    authorizedAction.async ( implicit request => {
  //      val token = trimToken(request.headers.get(AuthTokenHeader).get)
  //
  //      for {
  //        gitlabGroups <- gitlabGroupService.getMyGroups(token)
  //        gitlabGroupsWire = AllGitGroups(
  //          gitlabGroups = gitlabGroups.map(group => GitGroup.fromGitlabGroup(group)),
  //          githubGroups = Seq.empty[GitGroup]
  //        )
  //      } yield Ok(Json.toJson(gitlabGroupsWire))
  //    })
  //  }

  def getGitReadme(projectId: Int) = {
    authorizedAction.async(implicit request => {
      implicit val token = if (request.maybeHostUser.isDefined) request.maybeHostUser.get.host_user_token else defaultToken

      for {
        subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
        hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)

        gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
        isPublic = gitlabProject.getVisibility.toString == "public"

        isAllowed = hasBasicPermissions || isPublic
        maybeReadmeRepoFile <- {
          if (isAllowed) {
            gitlabRepositoryFileService.getFile("README.md", subspaceProject.host_project_id, "master")
          } else {
            Future.successful(None)
          }
        }

        readMe = if (maybeReadmeRepoFile.isDefined) ReadMe(Some(maybeReadmeRepoFile.get.getContent)) else ReadMe(None)
      } yield {
        Ok(Json.toJson(readMe))
      }
    })
  }

  def getGitRepos() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            gitlabProjects <- gitlabProjectService.getMyProjects(token)
            subspaceProjects <- Future.sequence(gitlabProjects.map(gp => projectRepository.findByHostIdAndHostProjectId(hostUser.host_id, gp.getId)))
            projectUsers <- projectUserRepository.findByUserId(hostUser.user_id)
            gitlabReposWire = AllGitRepos(
              gitlabRepos = gitlabProjects.map(project => GitRepo.fromGitlabProject(project, subspaceProjects.flatten, projectUsers)),
              githubRepos = Seq.empty[GitRepo]
            )
          } yield {
            Ok(Json.toJson(gitlabReposWire))
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getRepoBranches(projectId: Int) = {
    implicit val branchFormat = Json.writes[Branch]
    implicit val branchesFormat = Json.writes[Branches]

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabBranches <- gitlabRepositoryService.getBranches(token, subspaceProject.host_project_id)
            tasks <- taskRepository.getTaskListWire(subspaceProject.id)
            contributions <- contributionRepository.getAllByProjectId(subspaceProject.id)
            branches = Branches(subspaceProject.id, subspaceProject.host_id, subspaceProject.host_project_id, gitlabBranches.map(glb => Branch.fromGitlabBranch(glb, tasks, contributions)))
          } yield Ok(Json.toJson(branches))
        }
        case None => {
          val token = defaultToken

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabBranches <- gitlabRepositoryService.getBranches(token, subspaceProject.host_project_id)
            tasks <- taskRepository.getTaskListWire(subspaceProject.id)
            contributions <- contributionRepository.getAllByProjectId(subspaceProject.id)
            branches = Branches(subspaceProject.id, subspaceProject.host_id, subspaceProject.host_project_id, gitlabBranches.map(glb => Branch.fromGitlabBranch(glb, tasks, contributions)))

            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
          } yield {
            if (isPublic){
              Ok(Json.toJson(branches))
            } else {
              Ok(Json.toJson(Seq.empty[Branches]))
            }
          }
        }
      }
    })
  }

  def getProjects() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val projectFormat = Json.writes[Project]

          for {
            //        gitlabProjects <- gitlabProjectService.getMyProjects(token)
            //        hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            //        maybeSubspaceProjects <- Future.sequence(gitlabProjects.map(gp => projectRepository.findByHostIdAndHostProjectId(hostUser.host_id, gp.getId)))
            //        subspaceProjects = maybeSubspaceProjects.flatten

            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            projectUsers <- projectUserRepository.findByUserId(hostUser.user_id)
            projectIds = projectUsers.map(_.project_id).toSet
            subspaceProjects <- projectRepository.findByProjectIds(projectIds)
          } yield {
            Ok(Json.toJson(subspaceProjects))
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getPublicProject(host: String, namespace: String, name: String) = {
    authorizedAction.async(implicit request => {
      val token = defaultToken

      for {
        host <- hostRepository.findByName(host) failOnNone util.NotFound(s"Host with [name = $host]")
        subspaceProject <- projectRepository.findByHostIdNamespaceName(host.id, namespace: String, name: String) failOnNone util.NotFound(s"SubspaceProject with [host_id = ${host.id}, namespace = ${namespace}, name = ${name}]")
        gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
        isPublic = gitlabProject.getVisibility.toString == "public"
      } yield {
        if (isPublic){
          Ok(Json.toJson(Seq(subspaceProject)))
        } else {
          Ok(Json.toJson(Seq.empty[Project]))
        }
      }
    })
  }

  def getPublicProjects(limit: Int = 10, offset: Int = 0) = {
    implicit val timestampFormat = TimestampFormat
    implicit val projectFormat = Json.writes[PublicProject]

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            projectsAndNumUsers <- projectRepository.getPublicProjectsAndUserCount(limit, offset)
            gitlabProjects <- Future.sequence(projectsAndNumUsers.map(x => {
              gitlabProjectService.getProjectById(token, x._1.host_project_id)
            }))

            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProjects <- Future.sequence(gitlabProjects.flatten.map(gp => projectRepository.findByHostIdAndHostProjectId(hostUser.host_id, gp.getId)))
            projectUsers <- projectUserRepository.findByUserId(hostUser.user_id)

            gitlabProjectMap = gitlabProjects.flatten.map(p => p.getId -> p).toMap
            publicProjects = projectsAndNumUsers.map(x => {
              val subspaceProject = x._1
              val numSubspaceUsers = x._2

              val gitlabProject = gitlabProjectMap(subspaceProject.host_project_id)
              val gitRepo = GitRepo.fromGitlabProject(gitlabProject, subspaceProjects.flatten, projectUsers)

              PublicProject(numSubspaceUsers, subspaceProject, Some(gitRepo))
            }).sortWith((a, b) => a.num_users > b.num_users)
          } yield {
            Ok(Json.toJson(publicProjects))
          }
        }
        case None => {
          val token = defaultToken

          for {
            projectsAndNumUsers <- projectRepository.getPublicProjectsAndUserCount(limit, offset)
            gitlabProjects <- Future.sequence(projectsAndNumUsers.map(x => {
              gitlabProjectService.getProjectById(token, x._1.host_project_id)
            }))

            gitlabProjectMap = gitlabProjects.flatten.map(p => p.getId -> p).toMap
            publicProjects = projectsAndNumUsers.map(x => {
              val subspaceProject = x._1
              val numSubspaceUsers = x._2

              val gitlabProject = gitlabProjectMap(subspaceProject.host_project_id)
              val gitRepo = GitRepo.fromGitlabProject(gitlabProject, Seq.empty, Seq.empty)

              PublicProject(numSubspaceUsers, subspaceProject, Some(gitRepo))
            }).sortWith((a, b) => a.num_users > b.num_users)
          } yield {
            Ok(Json.toJson(publicProjects))
          }
        }
      }
    })
  }

  def joinPublicProject(projectId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            adminHostUser <- hostUserRepository.findByHostAndHostId(subspaceProject.host_id, subspaceProject.host_user_admin_id) failOnNone util.NotFound(s"admin HostUser with [host_id = ${subspaceProject.host_user_admin_id}]")
            gitlabProject <- gitlabProjectService.getProjectById(adminHostUser.host_user_token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            _ <- if (isPublic) gitlabProjectService.addDeveloperToProject(adminHostUser.host_user_token, hostUser.host_user_id, subspaceProject.host_project_id) else Future.successful({})
            _ <- if (isPublic) projectUserRepository.addProjectUser(subspaceProject.id, hostUser.user_id) else Future.successful({})
          } yield {
            Ok("")
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getGitUser() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            host <- hostRepository.findById(hostUser.host_id) failOnNone util.NotFound(s"Host with [id = $hostUser.host_id]")
          } yield Ok(Json.toJson(hostUser.getWire(host.name)))
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getSubspaceUser() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val userFormat = Json.writes[User]

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            user <- userRepository.findById(hostUser.user_id) failOnNone util.NotFound(s"User with [userId = ${hostUser.user_id}]")
            //        userRep <- userReputationRepository.findByUserId(user.id)
            userRep <- dynamicsAndRatingService.getAccurateUserRating(user.id)
            userMana <- userManaRepository.getUserMana(user.id)
          } yield {
            Ok(Json.toJson(user.toWire(Some(userRep), Some(userMana.mana))))
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getTaskTree(projectId: Int) = {
    implicit val taskFormat = Json.writes[Task]
    implicit val taskWireFormat = Json.writes[TaskWire]

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            taskTreeWire <- if (hasBasicPermissions) taskRepository.getTaskTreeWire(subspaceProject.id) else Future.successful(Seq.empty[TaskWire])
          } yield Ok(Json.toJson(taskTreeWire))
        }
        case None => {
          val token = defaultToken

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            taskTreeWire <- if (isPublic) taskRepository.getTaskTreeWire(subspaceProject.id) else Future.successful(Seq.empty[TaskWire])
          } yield Ok(Json.toJson(taskTreeWire))
        }
      }
    })
  }

  def getTaskList(projectId: Int) = {
    implicit val taskFormat = Json.writes[Task]
    implicit val taskWireFormat = Json.writes[TaskWire]

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            taskListWire <- if (hasBasicPermissions) taskRepository.getTaskListWire(subspaceProject.id) else Future.successful(Seq.empty[TaskWire])
          } yield {
            Ok(Json.toJson(taskListWire))
          }
        }
        case None => {
          val token = defaultToken

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            taskListWire <- if (isPublic) taskRepository.getTaskListWire(subspaceProject.id) else Future.successful(Seq.empty[TaskWire])
          } yield {
            Ok(Json.toJson(taskListWire))
          }
        }
      }
    })
  }

  def getTaskDetails(projectId: Option[Int], taskIid: Option[Int], taskId: Option[Int]) = {
    implicit val taskFormat = Json.writes[Task]
    implicit val taskWireFormat = Json.writes[TaskWire]

    assert((projectId.isDefined && taskIid.isDefined) || taskId.isDefined, "Need to have either the [project id + task iid] OR [task id]")

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          if (projectId.isDefined && taskIid.isDefined) {
            for {
              hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
              subspaceProject <- projectRepository.findByProjectId(projectId.get) failOnNone util.NotFound(s"Project with [id = ${projectId.get}]")
              hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
              taskAndUser <- {
                if (hasBasicPermissions) {
                  taskService.getTaskDetails(projectId.get, taskIid.get)
                } else {
                  Future.successful(None)
                }
              } failOnNone util.NotFound(s"Task with [projectId = ${projectId}, taskIid = $taskIid]")
              maybeUserRep <- userReputationRepository.findByUserId(taskAndUser._2.id)
              userRepVal = if (maybeUserRep.isDefined) maybeUserRep.get.reputation else 0
              taskWire = taskAndUser._1.toTaskWire(Some(taskAndUser._2.toWire(maybeRep = Some(userRepVal))))
            } yield {
              Ok(Json.toJson(taskWire))
            }
          } else {
            for {
              hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
              taskAndUser <- taskService.getTaskDetailsById(taskId.get) failOnNone util.NotFound(s"Task with [taskId = ${taskId.get}")
              subspaceProject <- projectRepository.findByProjectId(taskAndUser._1.projectId) failOnNone util.NotFound(s"Project with [id = ${taskAndUser._1.projectId}]")
              hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
              maybeUserRep <- userReputationRepository.findByUserId(taskAndUser._2.id)
              userRepVal = if (maybeUserRep.isDefined) maybeUserRep.get.reputation else 0
              taskWire = taskAndUser._1.toTaskWire(Some(taskAndUser._2.toWire(maybeRep = Some(userRepVal))))
            } yield {
              if (hasBasicPermissions) {
                Ok(Json.toJson(taskWire))
              } else {
                Ok("")
              }
            }
          }
        }
        case None => {
          val token = defaultToken

          if (projectId.isDefined && taskIid.isDefined) {
            for {
              subspaceProject <- projectRepository.findByProjectId(projectId.get) failOnNone util.NotFound(s"Project with [id = ${projectId.get}]")
              gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
              isPublic = gitlabProject.getVisibility.toString == "public"
              taskAndUser <- {
                if (isPublic) {
                  taskService.getTaskDetails(projectId.get, taskIid.get)
                } else {
                  Future.successful(None)
                }
              } failOnNone util.NotFound(s"Task with [projectId = ${projectId}, taskIid = $taskIid]")
              maybeUserRep <- userReputationRepository.findByUserId(taskAndUser._2.id)
              userRepVal = if (maybeUserRep.isDefined) maybeUserRep.get.reputation else 0
              taskWire = taskAndUser._1.toTaskWire(Some(taskAndUser._2.toWire(maybeRep = Some(userRepVal))))
            } yield {
              Ok(Json.toJson(taskWire))
            }
          } else {
            for {
              taskAndUser <- taskService.getTaskDetailsById(taskId.get) failOnNone util.NotFound(s"Task with [taskId = ${taskId.get}")
              subspaceProject <- projectRepository.findByProjectId(taskAndUser._1.projectId) failOnNone util.NotFound(s"Project with [id = ${taskAndUser._1.projectId}]")
              gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
              isPublic = gitlabProject.getVisibility.toString == "public"
              maybeUserRep <- userReputationRepository.findByUserId(taskAndUser._2.id)
              userRepVal = if (maybeUserRep.isDefined) maybeUserRep.get.reputation else 0
              taskWire = taskAndUser._1.toTaskWire(Some(taskAndUser._2.toWire(maybeRep = Some(userRepVal))))
            } yield {
              if (isPublic) {
                Ok(Json.toJson(taskWire))
              } else {
                Ok("")
              }
            }
          }
        }
      }
    })
  }

  def newTask() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          implicit val token = hostUser.host_user_token

          implicit val newTaskFormat = Json.reads[NewTask]
          val newTask = request.body.asJson.get.as[NewTask]

          for {
            subspaceProject <- projectRepository.findByProjectId(newTask.projectId) failOnNone util.NotFound(s"Project with [id = ${newTask.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            task <- taskService.addNewTask(newTask)
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def editTask(taskId: Int) = { // the task id param is not actually used here
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val editTaskFormat = Json.reads[EditTask]
          val editTask = request.body.asJson.get.as[EditTask]

          // TODO: check that all the edits are valid

          for {
            subspaceProject <- projectRepository.findByProjectId(editTask.projectId) failOnNone util.NotFound(s"Project with [id = ${editTask.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            taskToEdit <- taskRepository.findById(editTask.id) failOnNone util.NotFound(s"Task with [id = ${editTask.id}]") // FIXME: this logic might be better suited for the TaskService, there is a draft -- the 'editTask' function
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            isOwner = hostUser.user_id == taskToEdit.ownerId
            taskId <- {
              if (hasBasicPermissions && isOwner) {
                taskRepository.update(editTask, taskToEdit)
              } else {
                Future.successful(None)
              }
            }
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def removeTask(taskId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            task <- taskRepository.findById(taskId) failOnNone util.NotFound(s"Task with [id = $taskId]")
            _ <- if (hostUser.user_id == task.ownerId) taskRepository.deleteById(taskId) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def submitContribution() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val newContributionFormat = Json.reads[NewContribution]
          val newContribution = request.body.asJson.get.as[NewContribution]

          for {
            subspaceProject <- projectRepository.findByProjectId(newContribution.projectId) failOnNone util.NotFound(s"Project with [id = ${newContribution.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            maybeMana <- userManaRepository.findByUserId(hostUser.user_id)
            userMana = if (maybeMana.isDefined) maybeMana.get else UserMana(hostUser.user_id, DynamicsAndRatingService.maxMana)
            hasEnoughMana = newContribution.manaUsed <= userMana.mana
            maybeAddedContribution <- if (hasBasicPermissions && hasEnoughMana) contributionService.submitNewContribution(newContribution) else Future.successful(None)
            _ <- if (hasBasicPermissions && hasEnoughMana) userManaRepository.insertOrUpdate(userMana.copy(mana = userMana.mana - newContribution.manaUsed)) else Future.successful({})
            _ <- if (hasBasicPermissions && hasEnoughMana) dynamicsAndRatingService.evaluateContributions(Set(maybeAddedContribution.get.id)) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def editContribution(contributionId: Int) = { // the contribution id param is not actually used here
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val editContributionFormat = Json.reads[EditContribution]
          val editContribution = request.body.asJson.get.as[EditContribution]

          // TODO: check that all the edits are valid

          for {
            subspaceProject <- projectRepository.findByProjectId(editContribution.projectId) failOnNone util.NotFound(s"Project with [id = ${editContribution.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            contributionToEdit <- contributionRepository.findById(editContribution.id) failOnNone util.NotFound(s"Contribution with [id = ${editContribution.id}]") // FIXME: this logic might be better suited for the ContributionService
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            isOwner = hostUser.user_id == editContribution.userId
            contributionId <- {
              if (hasBasicPermissions && isOwner) {
                contributionRepository.update(editContribution, contributionToEdit)
              } else {
                Future.successful(None)
              }
            }
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def removeContribution(contributionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            contribution <- contributionRepository.findById(contributionId) failOnNone util.NotFound(s"Contribution with [id = ${contributionId}]")
            _ <- if (hostUser.user_id == contribution.userId) contributionRepository.deleteById(contributionId) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getProjectContributions(projectId: Int) = {
    implicit val contributionFormat = Json.writes[Contribution]
    implicit val userFormat = Json.writes[User]

    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            hostUser <- hostUserRepository.findByToken(token) failOnNone util.NotFound(s"HostUser with [token = $token]")
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            contributionsAndUsers <- {
              if (hasBasicPermissions) {
                contributionRepository.getAllByProjectId_withUsers(projectId)
              } else {
                Future.successful(Seq.empty[(Contribution, User)])
              }
            }
            contributionWires <- Future.sequence(contributionsAndUsers.map(x => {
              val (c, u) = x
              for {
                showInQueue <- contributionService.userShouldReview(u.id, c.id) // TODO: find a better/faster way to do this, maybe through joins in slick/sql to do all contributions at once
                maybeApproval <- backRepository.findByUserIdAndContributionId(u.id, c.id)
              } yield c.toWire(u.toWire(), Some(showInQueue), maybeApproval)
            }))
          } yield {
            Ok(Json.toJson(contributionWires))
          }
        }
        case None => {
          val token = defaultToken

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            contributionsAndUsers <- {
              if (isPublic) {
                contributionRepository.getAllByProjectId_withUsers(projectId)
              } else {
                Future.successful(Seq.empty[(Contribution, User)])
              }
            }
            contributionWires <- Future.sequence(contributionsAndUsers.map(x => {
              val (c, u) = x
              for {
                showInQueue <- contributionService.userShouldReview(u.id, c.id) // TODO: find a better/faster way to do this, maybe through joins in slick/sql to do all contributions at once
                maybeApproval <- backRepository.findByUserIdAndContributionId(u.id, c.id)
              } yield c.toWire(u.toWire(), Some(showInQueue), maybeApproval)
            }))
          } yield {
            Ok(Json.toJson(contributionWires))
          }
        }
      }
    })
  }

  def getContributionDetails(projectId: Int, contributionIid: Int) = {
    implicit val contributionFormat = Json.writes[Contribution]

    authorizedAction.async(implicit request => {
      val token = if (request.maybeHostUser.isDefined) request.maybeHostUser.get.host_user_token else defaultToken

      val maybeHostUser = request.maybeHostUser

      for {
        subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
        hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)

        gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
        isPublic = gitlabProject.getVisibility.toString == "public"

        isAllowed = hasBasicPermissions || isPublic
        contributionAndUser <- {
          if (isAllowed) {
            contributionRepository.findByProjectIdAndIid_withUsers(projectId, contributionIid)
          } else {
            Future.successful(None)
          }
        } failOnNone util.NotFound(s"Contribution with [projectId = $projectId, contributionIid = $contributionIid]")
        maybeUserRep <- userReputationRepository.findByUserId(contributionAndUser._2.id)
        userRepVal = if (maybeUserRep.isDefined) maybeUserRep.get.reputation else 0
        contributionId = contributionAndUser._1.id
        maybeApproval <- if (maybeHostUser.isDefined) backRepository.findByUserIdAndContributionId(maybeHostUser.get.user_id, contributionId) else Future.successful(None)
        supportersAndReps <- backRepository.findUsersByContributionId(contributionId)
        supporterWires = supportersAndReps.map(x => {
          val user = x._1
          val userRep = x._2
          user.toWire(maybeRep = Some(userRep.reputation))
        })
        contributionWire = contributionAndUser._1.toWire(contributionAndUser._2.toWire(Some(userRepVal)), approval = maybeApproval, supporters = supporterWires)
      } yield {
        Ok(Json.toJson(contributionWire))
      }
    })
  }

  def newSuggestion() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val newSuggestionFormat = Json.reads[NewSuggestion]
          val newSuggestion = request.body.asJson.get.as[NewSuggestion]

          for {
            subspaceProject <- projectRepository.findByProjectId(newSuggestion.projectId) failOnNone util.NotFound(s"Project with [id = ${newSuggestion.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            maybeMana <- userManaRepository.findByUserId(hostUser.user_id)
            userMana = if (maybeMana.isDefined) maybeMana.get else UserMana(hostUser.user_id, DynamicsAndRatingService.maxMana)
            hasEnoughMana = newSuggestion.manaUsed <= userMana.mana
            userAtSuggestionLimit <- suggestionRepository.isUserAtSuggestionLimit(hostUser.user_id, newSuggestion.contId)
            canMakeSuggestion = hasBasicPermissions && hasEnoughMana && !userAtSuggestionLimit
            _ <- if (canMakeSuggestion) suggestionRepository.create(newSuggestion.toSuggestion()) else Future.successful({})
            _ <- if (canMakeSuggestion) userManaRepository.insertOrUpdate(userMana.copy(mana = userMana.mana - newSuggestion.manaUsed)) else Future.successful({})
            _ <- if (canMakeSuggestion) dynamicsAndRatingService.evaluateContributions(Set(newSuggestion.contId)) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def removeSuggestion(suggestionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          for {
            suggestion <- suggestionRepository.findById(suggestionId) failOnNone util.NotFound(s"Suggestion with [suggestionId = $suggestionId]")
            _ <- if (hostUser.user_id == suggestion.userId) suggestionRepository.deleteById(suggestionId) else Future.successful({})
            _ <- if (hostUser.user_id == suggestion.userId) dynamicsAndRatingService.evaluateContributions(Set(suggestion.contId)) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getSuggestionList(projectId: Int) = { // FIXME: add [contributionIid: Int] to method params to limit response size?
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            suggestionUserList <- {
              if (hasBasicPermissions) {
                suggestionRepository.getSuggestionList_withUsers(subspaceProject.id)
              } else {
                Future.successful(Seq.empty[(Suggestion, User, Option[UserReputation])])
              }
            }
            suggestionWireList = suggestionUserList.map(x => {
              val repVal = if (x._3.isDefined) x._3.get.reputation else 0
              x._1.toWire(x._2.toWire(maybeRep = Some(repVal)))
            })
          } yield {
            Ok(Json.toJson(suggestionWireList))
          }
        }
        case None => {
          val token = defaultToken

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            suggestionUserList <- {
              if (isPublic) {
                suggestionRepository.getSuggestionList_withUsers(subspaceProject.id)
              } else {
                Future.successful(Seq.empty[(Suggestion, User, Option[UserReputation])])
              }
            }
            suggestionWireList = suggestionUserList.map(x => {
              val repVal = if (x._3.isDefined) x._3.get.reputation else 0
              x._1.toWire(x._2.toWire(maybeRep = Some(repVal)))
            })
          } yield {
            Ok(Json.toJson(suggestionWireList))
          }
        }
      }
    })
  }

  def newSuggestionComment() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val newSuggestionCommentFormat = Json.reads[NewSuggestionComment]
          val newSuggestionComment = request.body.asJson.get.as[NewSuggestionComment]

          for {
            suggestion <- suggestionRepository.findById(newSuggestionComment.suggestionId) failOnNone util.NotFound(s"Suggestion with [id = ${newSuggestionComment.suggestionId}]")
            subspaceProject <- projectRepository.findByProjectId(suggestion.projectId) failOnNone util.NotFound(s"Project with [id = ${suggestion.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            suggestionComment <- if (hasBasicPermissions) suggestionCommentRepository.create(newSuggestionComment.toSuggestionComment()) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getSuggestionCommentList(suggestionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          for {
            suggestion <- suggestionRepository.findById(suggestionId) failOnNone util.NotFound(s"Suggestion with [id = ${suggestionId}]")
            subspaceProject <- projectRepository.findByProjectId(suggestion.projectId) failOnNone util.NotFound(s"Project with [id = ${suggestion.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            suggestionCommentList_withUsers <- {
              if (hasBasicPermissions) {
                suggestionCommentRepository.getSuggestionCommentList_withUsers(suggestion.id)
              } else {
                Future.successful(Seq.empty[(SuggestionComment, User)])
              }
            }
            suggestionCommentWires = suggestionCommentList_withUsers
              .map(x => x._1.toWire(x._2))
              .sortWith((a, b) => a.comment.createdAt.before(b.comment.createdAt))
          } yield {
            Ok(Json.toJson(suggestionCommentWires))
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getProjectUsers(projectId: Int) = {
    authorizedAction.async(implicit request => {
      val token = if (request.maybeHostUser.isDefined) request.maybeHostUser.get.host_user_token else defaultToken

      for {
        subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
        projectUsers <- projectUserRepository.findByProjectId(subspaceProject.id)
        usersAndReps <- userRepository.findByIds_withReps(projectUsers.map(_.user_id).toSet)
        userWires = usersAndReps.map(uar => {
          val (user, maybeUserRep) = uar
          if (maybeUserRep.isDefined) {
            user.toWire(Some(maybeUserRep.get.reputation))
          } else {
            user.toWire()
          }
        })
        hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)

        gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
        isPublic = gitlabProject.getVisibility.toString == "public"

        isAllowed = hasBasicPermissions || isPublic
      } yield {
        if (isAllowed) {
          Ok(Json.toJson(userWires))
        } else {
          Results.Unauthorized
        }
      }
    })
  }

  def addProjectUser() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {

          implicit val timestampFormat = TimestampFormat

          implicit val hostProjectFormat = Json.reads[HostProject]
          val hostProject = request.body.asJson.get.as[HostProject]

          val host = hostProject.host
          val hostProjectId = hostProject.hostProjectId

          for {
            host <- hostRepository.findByName(host) failOnNone util.NotFound(s"Host with [id = $hostUser.host_id]")
            subspaceProject <- projectRepository.findByHostIdAndHostProjectId(host.id, hostProjectId) failOnNone util.NotFound(s"Project with [host = $host, id = $hostProjectId]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(hostUser.host_user_token, subspaceProject.host_project_id)
            _ <- {
              if (hasBasicPermissions) {
                projectUserRepository.addProjectUser(subspaceProject.id, hostUser.user_id)
              } else {
                Future.successful(Seq.empty[SuggestionComment])
              }
            }
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def onboardRepo(host: String, hostProjectId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            gitlabProject <- gitlabProjectService.getProjectById(token, hostProjectId) failOnNone util.NotFound(s"Project with [host = $host, id = $hostProjectId]")
            gitlabUser <- gitlabUserService.getUserByToken(token) failOnNone util.NotFound(s"GitlabUser with [token = $token]")
            permissionsMaintainerOrAbove <- permissionsService.gitlabIsMaintainerOrAbove(token, gitlabProject.getId)
            isPublic = gitlabProject.getVisibility.toString == "public"
            subspaceProject <- if (permissionsMaintainerOrAbove) projectRepository.createIfNotExists(Project(0, hostUser.host_id, gitlabProject.getId, hostUser.host_user_id, gitlabProject.getNamespace.getName, gitlabProject.getName, gitlabProject.getWebUrl, isPublic)) else Future.successful(null)

            _ <- if (permissionsMaintainerOrAbove) projectSettingsRepository.insertOrUpdate(ProjectSettings(subspaceProject.id, 0.85, 3 * 24 * 60 * 60, 14 * 24 * 60 * 60, 28 * 24 * 60 * 60)) else Future.successful({})
            _ <- if (permissionsMaintainerOrAbove) projectUserRepository.addProjectUser(subspaceProject.id, hostUser.user_id) else Future.successful({})
          } yield {
            if (permissionsMaintainerOrAbove) {
              Ok(Json.toJson(subspaceProject))
            } else {
              Ok(Json.toJson(ErrorWire(s"User [${gitlabUser.getUsername}] does not have necessary 'maintainer' permissions to onboard [${gitlabProject.getNameWithNamespace}]")))
            }
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def newApproval() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat

          implicit val newApprovalFormat = Json.reads[NewBack]
          val newApproval = request.body.asJson.get.as[NewBack].toBack()

          for {
            subspaceProject <- projectRepository.findByProjectId(newApproval.projectId) failOnNone util.NotFound(s"Project with [id = ${newApproval.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            maybeMana <- userManaRepository.findByUserId(hostUser.user_id)
            userMana = if (maybeMana.isDefined) maybeMana.get else UserMana(hostUser.user_id, DynamicsAndRatingService.maxMana)
            hasEnoughMana = newApproval.manaUsed <= userMana.mana
            _ <- if (hasBasicPermissions && hasEnoughMana) {
              backRepository.create(newApproval)
            } else Future.successful({})
            _ <- if (hasBasicPermissions && hasEnoughMana) userManaRepository.insertOrUpdate(userMana.copy(mana = userMana.mana - newApproval.manaUsed)) else Future.successful({})
            _ <- if (hasBasicPermissions && hasEnoughMana) dynamicsAndRatingService.evaluateContributions(Set(newApproval.contId)) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def editApproval(contributionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          ???
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def removeApproval(contributionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          for {
            _ <- backRepository.deleteByUserIdAndContributionId(hostUser.user_id, contributionId)
            _ <- dynamicsAndRatingService.evaluateContributions(Set(contributionId))
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getApproval(contributionId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            contribution <- contributionRepository.findById(contributionId) failOnNone util.NotFound(s"Contribution with [id = ${contributionId}]")
            subspaceProject <- projectRepository.findByProjectId(contribution.projectId) failOnNone util.NotFound(s"Project with [id = ${contribution.projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            approval <- backRepository.findByUserIdAndContributionId(hostUser.user_id, contributionId)
          } yield {
            if (hasBasicPermissions) {
              Ok(Json.toJson(approval))
            } else {
              Ok("")
            }
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getSuggestionCommits(projectId: Int, contributionId: Int, suggestionId: Int, branch: String) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            suggestion <- suggestionRepository.findById(suggestionId) failOnNone util.NotFound(s"Suggestion with [suggestionId = $suggestionId]")
            timeWhenSuggested = Date.from(suggestion.createdAt.toInstant)
            commits <- gitlabCommitsService.getCommits(token, subspaceProject.host_project_id, Some(branch), Some(timeWhenSuggested))
          } yield {
            if (hasBasicPermissions) {
              val commitWires = commits.map(c => CommitWire.toWire(c))
              Ok(Json.toJson(commitWires))
            } else {
              Ok("")
            }
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def suggestionAction(suggestionId: Int, action: String) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            suggestion <- suggestionRepository.findById(suggestionId) failOnNone util.NotFound(s"Suggestion with [suggestionId = $suggestionId]")
            contribution <- contributionRepository.findById(suggestion.contId) failOnNone util.NotFound(s"Suggestion with [contId = ${suggestion.contId}]")
            _ <- suggestionService.takeSuggestionAction(hostUser, suggestion, contribution, action)
          } yield {
            Ok("")
          }
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  def getProjectSettings(projectId: Int) = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasBasicPermissions <- permissionsService.hasBasicPermissions(token, subspaceProject.host_project_id)
            projectSettings <- projectSettingsRepository.findByProjectId(projectId)
          } yield {
            if (hasBasicPermissions) {
              Ok(Json.toJson(projectSettings))
            } else {
              Ok("")
            }
          }
        }
        case None => {
          val token = defaultToken

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            gitlabProject <- gitlabProjectService.getProjectById(token, subspaceProject.host_project_id) failOnNone util.NotFound(s"GitLab Project with [host_project_id = ${subspaceProject.host_project_id}]")
            isPublic = gitlabProject.getVisibility.toString == "public"
            projectSettings <- projectSettingsRepository.findByProjectId(projectId)
          } yield {
            if (isPublic) {
              Ok(Json.toJson(projectSettings))
            } else {
              Ok("")
            }
          }
        }
      }
    })
  }

  def editProjectSettings() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          implicit val timestampFormat = TimestampFormat
          implicit val projectSettingsFormat = Json.reads[ProjectSettings]
          val newProjectSettings = request.body.asJson.get.as[ProjectSettings]

          val projectId = newProjectSettings.project_id

          for {
            subspaceProject <- projectRepository.findByProjectId(projectId) failOnNone util.NotFound(s"Project with [id = ${projectId}]")
            hasMaintainerPermissions <- permissionsService.isMaintainerOrAbove(token, subspaceProject.host_project_id)
            _ <- if (hasMaintainerPermissions) projectSettingsRepository.insertOrUpdate(newProjectSettings) else Future.successful({})
          } yield Ok("")
        }
        case None => {
          Future.successful(Results.Unauthorized)
        }
      }
    })
  }

  //  def websocket() = WebSocket.acceptOrResult[String, String] { request =>
  //    implicit val websocketTopicsServiceImplicit: WebsocketTopicsService = websocketTopicsService
  //
  //    val maybeToken = request.headers.get(AuthTokenHeader)
  //
  //    if (maybeToken.isDefined){
  //      val token = trimToken(maybeToken.get)
  //
  //      for {
  //        user <- userService.getOrCreateUserFromToken(token) failOnNone util.NotFound(s"User with [token = $token]")
  //      } yield Right(ActorFlow.actorRef(out => WebsocketActor.props(out, user)))
  //    } else {
  //      Future.successful(Left(Results.Unauthorized))
  //    }
  //  }

  def syncToken() = authorizedAction.async(implicit request => {
    Future.successful(Ok(""))
  })

  def isTOSAccepted() = {
    authorizedAction.async(implicit request => {
      request.maybeHostUser match {
        case Some(hostUser: HostUser) => {
          val token = hostUser.host_user_token

          for {
            maybeGitlabUser <- gitlabUserService.getUserByToken(token)
          } yield {
            if (maybeGitlabUser.isDefined) {
              Ok(Json.toJson(Tos(true)))
            } else {
              Ok(Json.toJson(Tos(false)))
            }
          }
        }
        case None => {
          Future(Ok(Json.toJson(Tos(true))))
        }
      }
    })

    //    Action.async { implicit request =>
    //      val token = trimToken(request.headers.get(AuthTokenHeader).get)
    //
    //      for {
    //        maybeGitlabUser <- gitlabUserService.getUserByToken(token)
    //      } yield {
    //        if (maybeGitlabUser.isDefined){
    //          Ok(Json.toJson(Tos(true)))
    //        } else {
    //          Ok(Json.toJson(Tos(false)))
    //        }
    //      }
    //    }
  }

  def healthCheck() = Action { implicit request =>
    Ok("")
  }

}