package services

import java.io.File

import com.google.inject.Inject
import models.util
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.merge.MergeStrategy
import org.eclipse.jgit.transport._
import play.api.libs.ws.WSClient
import utils.Logger
import utils.cats.custom.implicits._
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class JGitService @Inject()(ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  val origin = "origin"

  private def cloneOrUpdateAll(gitUrl: String, projectId: String, checkoutBranch: String)(implicit token: String): Future[Option[Git]] ={
    val tokenProvider = new UsernamePasswordCredentialsProvider( "oauth2", token)
    val gitDir = new File(localGitPath(projectId))

    Future {
      val tryClone = Try {
        if (!gitDir.exists()) {
          Git.cloneRepository()
            .setURI(gitUrl)
            .setDirectory(gitDir)
            .setCloneAllBranches(true)
            .setCredentialsProvider(tokenProvider)
            .setBranch(s"refs/heads/$checkoutBranch")
            .call()
        } else {
          val git = Git.open(gitDir)

          git.fetch()
            .setCredentialsProvider(tokenProvider)
            .setRemote(origin)
            .setRefSpecs(new RefSpec(s"refs/heads/*:refs/remotes/$origin/*"))
            .call()

          val branchList = git.branchList().call().asScala.map(_.getName)

          git.checkout()
            .setCreateBranch(!branchList.contains(s"refs/heads/$checkoutBranch"))
            .setName(checkoutBranch)
            .call()

          git.pull()
            .setCredentialsProvider(tokenProvider)
            .setRemote(origin)
            .call()

          git
        }
      }

      if (tryClone.isFailure){
        logger.error(tryClone.failed.get.getMessage)
      }

      tryClone.toOption
    }
  }

  private def cloneOrUpdateAllWithDeleteRetry(gitUrl: String, projectId: String, checkoutBranch: String)(implicit token: String): Future[Option[Git]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [gitUrl: $gitUrl, projectId: $projectId, checkoutBranch: $checkoutBranch]"
  ) {
    for {
      maybeGit <- cloneOrUpdateAll(gitUrl, projectId, checkoutBranch)
      _ <- if (maybeGit.isEmpty){
        Future {
          println(s"Failed pull update with [gitUrl = $gitUrl, projectId = $projectId, checkoutBranch = $checkoutBranch] - going to delete directory and retry")
          val gitDir = new File(localGitPath(projectId))
          deleteRecursively(gitDir)
        }
      } else Future.successful({})
      probablyGit <- if (maybeGit.isEmpty) cloneOrUpdateAll(gitUrl, projectId, checkoutBranch) else Future.successful(maybeGit)
    } yield probablyGit
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def merge(
             gitUrl: String,
             projectId: String,
             parentBranch: String,
             childBranchWithChanges: String
           )(implicit token: String): Future[Unit] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [gitUrl: $gitUrl, projectId: $projectId, parentBranch: $parentBranch, childBranchWithChanges: $childBranchWithChanges]"
  ) {
    val tokenProvider = new UsernamePasswordCredentialsProvider( "oauth2", token)

    for {
      git <- cloneOrUpdateAllWithDeleteRetry(gitUrl, projectId, parentBranch) failOnNone util.NotFound(s"Contribution with [gitUrl = $gitUrl, projectId: $projectId, parentBranch: $parentBranch]")
      changes = git
        .getRepository
        .findRef(s"refs/remotes/origin/$childBranchWithChanges")
        .getObjectId
      _ <- Future {
        git.merge()
          .include(changes)
          .setCommit(true)
          .setStrategy(MergeStrategy.THEIRS)
          .call()
      }
      _ <- Future {
        git.push()
          .setForce(true)
          .setRemote(origin)
          .setCredentialsProvider(tokenProvider)
          .call()
      }
    } yield {}
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def rebase(
               gitUrl: String,
               projectId: String,
               parentBranchWithChanges: String,
               childBranch: String
             )(implicit token: String): Future[Unit] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [gitUrl: $gitUrl, projectId: $projectId, parentBranchWithChanges: $parentBranchWithChanges, childBranch: $childBranch]"
  ) {
    val tokenProvider = new UsernamePasswordCredentialsProvider( "oauth2", token)

    for {
      git <- cloneOrUpdateAllWithDeleteRetry(gitUrl, projectId, childBranch) failOnNone util.NotFound(s"Contribution with [gitUrl = $gitUrl, projectId: $projectId, childBranch: $childBranch]")
      _ <- Future {
        git
          .rebase()
          .setUpstream(s"origin/$parentBranchWithChanges")
          .call()
      }
      _ <- Future {
        git.push()
          .setForce(true)
          .setRemote(origin)
          .setCredentialsProvider(tokenProvider)
          .call()
      }
    } yield {}
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  private def localGitPath(projectId: String): String ={
    s"/repos/$projectId"
  }

  private def deleteRecursively(file: File): Unit = {
    if (file.isDirectory) {
      file.listFiles.foreach(deleteRecursively)
    }
    if (file.exists && !file.delete) {
      throw new Exception(s"Unable to delete ${file.getAbsolutePath}")
    }
  }

}