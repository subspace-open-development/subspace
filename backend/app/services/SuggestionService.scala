package services

import com.google.inject.Inject
import models.enums.{Hosts, SuggestionActions, SuggestionState}
import models.util.NotFound
import models.{Contribution, HostUser, Suggestion, util}
import org.gitlab4j.api.models.{User => GitlabUser}
import play.api.libs.ws.WSClient
import repositories.{HostRepository, HostUserRepository, SuggestionRepository, UserRepository}
import services.gitlab.GitlabUserService
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

case class SuggestionService @Inject()(userRepository: UserRepository,
                                     hostRepository: HostRepository,
                                     hostUserRepository: HostUserRepository,
                                     suggestionRepository: SuggestionRepository,
                                     gitlabUserService: GitlabUserService,
                                     ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  def takeSuggestionAction(hostUser: HostUser, suggestion: Suggestion, contribution: Contribution, action: String): Future[Boolean] = withLogging (
    infoIn = s"About to [${funcName()}] in [${className()}] with [hostUser: $hostUser]"
  ) {
    action match {
      case SuggestionActions.ADDRESS => {
        if (hostUser.user_id == contribution.userId && suggestion.status == SuggestionState.SUGGESTED){
          for {
            _ <- suggestionRepository.changeStatus(suggestion, SuggestionState.ADDRESSED)
          } yield true
        } else {
          Future.successful(false)
        }
      }
      case SuggestionActions.ACCEPT => {
        if (hostUser.user_id == suggestion.userId && suggestion.status == SuggestionState.ADDRESSED){
          for {
            _ <- suggestionRepository.changeStatus(suggestion, SuggestionState.ACCEPTED)
          } yield true
        } else {
          Future.successful(false)
        }
      }
      case _ => Future.successful(false)
    }
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}