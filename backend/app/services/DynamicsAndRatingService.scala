package services

import java.sql.Timestamp

import com.google.inject.Inject
import models._
import models.enums.{ContributionState, ReputationChangeTypes, SuggestionState}
import play.api.libs.ws.WSClient
import repositories.{BackRepository, ContributionRepository, HostRepository, HostUserRepository, ProjectRepository, ProjectReputationRepository, ProjectSettingsRepository, ReputationChangeRepository, SuggestionRepository, TaskRepository, UserManaChangeRepository, UserRepository, UserReputationRepository}
import services.DynamicsAndRatingService.ContributionEvaluation
import services.gitlab.{GitlabRepositoryService, GitlabUserService}
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

object DynamicsAndRatingService {
  val maxMana: Double             = 325
  val manaRechargePerHour: Double = 1.0
  val repChangeRate: Double       = 1.0
  val repMean: Double             = 1000
  val lowerRepStart: Double       = 100

  val calcRepNormScaleMaxLatency: Long = 60 * 1000

  var latestRepNormScale: Double = 1.0
  var latestRepNormScaleUpdatedAt: Long = System.currentTimeMillis()

  case class ContributionEvaluation(currentState: String, secondsUntilChange: Option[Long], stateAfterChange: Option[String])
}

case class DynamicsAndRatingService @Inject()(userRepository: UserRepository,
                                              gitlabUserService: GitlabUserService,
                                              hostUserRepository: HostUserRepository,
                                              taskRepository: TaskRepository,
                                              reputationChangeRepository: ReputationChangeRepository,
                                              contributionRepository: ContributionRepository,
                                              backRepository: BackRepository,
                                              suggestionRepository: SuggestionRepository,
                                              projectRepository: ProjectRepository,
                                              projectSettingsRepository: ProjectSettingsRepository,
                                              projectReputationRepository: ProjectReputationRepository,
                                              userReputationRepository: UserReputationRepository,
                                              userManaChangeRepository: UserManaChangeRepository,
                                              gitlabRepositoryService: GitlabRepositoryService,
                                              jGitService: JGitService,
                                              ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  def evaluateContributions(contributionIds: Set[Int] = Set.empty[Int]): Future[Seq[ContributionEvaluation]] = withLogging (
    debugIn = s"About to [${funcName()}] in [${className()}]"
  ) {
    for {
      undecidedContributions <- {
        if (contributionIds.nonEmpty){
          contributionRepository.findAllByIds(contributionIds)
        } else {
          contributionRepository.getAllUndecided()
        }
      }
      contributionEvaluations <- Future.sequence(undecidedContributions.map(c => determineContributionEvaluation(c.id)))
      acceptedCnE = undecidedContributions.zip(contributionEvaluations).filter(_._2.currentState == ContributionState.ACCEPTED)
      _ <- Future.sequence(
        acceptedCnE.map(cce => {
          val cont = cce._1
          val ce = cce._2
          handleAcceptedContributionBranchChanges(cont, ce)
        })
      )
    } yield {
      contributionEvaluations
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def determineContributionEvaluation(contributionId: Int): Future[ContributionEvaluation] = withLogging (
    debugIn = s"About to [${funcName()}] in [${className()}] with [contributionId = $contributionId]"
  ) {
    for {
      contribution <- contributionRepository.findById(contributionId) failOnNone util.NotFound(s"Contribution with [id = $contributionId]")
      project <- projectRepository.findByProjectId(contribution.projectId) failOnNone util.NotFound(s"Project with [projectId = ${contribution.projectId}]")
      backs <- backRepository.getContributionBacks(contributionId)
      suggestions <- suggestionRepository.getByContributionId(contributionId)
      maybeProjectReputation <- projectReputationRepository.findByProjectId(contribution.projectId)
      projectReputation = maybeProjectReputation.getOrElse(ProjectReputation(contribution.projectId, 0))
      projectSettings <- projectSettingsRepository.findByProjectId(contribution.projectId) failOnNone util.NotFound(s"ProjectSettings with [projectId = ${contribution.projectId}]")
      serviceAccountHostUser <- hostUserRepository.findByHostAndHostId(project.host_id, project.host_user_admin_id)  failOnNone util.NotFound(s"HostUser with [host_id = ${project.host_id}, host_user_id = ${project.host_user_admin_id}]")
      branch <- gitlabRepositoryService.getBranch(serviceAccountHostUser.host_user_token, project.host_project_id, contribution.branch) failOnNone util.NotFound(s"Branch with [projectId = ${contribution.projectId}, branch = ${contribution.branch} ]")

      // FIXME: we probably want the startThreshold to be determined when the contribution is submitted... otherwise the sliding window will change as time goes by and probably confuse people
      // FIXME: saving the startThreshold initially will also save us from having to compute it every time...
      startThreshold <- getStartingContributionThreshold(contribution.id)

      secondsSinceLastCommit  = Math.round((System.currentTimeMillis().toDouble - branch.getCommit.getCreatedAt.getTime.toDouble) / 1000.0).toInt
      contributionEvaluation  = evaluateContributionState(contribution, backs, suggestions, projectSettings, secondsSinceLastCommit, contribution.getSecondsSinceCreated(), startThreshold)
      repChanges              = couldBuildReputationChanges(contribution, backs, suggestions, contributionEvaluation, projectReputation)

      // Save reputation changes
      _ <- Future.sequence(repChanges.map(r => reputationChangeRepository.create(r))) // TODO: we can probably find a faster way to do this in batch...

      // Make sure no reputations are less than 0
      _ <- reputationChangeRepository.zeroAllUserReputationChanges(repChanges.filter(_.is_user).map(_.user_id).toSet)
      _ <- reputationChangeRepository.zeroBottonProjectReputationChanges(contribution.projectId)

      // refresh the fast-access user and project reputations
      userIds = (backs.map(_.userId) ++ suggestions.map(_.userId) ++ Seq(contribution.userId)).toSet
      _ <- Future.sequence(userIds.map(id => getAccurateUserRating(id)))
      _ <- getAccurateProjectRating(contribution.projectId)

      _ <- contributionRepository.setEvaluation(contribution, contributionEvaluation)

    } yield {
      logger.debug(contributionEvaluation.toString())
      logger.debug(repChanges.toString())
      contributionEvaluation
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  import DynamicsAndRatingService._
  import ContributionState._

  private def couldBuildReputationChanges(
                               contribution: Contribution,
                               backs: Seq[Back],
                               suggestions: Seq[Suggestion],
                               contributionEvaluation: ContributionEvaluation,
                               projectReputation: ProjectReputation
                             ): Seq[ReputationChange] ={

    if (shouldChangeReputations(contribution, contributionEvaluation)){

      val timestamp = new Timestamp(System.currentTimeMillis())

      val outcome = contributionEvaluation.currentState

      val supporterPoints = backs
        .map(b => getVotePointsFromManaAndRep(b.manaUsed, b.userRep))
        .sum

      val feedbackPoints = suggestions
        .filter(_.status != SuggestionState.ACCEPTED)
        .map(s => getVotePointsFromManaAndRep(s.manaUsed, s.userRep))
        .sum

      val totalPoints = Math.max(supporterPoints + feedbackPoints, 1.0)

      val acceptScore   = if (outcome == ContributionState.ACCEPTED) 1 else 0
      val declineScore  = if (outcome == ContributionState.DECLINED) 1 else 0

      val (contributorRepChange, projectRepChange) = reputationChanges(contribution.userRep, projectReputation.reputation, acceptScore, declineScore, totalPoints)

      val backsSorted = backs.sortWith((a, b) => a.createdAt.before(b.createdAt))
      val suggestionsSorted = suggestions.sortWith((a, b) => a.createdAt.before(b.createdAt))

      val contributorContributionRepChangeObj  = ReputationChange(0, true, false, contribution.projectId, contribution.id, None, contribution.userId, contributorRepChange, totalPoints, ReputationChangeTypes.CONTRIBUTION, outcome, None, timestamp)
      val projectContributionRepChangeObj      = ReputationChange(0, false, true, contribution.projectId, contribution.id, None, contribution.userId, projectRepChange, totalPoints, ReputationChangeTypes.CONTRIBUTION, outcome, None, timestamp)

      var effectivePoints = totalPoints
      val supporterRepChanges = backsSorted.flatMap(b => {
        val tempEffectivePoints = Math.max(effectivePoints * 0.5, 1.0)
        effectivePoints = tempEffectivePoints

        val (supporterRepChange, projectRepChange) = reputationChanges(b.userRep, projectReputation.reputation, acceptScore, declineScore, effectivePoints)

        val supporterRepChangeObj = ReputationChange(0, true, false, contribution.projectId, contribution.id, None, b.userId, supporterRepChange, effectivePoints, ReputationChangeTypes.SUPPORT, outcome, None, timestamp)
        val projectRepChangeObj   = ReputationChange(0, false, true, contribution.projectId, contribution.id, None, b.userId, projectRepChange, effectivePoints, ReputationChangeTypes.SUPPORT, outcome, None, timestamp)

        Seq(supporterRepChangeObj, projectRepChangeObj)
      })

      val suggesterContributorRepChanges = suggestionsSorted.flatMap(s => {
        val maybeScorePair = {
          if (outcome == ContributionState.ACCEPTED){
            if (s.status == SuggestionState.SUGGESTED){
              Some((0, 1))
            } else if (s.status == SuggestionState.ADDRESSED){
              Some((1, 0))
            } else if (s.status == SuggestionState.ACCEPTED){
              Some((1, 0))
            } else {
              None
            }
          } else {
            if (s.status == SuggestionState.SUGGESTED){
              None
            } else if (s.status == SuggestionState.ADDRESSED){
              Some((1, 0))
            } else if (s.status == SuggestionState.ACCEPTED){
              Some((1, 0))
            } else {
              None
            }
          }
        }

        if (maybeScorePair.isDefined){
          val (suggesterScore, contributorScore) = maybeScorePair.get
          val points = getVotePointsFromManaAndRep(s.manaUsed, s.userRep)
          val (suggesterRepChange, contributorRepChange) = reputationChanges(s.userRep, contribution.userRep, suggesterScore, contributorScore, points)

          val supporterRepChangeObj   = ReputationChange(0, true, false, contribution.projectId, contribution.id, Some(s.id), s.userId, suggesterRepChange, points, ReputationChangeTypes.SUGGESTION, outcome, Some(s.status), timestamp)
          val contributorRepChangeObj = ReputationChange(0, true, false, contribution.projectId, contribution.id, Some(s.id), contribution.userId, contributorRepChange, points, ReputationChangeTypes.SUGGESTION, outcome, Some(s.status), timestamp)

          Seq(supporterRepChangeObj, contributorRepChangeObj)
        } else {
          Seq.empty
        }
      })

      val suggesterProjectRepChanges = suggestionsSorted.flatMap(s => {
        val maybeScorePair = {
          if (outcome == ContributionState.ACCEPTED){
            if (s.status == SuggestionState.SUGGESTED){
              Some((0, 1))
            } else if (s.status == SuggestionState.ADDRESSED){
              Some((0, 1))
            } else if (s.status == SuggestionState.ACCEPTED){
              Some((1, 0))
            } else {
              None
            }
          } else {
            if (s.status == SuggestionState.SUGGESTED){
              Some((1, 0))
            } else if (s.status == SuggestionState.ADDRESSED){
              Some((1, 0))
            } else if (s.status == SuggestionState.ACCEPTED){
              None
            } else {
              None
            }
          }
        }

        if (maybeScorePair.isDefined){
          val (suggesterScore, projectScore) = maybeScorePair.get
          val points = getVotePointsFromManaAndRep(s.manaUsed, s.userRep)
          val (suggesterRepChange, projectRepChange) = reputationChanges(s.userRep, projectReputation.reputation, suggesterScore, projectScore, points)

          val supporterRepChangeObj = ReputationChange(0, true, false, contribution.projectId, contribution.id, Some(s.id), s.userId, suggesterRepChange, points, ReputationChangeTypes.SUGGESTION, outcome, Some(s.status), timestamp)
          val projectRepChangeObj   = ReputationChange(0, false, true, contribution.projectId, contribution.id, Some(s.id), s.userId, projectRepChange, points, ReputationChangeTypes.SUGGESTION, outcome, Some(s.status), timestamp)

          Seq(supporterRepChangeObj, projectRepChangeObj)
        } else {
          Seq.empty
        }
      })

      val allReputationChanges = Seq(contributorContributionRepChangeObj, projectContributionRepChangeObj) ++ supporterRepChanges ++ suggesterContributorRepChanges ++ suggesterProjectRepChanges

      allReputationChanges
    } else {
      Seq.empty[ReputationChange]
    }
  }


  private def shouldChangeReputations(contribution: Contribution, contributionEvaluation: ContributionEvaluation): Boolean ={
    val stateChanged = contribution.state != contributionEvaluation.currentState
    val nowClosed = contributionEvaluation.currentState == ACCEPTED || contributionEvaluation.currentState == DECLINED

    stateChanged && nowClosed
  }

  private def evaluateContributionState(
                                 contribution: Contribution,
                                 backs: Seq[Back],
                                 suggestions: Seq[Suggestion],
                                 projectSettings: ProjectSettings,
                                 secondsSinceLastCommit: Int,
                                 secondsSinceCreated: Int,
                                 startThreshold: Double
                               ): ContributionEvaluation ={

    logger.debug(s"#### secondsSinceLastCommit $secondsSinceLastCommit")
    logger.debug(s"#### secondsSinceCreated $secondsSinceCreated")
    logger.debug(s"#### startThreshold $startThreshold")

    val thresholdHalflifeSeconds = projectSettings.threshold_halflife_seconds
    val supportProportionRequired = projectSettings.support_proportion_required
    val secondsFromCommitUntilDecline = projectSettings.seconds_from_commit_until_decline
    val secondsFromStartUntilDecline = projectSettings.seconds_from_start_until_decline

    logger.debug(s"#### thresholdHalflifeSeconds $thresholdHalflifeSeconds")
    logger.debug(s"#### supportProportionRequired $supportProportionRequired")
    logger.debug(s"#### secondsFromCommitUntilDecline $secondsFromCommitUntilDecline")
    logger.debug(s"#### secondsFromStartUntilDecline $secondsFromStartUntilDecline")

    val contributorPoints = getVotePointsFromManaAndRep(contribution.manaUsed, contribution.userRep)

    logger.debug(s"#### contributorPoints $contributorPoints")

    val supporterPoints = backs
      .map(b => getVotePointsFromManaAndRep(b.manaUsed, b.userRep))
      .sum

    logger.debug(s"#### supporterPoints $supporterPoints")

    val feedbackPoints = suggestions
      .filter(_.status != SuggestionState.ACCEPTED)
      .map(s => getVotePointsFromManaAndRep(s.manaUsed, s.userRep))
      .sum

    logger.debug(s"#### feedbackPoints $feedbackPoints")

    val supportPoints = contributorPoints + supporterPoints
    val totalPoints = supportPoints + feedbackPoints

    logger.debug(s"#### totalPoints $totalPoints")

    val approvalProportion = supportPoints / totalPoints

    logger.debug(s"#### approvalProportion $approvalProportion")

    val currentThreshold = getCurrentContributionThreshold(startThreshold, thresholdHalflifeSeconds, secondsSinceLastCommit, secondsSinceCreated)

    logger.debug(s"#### currentThreshold $currentThreshold")

    val passingThreshold = totalPoints >= currentThreshold
    val passingApproval = supportProportionRequired <= approvalProportion

    val passingRecentCommitDuration = secondsSinceLastCommit < secondsFromCommitUntilDecline
    val passingAbsoluteDuration = secondsSinceCreated < secondsFromStartUntilDecline

    val passingDurations = passingRecentCommitDuration && passingAbsoluteDuration

    logger.debug(s"#### passingThreshold $passingThreshold")
    logger.debug(s"#### passingApproval $passingApproval")
    logger.debug(s"#### passingRecentCommitDuration $passingRecentCommitDuration")
    logger.debug(s"#### passingAbsoluteDuration $passingAbsoluteDuration")
    logger.debug(s"#### passingDurations $passingDurations")

    val contributionEvaluation = {
      if (passingThreshold && passingApproval){
        // approved
        ContributionEvaluation(ACCEPTED, None, None)

      } else if (passingThreshold && !passingApproval && passingDurations){
        // pending
        val secondsLeft = Math.min(secondsFromCommitUntilDecline - secondsSinceLastCommit, secondsFromStartUntilDecline - secondsSinceCreated)
        ContributionEvaluation(PENDING, Some(secondsLeft), Some(DECLINED))

      } else if (passingThreshold && !passingApproval && !passingDurations){
        // declined
        ContributionEvaluation(DECLINED, None, None)

      } else if (!passingThreshold && passingApproval){
        // will be approved @ ...
        val secondsLeft = findTimeLeft(startThreshold, thresholdHalflifeSeconds, secondsSinceLastCommit, secondsSinceCreated, totalPoints)
        ContributionEvaluation(OPEN, Some(Math.round(secondsLeft)), Some(ACCEPTED))

      } else if (!passingThreshold && !passingApproval){
        // will be pending @ ...
        val secondsLeft = findTimeLeft(startThreshold, thresholdHalflifeSeconds, secondsSinceLastCommit, secondsSinceCreated, totalPoints)
        ContributionEvaluation(OPEN, Some(Math.round(secondsLeft)), Some(PENDING))

      } else {
        throw new Exception("This code can't be reached [RatingService::evaluateContributionState]")
      }
    }

    contributionEvaluation
  }

  private def findTimeLeft(startThreshold: Double, halflifeSeconds: Int, secondsSinceLastCommit: Int, secondsSinceCreated: Int, currentPoints: Double): Double ={
    val totalTime = (Math.log(currentPoints / startThreshold) / Math.log(0.5)) * halflifeSeconds

    val effectiveSeconds = Math.min(secondsSinceLastCommit, secondsSinceCreated)

    Math.max(totalTime - effectiveSeconds, 0)
  }

  private def getStartingContributionThreshold(contributionId: Int): Future[Double] ={
    for {
      backs <- getUniqueActiveUserRepsNormalizedPastWeek(contributionId)
      sum = backs.map(r => getMaxVotePointsFromManaAndRep(r)).sum
    } yield {
      Math.max(
        sum,
        getMaxVotePointsFromManaAndRep(lowerRepStart) // if there hasn't been any activity, pretend that there is at least 1 average user
      )
    }
  }

  private def getCurrentContributionThreshold(startThreshold: Double, halflifeSeconds: Int, secondsSinceLastCommit: Int, secondsSinceCreated: Int): Double ={
    val effectiveSeconds = Math.min(secondsSinceLastCommit, secondsSinceCreated)
    startThreshold * Math.pow(0.5, effectiveSeconds.toDouble / halflifeSeconds.toDouble)
  }

  private def getUniqueActiveUserRepsNormalizedPastWeek(contributionId: Int): Future[Seq[Double]] ={
    val oneWeek: Long = 7 * 24 * 60 * 60 * 1000
    val oneWeekAgoEpoch = System.currentTimeMillis() - oneWeek

    for {
      backs <- backRepository.findByContributionIdAndDate(contributionId, new Timestamp(oneWeekAgoEpoch))
      backMap = backs.map(b => (b.userId, b.userRep)).toMap
    } yield {
      backMap.values.toSeq
    }
  }

  private def getVotePointsFromManaAndRep(manaUsed: Double, reputation: Double): Double ={
    val scaledRep = Math.max(reputation, lowerRepStart) / repMean
    Math.sqrt(manaUsed * 2.0) * scaledRep
  }

  // this assumes all users have the same max mana and recharge rates
  private def getMaxVotePointsFromManaAndRep(reputation: Double): Double = getVotePointsFromManaAndRep(maxMana, reputation)

  def getAccurateUserRating(userId: Int): Future[Double] = withLogging (
    debugIn = s"About to [${funcName()}] in [${className()}] with [userId: $userId]"
  ) {
    for {
      repChanges <- reputationChangeRepository.findUserChangesByUserId(userId)
      rawUserRep = repChanges.map(_.rep_change).sum
      repNormScale <- getUserRepNormScale()
      userRating = (rawUserRep * repNormScale)
      _ <- {
        if (userRating < 0) {
          reputationChangeRepository.zeroBottomUserReputationChanges(userId)
        } else {
          userReputationRepository.insertOrUpdate(UserReputation(userId, userRating))
        }
      }
    } yield {
      if (userRating < 0) 0 else userRating
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def getAccurateProjectRating(projectId: Int): Future[Double] = withLogging (
    debugIn = s"About to [${funcName()}] in [${className()}] with [projectId: $projectId]"
  ) {
    for {
      repChanges <- reputationChangeRepository.findProjectChangesByProjectId(projectId)
      rawProjectRep = repChanges.map(_.rep_change).sum
//      repNormScale <- getUserRepNormScale() // TODO? not sure if we want to normalize project ratings, if so we have to create their own getUserRepNormScale function
//      projectRating = rawProjectRep * repNormScale
      projectRating = rawProjectRep
      _ <- {
        if (projectRating < 0) {
          reputationChangeRepository.zeroBottonProjectReputationChanges(projectId)
        } else {
          projectReputationRepository.update(ProjectReputation(projectId, rawProjectRep))
        }
      }
    } yield {
      if (projectRating < 0) 0 else projectRating
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  private def getUserRepNormScale(): Future[Double] = withLogging (
    debugIn = s"About to [${funcName()}] in [${className()}]"
  ) {
    val currentTime = System.currentTimeMillis()
    val upToDate = if (currentTime - latestRepNormScaleUpdatedAt > calcRepNormScaleMaxLatency) false else true

    if (upToDate){
      Future.successful(latestRepNormScale)
    } else {
      for {
        maybeTotalReputation <- reputationChangeRepository.getUserRepChangeSum
        realUsers <- userReputationRepository.numRealUsers(lowerRepStart)
        avgRep = maybeTotalReputation.getOrElse(0.0) / Math.max(realUsers.toDouble, 1)
        repNormScale = if (avgRep > lowerRepStart) repMean / avgRep else 1.0
      } yield {
        latestRepNormScale = repNormScale
        latestRepNormScaleUpdatedAt = System.currentTimeMillis()
        repNormScale
      }
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  // Given two reputations, what is their expectation of winning?
  private def expectedScores(p1Rep: Double, p2Rep: Double): (Double, Double) = {

    def model(p1R: Double, p2R: Double): Double = {
      1 / (1 + Math.pow(2.0, (p2R - p1R) / repMean))
    }

    val e1 = model(p1Rep, p2Rep)
    val e2 = model(p2Rep, p1Rep)

    (e1, e2)
  }

  // Given two reputations, two resulting scores, and some parameters, what are the reputation changes for two entities?
  private def reputationChanges(p1Rep: Double, p2Rep: Double, p1Score: Double, p2Score: Double, contributionValue: Double): (Double, Double) = {
    assert(Math.abs(p1Score + p2Score - 1) < 0.01, "p1Score and p2Score must sum to 1")

    val (e1, e2) = expectedScores(p1Rep, p2Rep)

    val p1Change = contributionValue * repChangeRate * (p1Score - e1)
    val p2Change = contributionValue * repChangeRate * (p2Score - e2)

    (p1Change, p2Change)
  }

  def handleAcceptedContributionBranchChanges(contribution: Contribution, contEval: ContributionEvaluation): Future[Unit] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [ContributionEvaluation: $contEval]"
  ) {
    for {
      task <- taskRepository.findById(contribution.taskId) failOnNone util.NotFound(s"Task with [id = ${contribution.taskId}]")
      subspaceProject <- projectRepository.findByProjectId(contribution.projectId) failOnNone util.NotFound(s"Project with [id = ${contribution.projectId}]")
      serviceAdmin <- hostUserRepository.findByHostAndHostId(subspaceProject.host_id, subspaceProject.host_user_admin_id) failOnNone util.NotFound(s"HostUser with [host_id = ${subspaceProject.host_id}, id = ${subspaceProject.host_user_admin_id}]")
      _ <- jGitService.merge(
        subspaceProject.web_url,
        subspaceProject.id.toString,
        task.branch.get,
        contribution.branch
      )(serviceAdmin.host_user_token)
      _ <- handleTaskVertex(subspaceProject, task.id, Set.empty[Int])(serviceAdmin.host_user_token)
    } yield {}
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  private def handleTaskVertex(subspaceProject: Project, taskId: Int, alreadyHandledTasks: Set[Int])(implicit token: String): Future[Set[Int]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [taskId: $taskId]"
  ) {
    if (!alreadyHandledTasks.contains(taskId)){
      for {
        task <- taskRepository.findById(taskId) failOnNone util.NotFound(s"Task with [id = $taskId]")
        maybeParentTask <- if (task.parentId.isDefined) taskRepository.findById(task.parentId.get) else Future.successful(None)
        childrenTasks <- taskRepository.findChildrenOfTaskWithId(taskId)
        filteredChildrenTasks = childrenTasks.filter(t => !alreadyHandledTasks.contains(t.id))
        shouldMergeUp = task.autoMergeUp && maybeParentTask.isDefined && !alreadyHandledTasks.contains(maybeParentTask.get.id)
        shouldRebaseDown = task.autoRebase && filteredChildrenTasks.nonEmpty

        _ <- if (shouldMergeUp) {
          jGitService.merge(
            subspaceProject.web_url,
            subspaceProject.id.toString,
            maybeParentTask.get.branch.get,
            task.branch.get
          )
        } else Future.successful({})

        _ <- if (shouldRebaseDown) {
          Future.sequence(
            filteredChildrenTasks.map(ct => {
              jGitService.rebase(
                subspaceProject.web_url,
                subspaceProject.id.toString,
                task.branch.get,
                ct.branch.get
              )
            })
          )
        } else Future.successful({})

        upHandled <- if (shouldMergeUp) handleTaskVertex(subspaceProject, maybeParentTask.get.id, alreadyHandledTasks ++ Seq(taskId)) else Future.successful(Seq.empty[Int])

        downHandledSeq <- if (shouldRebaseDown){
          Future.sequence(
            filteredChildrenTasks.map(ct => {
              handleTaskVertex(subspaceProject, ct.id, alreadyHandledTasks ++ Seq(taskId))
            })
          )
        } else Future.sequence(Seq(Future.successful(Seq.empty[Int])))

        downHandled = downHandledSeq.flatten

      } yield alreadyHandledTasks ++ upHandled ++ downHandled
    } else {
      Future.successful(alreadyHandledTasks)
    }
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}