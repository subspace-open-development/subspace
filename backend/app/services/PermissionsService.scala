package services

import com.google.inject.Inject
import models._
import models.enums.Hosts
import models.util.NotFound
import play.api.libs.ws.WSClient
import org.gitlab4j.api.models.{Project => GitlabProject}
import repositories.{HostRepository, HostUserRepository, UserRepository}
import services.gitlab.{GitlabGroupService, GitlabProjectService, GitlabUserService}
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}
import play.api.{Configuration, Logger => PlayLogger}

case class PermissionsService @Inject()( userRepository: UserRepository,
                                         hostRepository: HostRepository,
                                         hostUserService: HostUserService,
                                         hostUserRepository: HostUserRepository,
                                         gitlabUserService: GitlabUserService,
                                         gitlabGroupService: GitlabGroupService,
                                         gitlabProjectService: GitlabProjectService,
                                         ws: WSClient)(implicit ec: ExecutionContext) extends Logger {

  val playLogger = PlayLogger("application")

  val GITLAB_ADMIN      = 60
  val GITLAB_OWNER      = 50
  val GITLAB_MAINTAINER = 40
  val GITLAB_DEVELOPER  = 30
  val GITLAB_REPORTER   = 20
  val GITLAB_GUEST      = 10
  val GITLAB_NO_ACCESS  = 0

  def hasBasicPermissions(token: String, hostProjectId: Int): Future[Boolean] = withLogging(
    debugIn = s"About to [${funcName()}] in [${className()}] with [token: $token, hostProjectId: $hostProjectId]"
  ) {
    val host = Hosts.GITLAB

    host match {
      case Hosts.GITLAB => {
        for {
          gitlabPermissionLevel <- getGitlabPermissionLevel(token, hostProjectId)
        } yield gitlabPermissionLevel > GITLAB_NO_ACCESS
      }
      case Hosts.GITHUB => Future.failed(NotFound("GitHub OAuth not yet supported"))
      case Hosts.BITBUCKET => Future.failed(NotFound("BitBucket OAuth not yet supported"))
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def isMaintainerOrAbove(token: String, hostProjectId: Int): Future[Boolean] = withLogging(
    debugIn = s"About to [${funcName()}] in [${className()}] with [token: $token, hostProjectId: $hostProjectId]"
  ) {
    val host = Hosts.GITLAB

    host match {
      case Hosts.GITLAB => gitlabIsMaintainerOrAbove(token, hostProjectId)
      case Hosts.GITHUB => Future.failed(NotFound("GitHub OAuth not yet supported"))
      case Hosts.BITBUCKET => Future.failed(NotFound("BitBucket OAuth not yet supported"))
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def gitlabIsMaintainerOrAbove(token: String, gitlabProjectId: Int): Future[Boolean] = withLogging(
    debugIn = s"About to [${funcName()}] in [${className()}] with [token: $token, gitlabProjectId: $gitlabProjectId]"
  ) {
    for {
      gitlabPermissionsLevel <- getGitlabPermissionLevel(token, gitlabProjectId)
    } yield gitlabPermissionsLevel >= GITLAB_MAINTAINER
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def getGitlabPermissionLevel(token: String, gitlabProjectId: Int): Future[Int] = withLogging(
    debugIn = s"About to [${funcName()}] in [${className()}] with [token: $token, gitlabProjectId: $gitlabProjectId]"
  ) {
    for {
      gitlabProject <- gitlabProjectService.getProjectById(token, gitlabProjectId) failOnNone util.NotFound(s"Project with [gitlabProjectId = $gitlabProjectId]")
      gitlabUser <- gitlabUserService.getUserByToken(token) failOnNone util.NotFound(s"GitlabUser with [token = $token]")
      groupAccessLevel <- gitlabGroupService.getAccessLevel(token, gitlabProject.getNamespace.getPath, gitlabUser.getId) // FIXME: could someone make a subgroup with the same name as a top-level group to trick this function?
      projectAccessLevel <- gitlabProjectService.getAccessLevel(token, gitlabProject.getId, gitlabUser.getId)
      ownerAccessLevel = if (gitlabProject.getCreatorId == gitlabUser.getId) Some(GITLAB_OWNER) else None
      noAccessLevel = Some(GITLAB_NO_ACCESS)
      highestAccessLevel = Seq(ownerAccessLevel, groupAccessLevel, projectAccessLevel, noAccessLevel).flatten.max
    } yield {
      highestAccessLevel
    }
  }(
    debugOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}