package services

import com.google.inject.Inject
import models.{HostUser, User, util}
import models.enums.Hosts
import org.gitlab4j.api.models.{User => GitlabUser}
import models.util.NotFound
import play.api.libs.ws.WSClient
import repositories.{HostRepository, HostUserRepository, UserRepository}
import services.gitlab.GitlabUserService
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

case class HostUserService @Inject()(userRepository: UserRepository,
                                     hostRepository: HostRepository,
                                     hostUserRepository: HostUserRepository,
                                     gitlabUserService: GitlabUserService,
                                     ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  def getOrCreateHostUserFromToken(access_token: String): Future[HostUser] = withLogging(
    debugIn = s"About to [getOrCreateUserFromToken] in [UserService] with [access_token: $access_token]"
  ) {
    val host = Hosts.GITLAB

    host match {
      case Hosts.GITLAB => {
        for {
          gitlabUser <- gitlabUserService.getUserByToken(access_token) failOnNone NotFound(s"GitlabUser with [token = $access_token]")
          host <- hostRepository.findByName(Hosts.GITLAB) failOnNone NotFound(s"Host with [name = ${Hosts.GITLAB}]")
          maybeHostUser <- hostUserRepository.findByHostAndHostId(host.id, gitlabUser.getId)
          hostUser <- {
            if (maybeHostUser.isDefined && maybeHostUser.get.host_user_token == access_token) {
              Future.successful(maybeHostUser.get)
            } else if (maybeHostUser.isDefined && maybeHostUser.get.host_user_token != access_token) {
              updateHostUserToken(gitlabUser, access_token)
            } else {
              createUserAndHostUserFromGitlabUserAndToken(gitlabUser, access_token)
            }
          }
          _ <- userRepository.findById(hostUser.user_id)
        } yield hostUser
      }
      case Hosts.GITHUB => Future.failed(NotFound("GitHub OAuth not yet supported"))
      case Hosts.BITBUCKET => Future.failed(NotFound("BitBucket OAuth not yet supported"))
    }
  }(
    debugOut = result => s"Successfully [getOrCreateUserFromToken] in [UserService]",
    errorOut = error => s"Failed to [getOrCreateUserFromToken] in [UserService]. Reason: [$error]",
  )

  def createUserAndHostUserFromGitlabUserAndToken(gitlabUser: GitlabUser, token: String): Future[HostUser] = withLogging (
    infoIn = s"About to [${funcName()}] in [${className()}] with [gitlabUser: $gitlabUser]"
  ) {
    for {
      user <- userRepository.create(gitlabUser.getUsername, gitlabUser.getEmail)
      host <- hostRepository.findByName(Hosts.GITLAB) failOnNone NotFound(s"Host with [name = ${Hosts.GITLAB.toString}]")
      hostUser <- hostUserRepository.create(user.id, host.id, gitlabUser.getId, gitlabUser.getEmail, gitlabUser.getUsername, token)
    } yield hostUser
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def updateHostUserToken(gitlabUser: GitlabUser, token: String): Future[HostUser] = withLogging (
    infoIn = s"About to [${funcName()}] in [${className()}] with [gitlabUser: $gitlabUser]"
  ) {
    for {
      host <- hostRepository.findByName(Hosts.GITLAB) failOnNone NotFound(s"Host with [name = ${Hosts.GITLAB.toString}]")
      oldHostUser <- hostUserRepository.findByHostAndHostId(host.id, gitlabUser.getId) failOnNone util.NotFound(s"HostUser with [token = $token]")
      newHostUser <- hostUserRepository.updateToken(oldHostUser, token)
    } yield newHostUser
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}