package services

import com.google.inject.Inject
import models._
import models.enums.{ContributionState, SuggestionState}
import play.api.libs.ws.WSClient
import repositories._
import services.DynamicsAndRatingService.ContributionEvaluation
import services.gitlab.{GitlabGroupService, GitlabUserService}
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

case class ContributionService @Inject()(userRepository: UserRepository,
                                 hostRepository: HostRepository,
                                 contributionRepository: ContributionRepository,
                                 hostUserService: HostUserService,
                                 hostUserRepository: HostUserRepository,
                                 gitlabUserService: GitlabUserService,
                                 gitlabGroupService: GitlabGroupService,
                                 projectRepository: ProjectRepository,
                                 taskRepository: TaskRepository,
                                 backRepository: BackRepository,
                                 suggestionRepository: SuggestionRepository,
                                 dynamicsAndRatingService: DynamicsAndRatingService,
                                 jGitService: JGitService,
                                 ws: WSClient)(implicit ec: ExecutionContext) extends Logger {

  def submitNewContribution(newContribution: NewContribution): Future[Option[Contribution]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [newContribution: $newContribution]"
  ) {
    val contribution = newContribution.toContribution()

    for {
      // fixme: not sure if these comments apply to contributions (copied from tasks)
//       see if the contribution parameters are valid
//
//       see if the above task can be added to
//
//       see if the user has appropriate reputation permissions
//
//       find the parent task branch and copy it for this new task

      // get latest accurate contributor user rep
      userRep <- dynamicsAndRatingService.getAccurateUserRating(newContribution.userId)

      // add the new contribution to the database
      addedContribution <- contributionRepository.create(contribution.copy(userRep = userRep))
    } yield Some(addedContribution)
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def userShouldReview(userId: Int, contributionId: Int): Future[Boolean] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [contributionId: $contributionId, userId: $userId]"
  ) {
    for {
      approvalUpToDate <- isApprovalUpToDate(userId, contributionId)
      suggestions <- suggestionRepository.findByContributionAndUser(userId, contributionId)
      unaddressedSuggestionExists = suggestions.exists(s => s.status == SuggestionState.SUGGESTED)
      // if an approval has been addressed, then the user should check to see if it is satisfactory, and if the user has accepted a suggestion, then they might want to approve the contribution.
      // the one possibility here is that the user might be waiting on the completed work prompted from another suggestion, but, that shouldn't be their concern.
    } yield !approvalUpToDate && !unaddressedSuggestionExists
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def isApprovalUpToDate(userId: Int, contributionId: Int): Future[Boolean] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [contributionId: $contributionId]"
  ) {
    for {
      maybeApproval <- backRepository.findByUserIdAndContributionId(userId, contributionId)
      contribution <- contributionRepository.findById(contributionId) failOnNone util.NotFound(s"Contribution with [contributionId = $contributionId]")
    } yield {
      if (maybeApproval.isEmpty){
        false
      } else {
        val approval = maybeApproval.get
        approval.commit == contribution.commit || approval.maintain_approval
      }
    }
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}