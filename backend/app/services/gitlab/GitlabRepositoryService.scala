package services.gitlab

import com.google.inject.Inject
import models.util.AlreadyExists
import org.gitlab4j.api.{GitLabApiException, RepositoryApi}
import org.gitlab4j.api.models.Branch
import utils.Logger

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

class GitlabRepositoryService @Inject()(gitlabUserService: GitlabUserService)(implicit val executionContext: ExecutionContext) extends Logger{

//  def init(gitlabProject: Project): Future[RepositoryFile] = createFileInRepository(
//    gitlabProject.getId, "master", "Initial commit", "README.md",
//    content = ""
//  )
//
//  def createFileInRepository(projectId: Integer, branch: String, commitMessage: String,
//                             filePath: String, content: String): Future[RepositoryFile] = gitLabService.asRoot {
//    api =>
//      val repositoryFile = new RepositoryFile()
//      repositoryFile.setFilePath(filePath)
//      repositoryFile.setContent(content)
//      api.getRepositoryFileApi.createFile(
//        projectId,
//        repositoryFile,
//        branch,
//        commitMessage
//      )
//  }

  def createBranch(projectId: Int, newBranchName: String, createFromBranch: String)(implicit token: String): Future[Branch] = withRepositoryApi(token) {
    _.createBranch(projectId, newBranchName, createFromBranch)
  }

//    .recoverWith {
//    case error: GitLabApiException if error.getMessage == "Branch already exists" ||
//      error.getHttpStatus == 400 => Future.failed(AlreadyExists(s"Branch with name $newBranchName, already exists"))
//  }

//  def getNumberOfCommitsByBranch(projectId: Integer, ref: String): Future[Long] = gitLabService.asUser {
//    _.getCommitsApi.getCommits(projectId, ref, null, null, 1).getTotalItems.longValue
//  }

  def getBranch(token: String, projectId: Int, branchName: String): Future[Option[Branch]] = {
    withRepositoryApi(token)(
      gitlabApi => {
        Some(gitlabApi.getBranch(projectId, branchName))
      }).recover {
        case ex: GitLabApiException if ex.getMessage == "404 Project Not Found" => None
      }
  }

  def getBranches(token: String, projectId: Int): Future[Seq[Branch]] = withRepositoryApi(token) {
    _.getBranches(projectId)
  }

  private def withRepositoryApi[T](gitlabAccessToken: String)
                               (doWithRepositoryApi: RepositoryApi => T): Future[T] = gitlabUserService.asUser(gitlabAccessToken) {
    gitlabApi => Future(doWithRepositoryApi(gitlabApi.getRepositoryApi))
  }

  implicit def javaList2ScalaList[T](list: java.util.List[T]): List[T] = list.asScala.toList
}
