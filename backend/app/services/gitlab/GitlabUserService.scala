package services.gitlab

import com.google.inject.Inject
import models.User
import models.enums.Hosts
import models.util.NotFound
import org.gitlab4j.api.Constants.TokenType
import org.gitlab4j.api.GitLabApi.ApiVersion
import org.gitlab4j.api.models.{User => GitlabUser}
import org.gitlab4j.api.{GitLabApi, GitLabApiException}
import play.api.libs.ws.WSClient
import repositories.{HostUserRepository, UserRepository}
import services.config.GitlabConfigService
import utils.Logger
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

case class GitlabUserService @Inject()(gitlabConfig: GitlabConfigService,
                                       hostUserRepository: HostUserRepository,
                                       userRepository: UserRepository,
                                       ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  def getUserByToken(accessToken: String): Future[Option[GitlabUser]] = asUser(accessToken) {
    gitlabApi => {
      Future(Some(gitlabApi.getUserApi.getCurrentUser)).recover {
        case ex: GitLabApiException if ex.getMessage == "401 Unauthorized" => {
          logger.logger.info(ex.getMessage)
          None
        }
        case ex: GitLabApiException if (ex.getMessage.contains("403") || ex.getMessage.contains("must accept the Terms of Service")) => {
          logger.logger.info(ex.getMessage)
          None
        }
      }
    }
  }

  def asUser[T](accessToken: String)(doAsUser: GitLabApi => Future[T]): Future[T] = doAsUser {
    new GitLabApi(ApiVersion.V4, gitlabConfig.url, TokenType.OAUTH2_ACCESS, accessToken)
  }


}

