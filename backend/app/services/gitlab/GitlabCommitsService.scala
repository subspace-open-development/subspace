package services.gitlab

import java.util.Date

import com.google.inject.Inject
import org.gitlab4j.api.models.{Commit, CommitAction, Diff}
import org.gitlab4j.api.{CommitsApi, GitLabApiException, Pager}
import utils.Logger

import scala.language.implicitConversions
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

class GitlabCommitsService @Inject()(gitLabService: GitlabUserService)(implicit val executionContext: ExecutionContext) extends Logger {

//  def getCommit(projectId: Long, sha: String): Future[Commit] = withLogging(
//    infoIn = s"About to get a commit. Params: projectId = $projectId, sha = $sha"
//  ) {
//    withCommitsApi {
//      _.getCommit(projectId.toInt, sha)
//    }
//  }(
//    infoOut = result => s"Successfully gotten a commit. Result: $result",
//    errorOut = error => s"Failed to get a commit. Reason: $error"
//  )
//
//  def getCommits(projectId: Int, ref: String, itemsPerPage: Int): Future[Pager[Commit]] = withLogging(
//    infoIn = s"About to get commits. Params: projectId: '$projectId', ref: '$ref', itemsPerPage: '$itemsPerPage'"
//  )(withCommitsApi{
//      _.getCommits(projectId, ref, null, null, itemsPerPage)
//    })(
//    infoOut = result => s"Successfully gotten commits. Result: '$result'",
//    errorOut = error => s"Failed to get commits. Reason: $error"
//  )
//
//  def getCommits(projectId: Long, ref: String, path: String): Future[List[Commit]] = withLogging(
//    infoIn = s"About to get commits. Params: projectId = $projectId, ref = $ref, path = $path"
//  ) {
//    withCommitsApi {
//      _.getCommits(projectId.toInt, ref, path).asScala.toList
//    }
//  }(
//    infoOut = result => s"Successfully gotten commits. Result: $result",
//    errorOut = error => s"Failed to get commits. Reason: $error"
//  )

  def getCommits(token: String, projectId: Long, ref: Option[String] = None, since: Option[Date] = None, until: Option[Date] = None): Future[List[Commit]] = withCommitsApiAsUser(token) {
    commitsApi => commitsApi.getCommits(projectId.toInt, ref.orNull, since.orNull, until.orNull).asScala.toList
  }.recover {
    case ex: GitLabApiException if ex.getMessage == "404 Project Not Found" =>
      List.empty[Commit]
  }

//  //TODO use JGit implementation instead of it
//  def getInitialCommit(projectId: Int, branchName: String): Future[Option[Commit]] = withLogging(
//    infoIn = s"About to get initial commit. Params: projectId: $projectId, branchName = $branchName"
//  ) (withCommitsApi {
//      commitsApi =>
//        val commitsPager = commitsApi.getCommits(projectId, branchName, null, null, 1)
//        commitsPager.page(commitsPager.getTotalPages).headOption
//    })(
//    infoOut = result => s"Successfully executed 'getInitialCommit'. Result: $result",
//    errorOut = error => s"Failed to execute 'getInitialCommit'. Reason: $error"
//  )

//  def createCommit(projectId: Long, branch: String, commitMessage: String, startBranch: Option[String] = None, authorEmail: Option[String] = None,
//                   authorName: Option[String] = None, actions: List[CommitAction]): Future[Commit] = withLogging(
//    infoIn = s"About to create a commit. Params: projectId = $projectId, branch = $branch, commitMessage = $commitMessage," +
//      s"startBranch = $startBranch, authorEmail = $authorEmail, authorName = $authorName, actions = $actions"
//  ) {
//    withCommitsApi {
//      _.createCommit(
//        projectId.toInt,
//        branch,
//        commitMessage,
//        startBranch.orNull,
//        authorEmail.orNull,
//        authorName.orNull,
//        actions
//      )
//    }
//  }(
//    infoOut = result => s"Successfully created a commit. Result: $result",
//    errorOut = error => s"Failed to create a commit. Reason: $error"
//  )

  def createCommitAsUser(gitlabAccessToken: String, projectId: Long, branch: String, commitMessage: String, startBranch: Option[String] = None,
                         authorEmail: Option[String] = None, authorName: Option[String] = None, actions: List[CommitAction]): Future[Commit] = withLogging(
    infoIn = s"About to create a commit as User. Params: gitlabAccessToken = $gitlabAccessToken, projectId = $projectId, branch = $branch," +
      s"commitMessage = $commitMessage, startBranch = $startBranch, authorEmail = $authorEmail, authorName = $authorName, actions = $actions"
  ) {
    withCommitsApiAsUser(gitlabAccessToken) {
      _.createCommit(
        projectId.toInt,
        branch,
        commitMessage,
        startBranch.orNull,
        authorEmail.orNull,
        authorName.orNull,
        actions
      )
    }
  }(
    infoOut = result => s"Successfully created a commit as User. Result: $result",
    errorOut = error => s"Failed to create a commit as User. Reason: $error"
  )

//  def getDiff(projectId: Int, sha: String): Future[List[Diff]] = withLogging(
//    infoIn = s"About to get a commit differences. Params: projectId = $projectId, sha = $sha"
//  ) {
//    withCommitsApi {
//      _.getDiff(projectId, sha).asScala.toList
//    }
//  }(
//    infoOut = result => s"Successfully gotten commit differences. Result: $result",
//    errorOut = error => s"Failed to get commit differences. Reason: $error"
//  )

//  private def withCommitsApi[T](doWithCommitsApi: CommitsApi => T): Future[T] = gitLabService.asRoot {
//    gitlabApi => doWithCommitsApi(gitlabApi.getCommitsApi)
//  }

  private def withCommitsApiAsUser[T](gitlabAccessToken: String)
                                     (doWithCommitsApi: CommitsApi => T): Future[T] = gitLabService.asUser(gitlabAccessToken: String) {
    gitlabApi => Future(doWithCommitsApi(gitlabApi.getCommitsApi))
  }

  implicit def javaList2ScalaList[T](list: java.util.List[T]): List[T] = list.asScala.toList

  implicit def scalaList2JavaList[T](list: List[T]): java.util.List[T] = list.asJava
}
