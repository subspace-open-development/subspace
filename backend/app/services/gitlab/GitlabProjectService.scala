package services.gitlab

import com.google.inject.Inject
import models.util.AlreadyExists
import org.gitlab4j.api.models.{AccessLevel, Member, Project}
import org.gitlab4j.api.{GitLabApiException, ProjectApi}
import play.api.libs.ws.WSClient
import services.config.GitlabConfigService
import utils.{Logger, StringImplicits}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.Try

case class GitlabProjectService @Inject()(gitlabUserService: GitlabUserService,
                                          gitlabRepositoryService: GitlabRepositoryService,
                                          gitlabConfig: GitlabConfigService,
                                          ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger with StringImplicits {

  def getMyProjects(token: String): Future[List[Project]] = withProjectApi(token) {
    _.getMemberProjects(0, 1000)
  }

  def getProjects(token: String): Future[List[Project]] = withProjectApi(token) {
    _.getProjects
  }

  def getProjectUsers(token: String, projectId: Int): Future[Seq[Member]] = withProjectApi(token) {
    api => {
      val maybeMembers = Try(api.getMembers(projectId)).toOption
      if (maybeMembers.isDefined){
        maybeMembers.get.asScala
      } else {
        Seq.empty[Member]
      }
    }
  }

  def isMaintainerOrAbove(token: String, projectId: Int, userId: Int): Future[Option[Boolean]] = withProjectApi(token) {
    api => {
      val accessLevel = Try(api.getMember(projectId, userId).getAccessLevel.value).toOption
      if (accessLevel.isDefined && accessLevel.get >= gitlabConfig.MAINTAINER_ACCESS_LEVEL) Some(true) else None
    }
  }

  def getAccessLevel(token: String, projectId: Int, userId: Int): Future[Option[Int]] = withProjectApi(token) {
    api => {
      Try(api.getMember(projectId, userId).getAccessLevel.value).toOption.map(_.toInt)
    }
  }

  def addDeveloperToProject(token: String, hostUserId: Int, hostProjectId: Int): Future[Member] = withProjectApi(token) {
    _.addMember(hostProjectId, hostUserId, AccessLevel.DEVELOPER)
  }

  def getProjectsByCriteria(token: String, criteria: String): Future[List[Project]] = withProjectApi(token) {
    _.getProjects(criteria)
  }

  def createFork(token: String, projectId: Int): Future[Project] = gitlabUserService.asUser(token) {
    gitlabApi =>
      for {
        userName <- Future(gitlabApi.getUserApi.getCurrentUser.getUsername)
        project <- Future(gitlabApi.getProjectApi.forkProject(projectId, userName)).recoverWith {
          case ex: GitLabApiException if ex.getHttpStatus == 409 => Future.failed {
            AlreadyExists(s"User(username: $userName) already forked Project(id: $projectId)")
          }
        }
      } yield project
  }

  def getProjectById(token: String, id: Int): Future[Option[Project]] = withProjectApi(token) {
    projectApi => Some(projectApi.getProject(id))
  }.recover {
    case ex: GitLabApiException if ex.getMessage == "404 Project Not Found" =>
      None
  }

  def getProjectByNamespaceAndName(token: String, namespace: String, name: String): Future[Option[Project]] = withProjectApi(token) {
    projectApi => Some(projectApi.getProject(namespace, name))
  }.recover {
    case ex: GitLabApiException if ex.getMessage == "404 Project Not Found" =>
      None
  }

  def deleteProject(token: String, id: Int): Future[Unit] = withProjectApi(token) {
    _.deleteProject(id)
  }

  private def withProjectApi[T](gitlabAccessToken: String)
                               (doWithProjectApi: ProjectApi => T): Future[T] = gitlabUserService.asUser(gitlabAccessToken) {
    gitlabApi => Future(doWithProjectApi(gitlabApi.getProjectApi))
  }

  implicit def javaList2ScalaList[T](list: java.util.List[T]): List[T] = list.asScala.toList

}
