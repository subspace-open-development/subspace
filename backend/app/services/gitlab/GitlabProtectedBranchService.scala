package services.gitlab

import com.google.inject.Inject
import models.util.AlreadyExists
import org.gitlab4j.api.models.{Branch, Member, Project, ProtectedBranch}
import org.gitlab4j.api.{GitLabApiException, ProjectApi, ProtectedBranchesApi}
import play.api.libs.ws.WSClient
import services.config.GitlabConfigService
import utils.{Logger, StringImplicits}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.Try

case class GitlabProtectedBranchService @Inject()(gitlabUserService: GitlabUserService,
                                          gitlabRepositoryService: GitlabRepositoryService,
                                          gitlabConfig: GitlabConfigService,
                                          ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger with StringImplicits {

  def protectBranch(projectId: Int, branchName: String)(implicit token: String): Future[Option[ProtectedBranch]] = withProtectedBranchesApi(token) {
    api => Try(api.protectBranch(projectId, branchName)).toOption
  }.recoverWith {
    case error: GitLabApiException if error.getMessage == "Branch already exists" ||
      error.getHttpStatus == 400 => Future.failed(AlreadyExists(s"Branch with name $branchName, already exists"))
  }

  private def withProtectedBranchesApi[T](gitlabAccessToken: String)
                               (doWithProtectedBranchesApi: ProtectedBranchesApi => T): Future[T] = gitlabUserService.asUser(gitlabAccessToken) {
    gitlabApi => Future(doWithProtectedBranchesApi(gitlabApi.getProtectedBranchesApi))
  }

  implicit def javaList2ScalaList[T](list: java.util.List[T]): List[T] = list.asScala.toList

}
