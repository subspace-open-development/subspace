package services.gitlab

import com.google.inject.Inject
import models.Contribution
import models.webhooks.gitlab.PushWebhook
import repositories._
import utils.Logger
import scala.concurrent.{ExecutionContext, Future}

class GitlabWebhookService @Inject()(
                                      userRepo: UserRepository,
                                      contributionRepo: ContributionRepository,
                                      implicit val executionContext: ExecutionContext) extends Logger {

  def processPush(push: PushWebhook): Future[Contribution] = withLogging(
    infoIn = s"About to process the Gitlab PushWebhook"
  ) {
    ???
  }(
    infoOut = mr => s"Successfully processed the Gitlab PushWebhook",
    errorOut = error => s"Failed to xxx. Reason: ${error.getMessage}"
  )

}