package services.gitlab

import com.google.inject.Inject
import models.util.AlreadyExists
import org.gitlab4j.api.models.{Group, Member, Project}
import org.gitlab4j.api.{GitLabApiException, GroupApi, ProjectApi}
import play.api.libs.ws.WSClient
import services.config.GitlabConfigService
import utils.{Logger, StringImplicits}

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.Try

case class GitlabGroupService @Inject()(gitlabUserService: GitlabUserService,
                                        gitlabRepositoryService: GitlabRepositoryService,
                                        gitlabConfig: GitlabConfigService,
                                        ws: WSClient,
                                        implicit val executionContext: ExecutionContext) extends Logger with StringImplicits {

  def getMyGroups(token: String): Future[List[Group]] = withGroupApi(token) {
    _.getGroups
  }

  def isMaintainerOrAbove(token: String, groupPath: String, userId: Int): Future[Option[Boolean]] = withGroupApi(token) {
    api => {
      val accessLevel = Try(api.getMember(groupPath, userId).getAccessLevel.value).toOption
      if (accessLevel.isDefined && accessLevel.get >= gitlabConfig.MAINTAINER_ACCESS_LEVEL) Some(true) else None
    }
  }

  def getGroupUsers(token: String, groupPath: String): Future[Seq[Member]] = withGroupApi(token) {
    api => {
      val maybeMembers = Try(api.getMembers(groupPath)).toOption
      if (maybeMembers.isDefined){
        maybeMembers.get.asScala
      } else {
        Seq.empty[Member]
      }
    }
  }

  def getAccessLevel(token: String, groupPath: String, userId: Int): Future[Option[Int]] = withGroupApi(token) {
    api => {
      Try(api.getMember(groupPath, userId).getAccessLevel.value).toOption.map(_.toInt)
    }
  }

  private def withGroupApi[T](gitlabAccessToken: String)
                               (doWithGroupApi: GroupApi => T): Future[T] = gitlabUserService.asUser(gitlabAccessToken) {
    gitlabApi => Future(doWithGroupApi(gitlabApi.getGroupApi))
  }

  implicit def javaList2ScalaList[T](list: java.util.List[T]): List[T] = list.asScala.toList

}
