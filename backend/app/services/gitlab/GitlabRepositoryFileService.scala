package services.gitlab

import com.google.inject.Inject
import org.gitlab4j.api.models.RepositoryFile
import org.gitlab4j.api.{GitLabApiException, RepositoryFileApi}
import utils.Logger

import scala.concurrent.{ExecutionContext, Future}

class GitlabRepositoryFileService @Inject()(gitLabService: GitlabUserService)(implicit val executionContext: ExecutionContext) extends Logger {

  def getFile(path: String, projectId: Long, ref: String)(implicit token: String): Future[Option[RepositoryFile]] = withLogging(
    debugIn = s"About to get a [GITLAB] file. Params: path = $path, projectId = $projectId, ref = $ref"
  ) {
    withRepositoryFileApi(token) {
      repositoryFileApi => Some(repositoryFileApi.getOptionalFile(projectId.toInt, path, ref).get)
    }.recover {
      case ex: GitLabApiException if ex.getMessage == "404 File Not Found" => None
    }
  }(
    debugOut = result => s"Successfully gotten a [GITLAB] file. Result: $result",
    warnOut = _ => s"Can't find the file on [GITLAB] with path [$path], projectId [$projectId], ref [$ref]",
    errorOut = error => s"Failed to get a [GITLAB] file. Reason: [$error]",
  )

//  def createFile(projectId: Long, branch: String, commitMessage: String,
//                  filePath: String, content: String): Future[RepositoryFile] = withLogging(
//    infoIn = s"About to create a [GITLAB] file. Params: " +
//      s"projectId = $projectId, branch = $branch, commitsMessage = $commitMessage, filePath = $filePath, content = $content"
//  ) {
//    withRepositoryFileApi {
//      api =>
//        val repositoryFile = new RepositoryFile()
//        repositoryFile.setFilePath(filePath)
//        repositoryFile.setContent(content)
//        api.createFile(
//          projectId.toInt,
//          repositoryFile,
//          branch,
//          commitMessage
//        )
//    }
//  }(
//    infoOut = result => s"Successfully created a [GITLAB] file. Result: $result",
//    errorOut = error => s"Failed to create a [GITLAB] file. Reason: $error"
//  )

  private def withRepositoryFileApi[T](gitlabAccessToken: String)
                               (doWithRepositoryFileApi: RepositoryFileApi => T): Future[T] = gitLabService.asUser(gitlabAccessToken) {
    gitlabApi => Future(doWithRepositoryFileApi(gitlabApi.getRepositoryFileApi))
  }
}
