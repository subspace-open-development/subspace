package services.config

import com.google.inject.{Inject, Singleton}
import org.gitlab4j.api.GitLabApi

/**
  * A configuration service for reading the "gitlab" configuration
  *
  * @param envConfigService a service that reads configuration values from conf/application.conf, based on the current
  *                         environment
  */
@Singleton
class GitlabConfigService @Inject()(envConfigService: EnvConfigService) {

  val MAINTAINER_ACCESS_LEVEL = 40

  /**
    * All config keys for gitlab should start with the following value
    */
  val gitlabConfBaseKey: String = "gitlab"

  /**
    * Should return gitlab.users.root.email from application.conf
    */
  val defaultNamespace: String = s"$gitlabConfBaseKey.namespace"

  /**
    * All config keys for csrf should start with the following value
    */
  val csrfPrefix: String = s"$gitlabConfBaseKey.csrf"

  /**
    * All config keys for gitlab root user should start with the following value
    */
  val rootPrefix: String = s"$gitlabConfBaseKey.users.root"

  /**
    * Should return gitlab.users.root.email from application.conf
    */
  val rootEmail: String = envConfigService.readOrThrow(s"$rootPrefix.email")

  /**
    * Should return gitlab.users.root.password from application.conf
    */
  val rootPassword: String = envConfigService.readOrThrow(s"$rootPrefix.password")

  /**
    * Should return gitlab.url from application.conf
    */
  val url: String = envConfigService.readOrThrow(s"$gitlabConfBaseKey.url")

//  /**
//    * Should return gitlab.oauth.loginUrlSuffix from application.conf
//    */
//  val oauthLoginUrlSuffix: String = envConfigService.readOrThrow(s"$gitlabConfBaseKey.oauth.loginUrlSuffix")

//  val publicSshKey: String = envConfigService.readOrThrow(s"$gitlabConfBaseKey.publicSshKey")
//  val privateSshKey: String = envConfigService.readOrThrow(s"$gitlabConfBaseKey.privateSshKey")

  /**
    * Should return gitlab.csrf.tokenPatternStart from application.conf
    */
  val csrfTokenPatternStart: String = envConfigService.readOrThrow(s"$csrfPrefix.tokenPatternStart")

  /**
    * Should return gitlab.csrf.tokenPatternEnd from application.conf
    */
  val csrfTokenPatternEnd: String = envConfigService.readOrThrow(s"$csrfPrefix.tokenPatternEnd")

//  /**
//    * Should return gitlab.internalUrl from application.conf
//    */
//  val internalUrl: String = envConfigService.readOrThrow(s"$gitlabConfBaseKey.internalUrl")

  val gitlabApi: GitLabApi = {
    try {
      val api = GitLabApi.oauth2Login(url, rootEmail, rootPassword.toCharArray)
      GitlabConfigService.gitlabApi = api
      api
    } catch {
      case e: Exception => {
        GitlabConfigService.gitlabApi
      }
    }
  }

  object GitlabConfigService {
    var gitlabApi: GitLabApi = _
  }

}
