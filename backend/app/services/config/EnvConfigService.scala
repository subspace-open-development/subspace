package services.config

import com.google.inject.Inject
import play.api.Mode.{Dev, Prod, Test}
import play.api.{Configuration, Environment, Mode}

/**
  * Reads environment-dependent values from the conf/application.conf. <br/>
  * A conf value have its key as [baseKey].[environment].[keyOrKeys]. <br/>
  * <br/>
  * For example, if this service receives <i>"frontend.url"</i> then it will eventually check for:
  * <ul>
  *   <li><i>"frontend.dev.url"</i> for <b>Dev</b> mode </li>
  *   <li><i>"frontend.test.url"</i> for <b>Test</b> </li>
  *   <li><i>"frontend.prod.url"</i> for <b>Production</b> </li>
  *   <li>and <i>"frontend.url"</i> if none of the upper-mentioned was defined</li>
  * </ul>
  *
  *  <b>Please note, that the environment suffix (dev/test/prod) will be injected right after the first dot in the provided key</b>
  *
  * @param config Play configuration helper. Used to access conf/application.conf
  * @param env Play environment. Used to check if we're in Dev/Prod/Test
  */
class EnvConfigService @Inject() (val config: Configuration,
                                  val env: Environment) {

  /**
    * Defines the precedence for each env descending left to right. <br/>
    * <br/>
    * E.g. for <b>Test</b> the service will try to first fetch <i>"test"</i>, if it's not there it will try to fallback to the <i></i>
    */
  val precedences: Map[Mode, List[String]] = Map(
    Dev -> ("dev" :: "default" :: Nil),
    Prod -> ("prod" :: "default" :: Nil),
    Test -> ("test" :: "dev" :: "default" :: Nil)
  )

  /**
    * Reads a config value with a given key from the conf/application.conf
    *
    * @param configKey a config key to read from the app configuration
    * @return the config value for a given config key on None it the given value is not present in the app config
    */
  def read(configKey: String): Option[String] = configKey.split("\\.", 2).toList match {
    case baseKey :: key :: Nil => readByPriority(precedences(env.mode), baseKey, Some(key))
    case key :: Nil => readByPriority(precedences(env.mode), key)
    case _ => None
  }

  /**
    * Reads a config value with a given key from the conf/application.conf
    *
    * @param configKey a config key to read from the app configuration
    * @return the config value for a given config key on None it the given value is not present in the app config
    */
  def readOrThrow(configKey: String): String = read(configKey) match {
    case Some(value) => value
    case _ => throw new RuntimeException(
      s"Value [$configKey] was not defined for [${env.mode}] environment"
    )
  }

  /**
    * Iterates over the given ordered list of precedences and executes some function until it returns some defined result.
    * If there's no result, then the next "by precedence" element is passed to the function
    *
    * @param envsByPrecedence contains the list of environments, sorted by precedence
    * @param baseKey a base config key, to insert the environment suffix after (before the first ".")
    * @param maybeKey the remaining part of the config key to insert the environment suffix before (after the first ".")
    * @return some value if it was defined in config and None if not.
    */ //TODO: Move this to a separate service
       //TODO: Add support for multi-dot base keys
  private def readByPriority(envsByPrecedence: List[String], baseKey: String, maybeKey: Option[String] = None): Option[String] = envsByPrecedence.view.flatMap {
    case "default" => config.getOptional[String](maybeKey.fold(s"$baseKey")(key => s"$baseKey.$key"))
    case devProdOrTest => config.getOptional[String](maybeKey.fold(s"$baseKey.$devProdOrTest")(key => s"$baseKey.$devProdOrTest.$key"))
  }.headOption
}
