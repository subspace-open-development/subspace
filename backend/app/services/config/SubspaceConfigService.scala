package services.config

import com.google.inject.{Inject, Singleton}

@Singleton
class SubspaceConfigService @Inject()(envConfigService: EnvConfigService) {

  val subspacePrefix: String = s"subspace"

  val frontendUrl: String = envConfigService.readOrThrow(s"$subspacePrefix.frontendUrl")
  val backendUrl: String = envConfigService.readOrThrow(s"$subspacePrefix.backendUrl")
}
