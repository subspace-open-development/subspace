package services

import com.google.inject.Inject
import models.util.WebsocketTopics
import play.api.libs.ws.WSClient
import repositories._
import services.gitlab.GitlabUserService
import utils.Logger
import scala.concurrent.{ExecutionContext, Future}

case class WebsocketTopicsService @Inject()(userRepository: UserRepository,
                                            hostRepository: HostRepository,
                                            hostUserService: HostUserService,
                                            hostUserRepository: HostUserRepository,
                                            groupUserRepository: GroupUserRepository,
                                            groupProjectRepository: GroupProjectRepository,
                                            gitlabUserService: GitlabUserService,
                                            ws: WSClient,
                                            implicit val executionContext: ExecutionContext) extends Logger {

  def getAllTopics(userId: Int): Future[Seq[String]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [userId: $userId]"
  ) {
    for {
      projectTopics <- getProjectTopics(userId)
    } yield projectTopics
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  private def getProjectTopics(userId: Int): Future[Seq[String]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [userId: $userId]"
  ) {
    for {
      groupUsers <- groupUserRepository.findByUserId(userId)
      groupProjects <- Future.sequence(groupUsers.map(gu => groupProjectRepository.findByGroupId(gu.group_id)))
      projectIds = groupProjects.flatten.map(gps => gps.project_Id)
    } yield projectIds.map(WebsocketTopics.projectTopic)
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

}