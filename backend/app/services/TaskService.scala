package services

import com.google.inject.Inject
import models._
import models.enums.Hosts
import models.util.NotFound
import play.api.libs.ws.WSClient
import repositories.{HostRepository, HostUserRepository, ProjectRepository, TaskRepository, UserRepository}
import services.gitlab.{GitlabGroupService, GitlabProjectService, GitlabProtectedBranchService, GitlabRepositoryService, GitlabUserService}
import utils.{Logger, Slug}
import utils.cats.custom.implicits._

import scala.concurrent.{ExecutionContext, Future}

case class TaskService @Inject()(userRepository: UserRepository,
                                 hostRepository: HostRepository,
                                 hostUserService: HostUserService,
                                 hostUserRepository: HostUserRepository,
                                 gitlabUserService: GitlabUserService,
                                 gitlabGroupService: GitlabGroupService,
                                 gitlabRepositoryService: GitlabRepositoryService,
                                 gitlabProtectedBranchService: GitlabProtectedBranchService,
                                 taskRepository: TaskRepository,
                                 projectRepository: ProjectRepository,
                                 ws: WSClient)(implicit val executionContext: ExecutionContext) extends Logger {

  def addNewTask(newTask: NewTask)(implicit token: String): Future[Task] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [newTask: $newTask]"
  ) {

    val task = newTask.toTask(Some(""))
    val taskHasBranch = task.branch.isDefined

    for {
      // see if the task parameters are valid

      // see if the above task can be added to

      // see if the user has appropriate reputation permissions

      // add the new task to the database
      addedTask <- taskRepository.create(task)

      project <- projectRepository.findByProjectId(task.projectId) failOnNone util.NotFound(s"Project with [id = ${task.projectId}]")

      // find the parent task branch and copy it for this new task
      parentBranchTask <- {
        // if this task has it's own branch, then use it, and if not then check the parent branch and use that task
        // the task would have a branch if it was pegged to an existing branch
        if (taskHasBranch){
          Future.successful[Task](task)
        } else {
          // if a task doesn't have a branch then the task's parent should always have a branch
          taskRepository.findById(newTask.parentId.get) failOnNone util.NotFound(s"Task with [taskId = ${newTask.parentId}]")
        }
      }
      parentBranchName = parentBranchTask.branch.get
      branch <- {
        if (!taskHasBranch){
          val newBranchName = s"task-${addedTask.iid}-${Slug(task.title, 20)}"
          logger.info(newBranchName)
          logger.info(parentBranchName)
          gitlabRepositoryService.createBranch(project.host_project_id, newBranchName, parentBranchName)
        } else {
          gitlabRepositoryService.getBranch(token, project.host_project_id, task.branch.get) failOnNone util.NotFound(s"Branch with [name = ${task.branch.get}]")
        }
      }
      _ <- taskRepository.updateBranch(addedTask, branch.getName)
      updatedTask = addedTask.copy(branch = Some(branch.getName))

      // protect the branch
      _ <- gitlabProtectedBranchService.protectBranch(project.host_project_id, branch.getName)
    } yield updatedTask
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def editTask(task: EditTask): Future[Int] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [EditTask: $task]"
  ) {
    for {
      // see if the task parameters are valid

      // see if the user has appropriate reputation permissions

      existingTask <- taskRepository.findById(task.id) failOnNone util.NotFound(s"Task with [id = ${task.id}]")
      taskId <- taskRepository.update(task, existingTask)
    } yield taskId
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def getTaskDetails(projectId: Int, taskIid: Int): Future[Option[(Task, User)]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [taskIid: $taskIid]"
  ) {
    for {
      taskUser <- taskRepository.findByProjectIdAndIid_withUser(projectId, taskIid)
    } yield taskUser
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )

  def getTaskDetailsById(taskId: Int): Future[Option[(Task, User)]] = withLogging(
    infoIn = s"About to [${funcName()}] in [${className()}] with [taskId: $taskId]"
  ) {
    for {
      taskUser <- taskRepository.findById_withUser(taskId)
    } yield taskUser
  }(
    infoOut = result => s"Successfully [${funcName()}] in [${className()}]",
    errorOut = error => s"Failed to [${funcName()}] in [${className()}]. Reason: [$error]",
  )


}