package models

import java.sql.Timestamp

case class ProjectUser(
                    project_id: Int,
                    user_id: Int,
                    created_at: Timestamp = new Timestamp(System.currentTimeMillis())
                  )

