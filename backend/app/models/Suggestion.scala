package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json

case class Suggestion(
                       id: Int,
                       iid: Int,
                       projectId: Int,
                       contId: Int,
                       userId: Int,
                       userRep: Double,
                       manaUsed: Double,
                       title: String,
                       description: String,
                       commit_when_made: String = "",
                       commit_when_addressed: String = "",
                       status: String = "SUGGESTED",
                       createdAt: Timestamp = new Timestamp(System.currentTimeMillis()),
                       updatedAt: Timestamp = new Timestamp(System.currentTimeMillis())
               ){

  def toWire(userWire: UserWire): SuggestionWire ={
    SuggestionWire(this, userWire)
  }

}

object Suggestion {
  implicit val timestampFormat = TimestampFormat
  implicit val userFormat = Json.format[Suggestion]
}

case class NewSuggestion(
                          projectId: Int,
                          contId: Int,
                          userId: Int,
                          userRep: Double,
                          manaUsed: Double,
                          title: String,
                          description: String
                     ){

  def toSuggestion(): Suggestion ={
    Suggestion(
      0,
      0,
      projectId,
      contId,
      userId,
      userRep,
      manaUsed,
      title,
      description
    )
  }
}

case class SuggestionWire(
                       suggestion: Suggestion,
                       user: UserWire
                     )

object SuggestionWire {
  implicit val timestampFormat = TimestampFormat
  implicit val userFormat = Json.format[SuggestionWire]
}