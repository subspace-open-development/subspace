package models

import java.sql.Timestamp

case class Task(
                 id: Int,
                 iid: Int,
                 parentId: Option[Int],
                 projectId: Int,
                 ownerId: Int,
                 branch: Option[String],
                 commit: Option[String],
                 closed: Boolean,
                 title: String,
                 description: String,
                 autoRebase: Boolean,
                 ongoing: Boolean,
                 noDirectContributions: Boolean,
                 autoMergeUp: Boolean,
                 requireOwnerApproval: Boolean,
                 created_at: Timestamp = new Timestamp(System.currentTimeMillis()),
                 updated_at: Timestamp = new Timestamp(System.currentTimeMillis())
               ){

  def toTaskWire(maybeUser: Option[UserWire] = None): TaskWire ={
    TaskWire(this, id, maybeUser)
  }
}

case class TaskWire(
                 task: Task,
                 key: Int, // this is just a convenience for the front-end, and is just the task id
                 user: Option[UserWire] = None,
                 children: Option[Seq[TaskWire]] = None
               )

case class NewTask(
                    parentId: Option[Int],
                    projectId: Int,
                    ownerId: Int,
                    branch: Option[String],
                    title: String,
                    description: String,
                    autoRebase: Boolean,
                    ongoing: Boolean,
                    noDirectContributions: Boolean,
                    autoMergeUp: Boolean,
                    requireOwnerApproval: Boolean
                   ){

  def toTask(commit: Option[String] = None): Task ={
    Task(
      0,
      0,
      parentId,
      projectId,
      ownerId,
      branch,
      commit,
      closed = false,
      title,
      description,
      autoRebase,
      ongoing,
      noDirectContributions,
      autoMergeUp,
      requireOwnerApproval
    )
  }
}

case class EditTask(
                     id: Int,
                     iid: Int,
                     parentId: Option[Int],
                     projectId: Int,
                     ownerId: Int,
                     branch: Option[String],
                     closed: Boolean,
                     title: String,
                     description: String,
                     autoRebase: Boolean,
                     ongoing: Boolean,
                     noDirectContributions: Boolean,
                     autoMergeUp: Boolean,
                     requireOwnerApproval: Boolean
                   ){
  def buildTask(task: Task): Task ={
    task.copy(
      ownerId = ownerId,
      branch = branch,
      closed = closed,
      title = title,
      description = description,
      autoRebase = autoRebase,
      ongoing = ongoing,
      noDirectContributions = noDirectContributions,
      autoMergeUp = autoMergeUp,
      requireOwnerApproval = requireOwnerApproval
    )
  }
}