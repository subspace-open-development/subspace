package models

import play.api.libs.json.Json

case class Tos(accepted: Boolean)

object Tos {
  implicit val tosFormat = Json.writes[Tos]
}