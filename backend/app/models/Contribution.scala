package models

import java.sql.Timestamp

import models.enums.ContributionState
import models.util.{OptionFormat, TimestampFormat}
import play.api.libs.json._

case class Contribution(
                         id: Int,
                         iid: Int,
                         projectId: Int,
                         taskId: Int,
                         userId: Int,
                         branch: String,
                         commit: String,
                         state: String,
                         stateChangeAt: Option[Timestamp],
                         stateFuture: Option[String],
                         closed: Boolean,
                         title: String,
                         description: String,
                         autoUpdateWithCommits: Boolean,
                         manaUsed: Double,
                         userRep: Double,
                         createdAt: Timestamp = new Timestamp(System.currentTimeMillis()),
                         updatedAt: Timestamp = new Timestamp(System.currentTimeMillis())
                       ){
  def toWire(user: UserWire, showInQueue: Option[Boolean] = None, approval: Option[Back] = None, supporters: Seq[UserWire] = Seq.empty[UserWire]): ContributionWire ={
    val sortedSupporters = supporters.distinct.sortWith((a, b) => a.reputation > b.reputation)

    ContributionWire(
      id,
      iid,
      projectId,
      taskId,
      user,
      sortedSupporters,
      branch,
      commit,
      state,
      stateChangeAt,
      stateFuture,
      closed,
      title,
      description,
      autoUpdateWithCommits,
      manaUsed,
      userRep,
      showInQueue,
      approval,
      createdAt,
      updatedAt
    )
  }

  def getSecondsSinceCreated(): Int ={
    Math.round((System.currentTimeMillis().toDouble - createdAt.getTime.toDouble)/ 1000.0).toInt
  }
}

object Contribution {
  implicit val timestampFormat = TimestampFormat
  implicit val contributionFormat = Json.format[Contribution]
}

case class NewContribution(
                            projectId: Int,
                            taskId: Int,
                            userId: Int,
                            branch: String,
                            commit: String,
                            title: String,
                            description: String,
                            autoUpdateWithCommits: Boolean,
                            manaUsed: Double,
                            userRep: Double
                          ){

  def toContribution(): Contribution ={
    Contribution(
      0,
      0,
      projectId,
      taskId,
      userId,
      branch,
      commit,
      ContributionState.OPEN,
      None,
      None,
      closed = false,
      title,
      description,
      autoUpdateWithCommits,
      manaUsed,
      userRep
    )
  }
}

case class EditContribution(
                             id: Int,
                             iid: Int,
                             projectId: Int,
                             taskId: Int,
                             userId: Int,
                             branch: String,
                             commit: String,
                             state: String,
                             stateChangeAt: Option[Timestamp],
                             stateFuture: Option[String],
                             closed: Boolean,
                             title: String,
                             description: String,
                             autoUpdateWithCommits: Boolean,
                             manaUsed: Double,
                             userRep: Double
                       ){
  def buildTask(c: Contribution): Contribution ={
    c.copy(
      taskId = taskId,
      userId = userId,
      branch = branch,
      commit = commit,
      state = state,
      stateChangeAt = stateChangeAt,
      stateFuture = stateFuture,
      closed = closed,
      title = title,
      description = description,
      autoUpdateWithCommits = autoUpdateWithCommits,
      manaUsed = manaUsed,
      userRep = userRep
    )
  }
}

object EditContribution extends OptionFormat {
  implicit val timestampFormat = TimestampFormat
  implicit val contributionFormat = Json.format[EditContribution]
}

case class ContributionWire(
                             id: Int,
                             iid: Int,
                             projectId: Int,
                             taskId: Int,
                             user: UserWire, // contributor
                             supporters: Seq[UserWire],
                             branch: String,
                             commit: String,
                             state: String,
                             stateChangeAt: Option[Timestamp],
                             stateFuture: Option[String],
                             closed: Boolean,
                             title: String,
                             description: String,
                             autoUpdateWithCommits: Boolean,
                             manaUsed: Double,
                             userRep: Double,
                             showInQueue: Option[Boolean],
                             approval: Option[Back],
                             createdAt: Timestamp = new Timestamp(System.currentTimeMillis()),
                             updatedAt: Timestamp = new Timestamp(System.currentTimeMillis())
                       )

object ContributionWire {
  implicit val timestampFormat = TimestampFormat
  implicit val contributionWireFormat = Json.format[ContributionWire]
}