package models

import models.util.TimestampFormat
import play.api.libs.json.Json

case class PublicProject(
                    num_users: Int,
                    project: Project,
                    gitRepo: Option[GitRepo]
                  )

object PublicProject {
  implicit val timestampFormat = TimestampFormat
  implicit val publicProjectFormat = Json.writes[PublicProject]
}