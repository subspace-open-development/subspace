package models

import models.enums.Hosts
import org.gitlab4j.api.models.{Project => GitlabProject}
import play.api.libs.json._

import scala.util.Try

case class AllGitRepos(
                     gitlabRepos: Seq[GitRepo],
                     githubRepos: Seq[GitRepo]
                   )

case class GitRepo(
                    name: String,
                    nameWithNamespace: String,
                    host: String,
                    host_project_id: Int,
                    description: Option[String],
                    icon: Option[String],
                    webUrl: Option[String],
                    numUsers: Option[Int],
                    visibility: Option[String],
                    onboarded: Boolean = false,
                    joined: Boolean = false
                  )

object AllGitRepos {
  implicit val gitRepoFormat = Json.format[GitRepo]
  implicit val gitRepoListFormat = Json.format[AllGitRepos]
}

object GitRepo {
  implicit val gitRepoFormat = Json.format[GitRepo]
  implicit val gitRepoListFormat = Json.format[AllGitRepos]

  def fromGitlabProject(
                         gitlabProject: GitlabProject,
                         subspaceGitlabProjects: Seq[Project],
                         projectUsers: Seq[ProjectUser]
                       ): GitRepo ={

    val onboarded = subspaceGitlabProjects.exists(p => p.host_project_id == gitlabProject.getId)
    val joined = {
      if (!onboarded){
        false
      } else {
        val subspaceProject = subspaceGitlabProjects.find(p => p.host_project_id == gitlabProject.getId).get
        projectUsers.exists(pu => pu.project_id == subspaceProject.id)
      }
    }

    GitRepo(
      gitlabProject.getName,
      gitlabProject.getNameWithNamespace,
      Hosts.GITLAB.toString,
      gitlabProject.getId,
      Try(gitlabProject.getDescription).toOption,
      Try(gitlabProject.getAvatarUrl).toOption,
      Try(gitlabProject.getWebUrl).toOption,
      Some(0),
      Try(gitlabProject.getVisibility.toString).toOption,
      onboarded,
      joined
    )
  }
}


