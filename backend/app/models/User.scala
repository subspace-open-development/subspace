package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json
import services.DynamicsAndRatingService

case class User(
  id: Int,
  username: String,
  email: String,
  created_at: Timestamp = new Timestamp(System.currentTimeMillis())
){
  def toWire(
              maybeRep: Option[Double] = None,
              maybeMana: Option[Double] = None
            ): UserWire ={

    val rep = maybeRep.getOrElse(0.0)
    val mana = maybeMana.getOrElse(DynamicsAndRatingService.maxMana)

    UserWire(this.id, this.username, this.email, rep, mana, this.created_at)
  }
}

case class Tokens(accessToken: String, refreshToken: String)
case class RegisterUserInput(username: String, email: String, password: String)
case class LoginUserInput(email: String, password: String)

object User {
  implicit val timestampFormat = TimestampFormat
  implicit val userFormat = Json.format[User]
}

case class UserWire(
                     id: Int,
                     username: String,
                     email: String,
                     reputation: Double,
                     mana: Double,
                     created_at: Timestamp
                   )

object UserWire {
  implicit val timestampFormat = TimestampFormat
  implicit val userFormat = Json.format[UserWire]
}