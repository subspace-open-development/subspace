package models

case class SshKey(
                   project_id: Int,
                   public_key: String,
                   private_key: String,
                   service: String
                 )


