package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json

case class Back(
                 projectId: Int,
                 contId: Int,
                 userId: Int,
                 userRep: Double,
                 manaUsed: Double,
                 branch: String,
                 commit: String,
                 maintain_approval: Boolean,
                 createdAt: Timestamp = new Timestamp(System.currentTimeMillis()),
                 updatedAt: Timestamp = new Timestamp(System.currentTimeMillis())
               )

object Back {
  implicit val timestampFormat = TimestampFormat
  implicit val backFormat = Json.format[Back]
}

case class NewBack(
                    projectId: Int,
                    contId: Int,
                    userId: Int,
                    userRep: Double,
                    manaUsed: Double,
                    branch: String,
                    commit: String,
                    maintain_approval: Boolean
               ){
  def toBack(): Back ={
    Back(
      projectId,
      contId,
      userId,
      userRep,
      manaUsed,
      branch,
      commit,
      maintain_approval
    )
  }
}

object NewBack {
  implicit val timestampFormat = TimestampFormat
  implicit val backFormat = Json.format[Back]
}