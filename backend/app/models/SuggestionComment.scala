package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json

case class SuggestionComment(
                 id: Int,
                 parentId: Option[Int],
                 suggestionId: Int,
                 authorId: Int,
                 comment: String,
                 createdAt: Timestamp = new Timestamp(System.currentTimeMillis()),
                 updatedAt: Timestamp = new Timestamp(System.currentTimeMillis())
               ){

  def toWire(user: User): SuggestionCommentWire ={
    SuggestionCommentWire(this, user)
  }

}

object SuggestionComment {
  implicit val timestampFormat = TimestampFormat
  implicit val suggestionCommentFormat = Json.writes[SuggestionComment]
}

case class NewSuggestionComment(
                       parentId: Option[Int],
                       suggestionId: Int,
                       authorId: Int,
                       comment: String
                     ){

  def toSuggestionComment(): SuggestionComment ={
    SuggestionComment(
      0,
      parentId,
      suggestionId,
      authorId,
      comment
    )
  }
}

case class SuggestionCommentWire(
                              comment: SuggestionComment,
                              user: User
                            )

object SuggestionCommentWire {
  implicit val timestampFormat = TimestampFormat
  implicit val suggestionCommentWireFormat = Json.writes[SuggestionCommentWire]
}