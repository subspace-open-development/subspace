package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json


case class HostUser(
                  id: Int,
                  user_id: Int,
                  host_id: Int,
                  host_user_id: Int,
                  host_user_email: String,
                  host_user_username: String,
                  host_user_token: String,
                  created_at: Timestamp = new Timestamp(System.currentTimeMillis()),
                  updated_at: Timestamp = new Timestamp(System.currentTimeMillis()),
                  active_at: Timestamp = new Timestamp(System.currentTimeMillis()),
               ){
  def getWire(host: String): HostUserWire ={
    HostUserWire(
      user_id,
      host,
      host_user_email,
      host_user_username
    )
  }
}
object HostUser {
  implicit val timestampFormat = TimestampFormat
  implicit val hostUserFormat = Json.format[HostUser]
}

case class HostUserWire(
                         user_id: Int,
                         host: String,
                         git_user_email: String,
                         git_user_username: String,
                       )
object HostUserWire {
  implicit val hostUserWireFormat = Json.format[HostUserWire]
}