package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json

case class ProjectSettings(
                            project_id: Int,
                            support_proportion_required: Double,
                            threshold_halflife_seconds: Int,
                            seconds_from_commit_until_decline: Int,
                            seconds_from_start_until_decline: Int,
                            created_at: Timestamp = new Timestamp(System.currentTimeMillis()),
                            updated_at: Timestamp = new Timestamp(System.currentTimeMillis())
                          )

object ProjectSettings {
  implicit val timestampFormat = TimestampFormat
  implicit val projectSettingsFormat = Json.format[ProjectSettings]
}
