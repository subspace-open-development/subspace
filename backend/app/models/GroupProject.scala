package models

import java.sql.Timestamp

case class GroupProject(
                    group_id: Int,
                    project_Id: Int,
                    created_at: Timestamp
                  )

