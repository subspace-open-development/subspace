package models

import java.sql.Timestamp
import models.enums.JoinRequestStatus

case class JoinRequest(
                    user_id: Int,
                    project_id: Int,
                    status: String = JoinRequestStatus.REQUESTED,
                    created_at: Timestamp = new Timestamp(System.currentTimeMillis()),
                    updated_at: Timestamp = new Timestamp(System.currentTimeMillis())
                  )

