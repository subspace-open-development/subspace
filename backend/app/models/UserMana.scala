package models

import java.sql.Timestamp

case class UserMana(
                     user_id: Int,
                     mana: Double,
                     updated_at: Timestamp = new Timestamp(System.currentTimeMillis())
                    )


