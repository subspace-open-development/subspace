package models

import java.sql.Timestamp

case class GroupUser(
                    group_id: Int,
                    user_id: Int,
                    created_at: Timestamp = new Timestamp(System.currentTimeMillis())
                  )

