package models

import play.api.libs.json.Json

case class ErrorWire(msg: String)

object ErrorWire {
  implicit val errorWireFormat = Json.writes[ErrorWire]
}