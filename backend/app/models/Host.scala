package models

case class Host(
                 id: Int,
                 name: String
               )