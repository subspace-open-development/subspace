package models

import java.sql.Timestamp
import org.gitlab4j.api.models.{Branch => GitlabBranch}
import models.util.TimestampFormat
import play.api.libs.json.Json

case class Branches(
                    project_id: Int,
                    host_id: Int,
                    host_project_id: Int,
                    branches: Seq[Branch]
                  )

object Branches {
  implicit val branchFormat = Json.writes[Branch]
  implicit val branchesFormat = Json.writes[Branches]
  implicit val timestampFormat = TimestampFormat
}

case class Branch(
                 name: String,
                 commit: String,
                 commit_short: String,
                 task_iid: Option[Int] = None,
                 contribution_iid: Option[Int] = None,
                 )

object Branch {
  implicit val branchFormat = Json.writes[Branch]
  implicit val timestampFormat = TimestampFormat

  def fromGitlabBranch(
                        gitlabBranch: GitlabBranch,
                        tasks: Seq[TaskWire] = Seq.empty[TaskWire],
                        contributions: Seq[Contribution] = Seq.empty[Contribution]
                      ): Branch ={

    val maybeBranchTask = tasks.find(t => t.task.branch.isDefined && t.task.branch.get == gitlabBranch.getName)
    // val maybeTaskId = if (maybeBranchTask.isDefined) Some(maybeBranchTask.get.task.id) else None
    val maybeTaskIid = if (maybeBranchTask.isDefined) Some(maybeBranchTask.get.task.iid) else None

    val maybeBranchContribution = contributions.find(c => c.branch == gitlabBranch.getName)
    val maybeBranchContributionIid = if (maybeBranchContribution.isDefined) Some(maybeBranchContribution.get.iid) else None

    Branch(gitlabBranch.getName, gitlabBranch.getCommit.getId, gitlabBranch.getCommit.getShortId, maybeTaskIid, maybeBranchContributionIid)
  }
}