
package models

import java.sql.Timestamp
import java.text.SimpleDateFormat

import models.util.TimestampFormat
import play.api.libs.json._

case class ProjectStateRecord(
                                id: Int,
                                project_id: Int,
                                start_at: Timestamp,
                                end_at: Timestamp,
                                points: Double,
                                budget: Option[Double],
                                funded: Option[Boolean]
                              )

object ProjectStateRecord {
  implicit val timestampFormat = TimestampFormat
  implicit val projectStateRecordFormat = Json.format[ProjectStateRecord]
}
