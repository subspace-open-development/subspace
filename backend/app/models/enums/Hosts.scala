package models.enums

object Hosts {
  val GITLAB = "GITLAB"
  val GITHUB = "GITHUB"
  val BITBUCKET = "BITBUCKET"
}