package models.enums

import play.api.libs.json.{Reads, Writes}

object CommentLocation extends Enumeration {
  type CommentLocation = Value
  val DISCUSSION, INLINE = Value

  implicit val readsMyEnum = Reads.enumNameReads(CommentLocation)
  implicit val writesMyEnum = Writes.enumNameWrites
}