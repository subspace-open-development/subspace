package models.enums

import play.api.libs.json.{Reads, Writes}

object ContributionState {
  val OPEN = "OPEN"
  val ACCEPTED = "ACCEPTED"
  val PENDING = "PENDING"
  val DECLINED = "DECLINED"
}