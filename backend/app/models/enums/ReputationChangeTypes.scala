package models.enums

object ReputationChangeTypes {
  val CONTRIBUTION = "CONTRIBUTION"
  val SUPPORT = "SUPPORT"
  val SUGGESTION = "SUGGESTION"
}