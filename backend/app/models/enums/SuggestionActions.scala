package models.enums

import play.api.libs.json.{Reads, Writes}

object SuggestionActions {
  val ADDRESS = "ADDRESS"
  val ACCEPT  = "ACCEPT"
}