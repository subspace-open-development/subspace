package models.enums

import play.api.libs.json.{Reads, Writes}

object SuggestionState  {
  val SUGGESTED = "SUGGESTED"
  val ADDRESSED = "ADDRESSED"
  val ACCEPTED  = "ACCEPTED"
}