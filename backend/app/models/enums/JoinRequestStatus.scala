package models.enums

object JoinRequestStatus {
  val REQUESTED = "REQUESTED"
  val DECLINED = "DECLINED"
  val ACCEPTED = "ACCEPTED"
}