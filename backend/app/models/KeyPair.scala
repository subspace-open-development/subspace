package models

case class KeyPair(privateKey: String, publicKey: String)