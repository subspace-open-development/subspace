package models.webhooks.gitlab

import play.api.libs.json._


case class WebhookMergeParams(
  force_remove_source_branch: Option[String]
)

case class WebhookLastCommit(
  id: String,
  message: String,
  timestamp: String,
  url: String,
  author: WebhookAuthor
)

case class WebhookMergeRequestAttributes(
  // assignee_id: Option[String],
  action: Option[String],
  assignee: Option[WebhookUser],
  author_id: Int,
  created_at: String,
  description: String,
//  head_pipeline_id: Int,
  id: Int,
  iid: Int,
  // last_edited_at: Option[String],
  last_edited_by_id: Option[Int],
//  merge_commit_sha: Option[String],
  merge_error: Option[String],
  merge_params: WebhookMergeParams,
  merge_status: String,
  // merge_user_id: Option[Int],
  // merge_when_pipeline_succeeds: Boolean,
  // milestone_id: Option[Int],
  source_branch: String,
  source_project_id: Int,
  state: String,
  target_branch: String,
  target_project_id: Int,
  // time_estimate: Double,
  title: String,
  // updated_at: String,
  // updated_by_id: Option[Int],
  url: String,
  source: WebhookProject,
  target: WebhookProject,
  last_commit: WebhookLastCommit,
  work_in_progress: Boolean,
  // total_time_spent: Double,
  // human_total_time_spent: Option[Double],
  // human_time_estimate: Option[Double],
  // position: Option[Int],
)

case class WebhookTotalTimeSpent(
  previous: Option[Double],
  current: Double
)

case class WebhookUpdatedAt(
  current: String,
  previous: String
)

case class WebhookMergeRequestStateChanges(
  current: String,
  previous: String
)

case class WebhookMergeRequestChanges(
  total_time_spent: WebhookTotalTimeSpent,
  state: Option[WebhookMergeRequestStateChanges]
)

case class MergeRequestWebhook(
  object_kind: String,
  event_type: String,
  labels: Seq[String],
  user: WebhookUser,
  project: WebhookProject,
  object_attributes: WebhookMergeRequestAttributes,
  changes: WebhookMergeRequestChanges,
  repository: WebhookRepository
){
  def getMergeRequestId (): Int ={
    object_attributes.id
  }
  def getProjectId (): Int ={
    project.id
  }
  def getGitlabUsername (): String ={
    user.username
  }
}
