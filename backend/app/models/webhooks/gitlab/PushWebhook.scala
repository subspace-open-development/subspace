package models.webhooks.gitlab

import play.api.libs.json.Json


case class WebhookCommit(
  id: String,
  message: String,
  timestamp: String,
  url: String,
  author: WebhookAuthor,
  added: Option[Seq[String]],
  modified: Option[Seq[String]],
  removed: Option[Seq[String]]
)

case class PushWebhook(
  object_kind: String,
  before: String,
  after: String,
  ref: String,
  checkout_sha: String,
  user_id: Int,
  user_name: String,
  user_username: String,
  user_email: String,
  user_avatar: String,
  project_id: Int,
  project: WebhookProject,
  repository: WebhookRepository,
  commits: Seq[WebhookCommit],
  total_commits_count: Int
)