package models.webhooks.gitlab

import play.api.libs.json.Json


case class WebhookIssueAttributes(
  id: Int,
  title: String,
  assignee_ids: List[Int],
  assignee_id: Option[Int],
  author_id: Int,
  project_id: Int,
  created_at: String,
  updated_at: String,
  position: Option[Int],
  branch_name: Option[String],
  description: String,
  milestone_id: Option[String],
  state: String,
  iid: Int,
  url: Option[String],
  action: Option[String]
)

case class WebhookLabels(
  id: Int,
  title: String,
  color: String,
  project_id: Int,
  created_at: String,
  updated_at: String,
  template: Int,
  description: String,
  `type`: String,
  group_id: Int
)

case class WebhookLabelsChanges(
  previous: List[WebhookLabels],
  current: List[WebhookLabels]
)

case class WebhookIssueChanges(
  updated_by_id: Option[String],
  updated_at: Option[List[String]],
  labels: Option[WebhookLabelsChanges]
)

case class IssueWebhook(
  object_kind: String,
  event_type: String,
  user: WebhookUser,
  project: WebhookProject,
  repository: WebhookRepository,
  object_attributes: WebhookIssueAttributes,
  assignees: Option[List[WebhookUser]],
  assignee: Option[WebhookUser],
  labels: List[WebhookLabels],
  changes: WebhookIssueChanges
)