package models.webhooks.gitlab

import play.api.libs.json.Json

case class WebhookAuthor(
  name: String,
  email: String
)

case class WebhookProject(
  id: Int,
  name: String,
  description: String,
  web_url: String,
  avatar_url: Option[String],
  git_ssh_url: String,
  git_http_url: String,
  namespace: String,
  visibility_level: Int,
  path_with_namespace: String,
  default_branch: String,
  ci_config_path: Option[String],
  homepage: String,
  url: String,
  ssh_url: String,
  http_url: String
)

case class WebhookRepository(
  name: String,
  url: String,
  description: String,
  homepage: String,
  git_http_url: Option[String],
  git_ssh_url: Option[String],
  visibility_level: Option[Int]
)

case class WebhookUser(
  name: String,
  username: String,
  avatar_url: String
)

object GitlabWebhooks {
  implicit val c0 = Json.format[WebhookAuthor]
  implicit val c1 = Json.format[WebhookProject]
  implicit val c2 = Json.format[WebhookRepository]
  implicit val c3 = Json.format[WebhookUser]

  implicit val i0 = Json.format[WebhookIssueAttributes]
  implicit val i1 = Json.format[WebhookLabels]
  implicit val i2 = Json.format[WebhookLabelsChanges]
  implicit val i3 = Json.format[WebhookIssueChanges]
  implicit val i4 = Json.format[IssueWebhook]

  implicit val mr0 = Json.format[WebhookMergeParams]
  implicit val mr1 = Json.format[WebhookLastCommit]
  implicit val mr2 = Json.format[WebhookTotalTimeSpent]
  implicit val mr3 = Json.format[WebhookMergeRequestAttributes]
  implicit val mr4 = Json.format[WebhookMergeRequestStateChanges]
  implicit val mr5 = Json.format[WebhookMergeRequestChanges]
  implicit val mr6 = Json.format[MergeRequestWebhook]

  implicit val g0 = Json.format[WebhookCommit]
  implicit val g1 = Json.format[PushWebhook]

  implicit val ct0 = Json.format[WebhookStDiff]
  implicit val ct1 = Json.format[WebhookCommentAttributes]
  implicit val ct2 = Json.format[CommentWebhook]
}