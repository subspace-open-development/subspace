package models.webhooks.gitlab

import play.api.libs.json.Json

case class WebhookStDiff(
  diff: String,
  new_path: String,
  old_path: String,
  a_mode: String,
  b_mode: String,
  new_file: Boolean,
  renamed_file: Boolean,
  deleted_file: Boolean
)

case class WebhookCommentAttributes(
  id: Int,
  note: String,
  noteable_type: String,
  author_id: Int,
  created_at: String,
  updated_at: String,
  project_id: Int,
  attachment: Option[String],
  line_code: Option[String],
  commit_id: Option[String],
  noteable_id: Int,
  system: Boolean,
  st_diff: Option[WebhookStDiff],
  url: String
)

case class CommentWebhook(
  object_kind: String,
  user: WebhookUser,
  project_id: Int,
  project: WebhookProject,
  repository: WebhookRepository,
  object_attributes: WebhookCommentAttributes,
  commit: Option[WebhookCommit],
  merge_request: Option[WebhookMergeRequestAttributes],
  issue: Option[WebhookIssueAttributes]
)