package models.util

object WebsocketTopics {

  def userTopic(userId: Int) = s"user-$userId"

  def projectTopic(projectId: Int) = s"project-$projectId"

}
