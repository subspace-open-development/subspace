package models.util

import models.util.Error.ErrorKey
import models.util.Error.ErrorKey.ErrorKey

class Error(msg: String) extends Exception(msg)

case class CustomError(key: ErrorKey, message: String) extends Exception(message)

class NotFound(val msg: String = "") extends CustomError(
  ErrorKey.NOT_FOUND, if (msg.nonEmpty) s"Entity with these params not found: $msg" else ""
)

object NotFound {
  def apply(msg: String = "") = new NotFound(msg)
}

class AlreadyExists(val msg: String = "") extends CustomError(
  ErrorKey.ALREADY_EXISTS, if (msg.nonEmpty) s"Entity with these params already exists: $msg" else ""
)

class IsNotAMember(val msg: String = "") extends CustomError(
  ErrorKey.IS_NOT_A_MEMBER, if (msg.nonEmpty) s"User is not a member of a : $msg" else ""
)

object IsNotAMember {
  def apply(msg: String = "") = new IsNotAMember(msg)
}

class UsernameAlreadyExists(val username: String = "") extends CustomError(
  ErrorKey.USERNAME_ALREADY_EXISTS, if (username.nonEmpty) s"A username [$username] has already been taken" else ""
)

object UsernameAlreadyExists {
  def apply(username: String = "") = new UsernameAlreadyExists(username)
}

class EmailAlreadyExists(val email: String = "") extends CustomError(
  ErrorKey.EMAIL_ALREADY_EXISTS, if (email.nonEmpty) s"An email [$email] has already been taken" else ""
)

object EmailAlreadyExists {
  def apply(email: String = "") = new EmailAlreadyExists(email)
}

object AlreadyExists {
  def apply(msg: String = "") = new AlreadyExists(msg)
}

class AmbigousResult(val msg: String = "") extends CustomError(
  ErrorKey.AMBIGUOUS_RESULT, if (msg.nonEmpty) s"Two or more entities exist: $msg" else ""
)

object AmbigousResult {
  def apply(msg: String = "") = new AmbigousResult(msg)
}

class WrongPassword(val msg: String) extends CustomError(
  ErrorKey.WRONG_PASSWORD, if (msg.nonEmpty) s"User with these params has another password: $msg" else ""
)

object WrongPassword {
  def apply(msg: String = "") = new WrongPassword(msg)
}

class AlreadyConfirmed(val msg: String) extends CustomError(
  ErrorKey.ALREADY_CONFIRMED, if (msg.nonEmpty) s"User's email had been already confirmed: $msg" else ""
)

object AlreadyConfirmed {
  def apply(msg: String = "") = new AlreadyConfirmed(msg)
}

class AlreadyLinked(val msg: String) extends CustomError(ErrorKey.ALREADY_LINKED, msg)

object AlreadyLinked {
  def apply(msg: String = "") = new AlreadyLinked(msg)
}

class Unauthenticated(val msg: String = "") extends CustomError(ErrorKey.UNAUTHENTICATED, msg)

object Unauthenticated {
  def apply(msg: String = "") = new Unauthenticated(msg)
}

class Unauthorized(val msg: String = "") extends CustomError(ErrorKey.UNAUTHORIZED, msg)

object Unauthorized {
  def apply(msg: String = "") = new Unauthorized(msg)
}

class NonMaintainer(val msg: String = "") extends CustomError(ErrorKey.NON_MAINTAINER, msg)

object NonMaintainer {
  def apply(msg: String = "") = new NonMaintainer(msg)
}

class InvalidToken(val msg: String = "") extends CustomError(ErrorKey.INVALID_TOKEN, msg)

object InvalidToken {
  def apply(msg: String = "") = new InvalidToken(msg)
}

class InvalidObjectId(val msg: String = "") extends CustomError(ErrorKey.INVALID_OBJECT_ID, msg)

object InvalidObjectId {
  def apply(msg: String = "") = new InvalidObjectId(msg)
}

class NotConfirmed(val msg: String = "") extends CustomError(
  ErrorKey.NOT_CONFIRMED, if (msg.nonEmpty) s"User's email hasn't been confirmed yet: $msg" else msg
)

object NotConfirmed {
  def apply(msg: String = "") = new NotConfirmed(msg)
}

class InvalidPassword(val msg: String = "") extends CustomError(
  ErrorKey.INVALID_PASSWORD, if (msg.nonEmpty) s"Invalid password: $msg" else ""
)

object InvalidPassword {
  def apply(msg: String = "") = new InvalidPassword(msg)
}

class InvalidEmail(val msg: String = "") extends CustomError(
  ErrorKey.INVALID_EMAIL, if (msg.nonEmpty) s"Invalid email: $msg" else ""
)

object InvalidEmail {
  def apply(msg: String = "") = new InvalidEmail(msg)
}

class InvalidUsername(val msg: String = "") extends CustomError(
  ErrorKey.INVALID_USERNAME, if (msg.nonEmpty) s"Invalid username: $msg" else ""
)

object InvalidUsername {
  def apply(msg: String = "") = new InvalidUsername(msg)
}

class NotSent(val msg: String = "") extends CustomError(
  ErrorKey.NOT_SENT, if (msg.nonEmpty) s"Message wasn't sent: $msg" else ""
)

object NotSent {
  def apply(msg: String = "") = new NotSent(msg)
}

class InvalidName(val msg: String = "") extends CustomError(ErrorKey.INVALID_NAME, msg)

object InvalidName {
  def apply(msg: String) = new InvalidName(msg)
}

class LengthExceeded(val msg: String = "") extends CustomError(ErrorKey.LENGTH_EXCEEDED, msg)

object LengthExceeded {
  def apply(msg: String) = new LengthExceeded(msg)
}

class InvalidDescription(val msg: String = "") extends CustomError(ErrorKey.INVALID_DESCRIPTION, msg)

object InvalidDescription {
  def apply(msg: String) = new InvalidDescription(msg)
}

class InvalidProjectKey(val msg: String = "") extends CustomError(ErrorKey.INVALID_PROJECT_KEY, msg)

object InvalidProjectKey {
  def apply(msg: String) = new InvalidProjectKey(msg)
}

class InvalidTitle(val msg: String = "") extends CustomError(ErrorKey.INVALID_TITLE, msg)

object InvalidTitle {
  def apply(msg: String = ""): InvalidTitle = new InvalidTitle(msg)
}

class InvalidRejectionComment(val msg: String = "") extends CustomError(ErrorKey.INVALID_REJECTION_COMMENT, msg)

object InvalidRejectionComment {
  def apply(msg: String = ""): InvalidRejectionComment = new InvalidRejectionComment(msg)
}

class HasConflicts(val msg: String = "") extends CustomError(ErrorKey.HAS_CONFLICTS, msg)

object HasConflicts {
  def apply(msg: String) = new HasConflicts(msg)
}

class NoChanges(val msg: String = "") extends CustomError(ErrorKey.NO_CHANGES, msg)

object NoChanges {
  def apply(msg: String) = new NoChanges(msg)
}

class Forbidden(val msg: String = "") extends CustomError(ErrorKey.FORBIDDEN, msg)

object Forbidden {
  def apply(msg: String) = new Forbidden(msg)
}

class IncorrectContributionState(val msg: String = "") extends CustomError(ErrorKey.INCORRECT_CONTRIBUTION_STATE, msg)

object IncorrectContributionState {
  def apply(msg: String = "") = new IncorrectContributionState(msg)
}

class IncorrectTaskStatus(val msg: String = "") extends CustomError(ErrorKey.INCORRECT_TASK_STATUS, msg)

object IncorrectTaskStatus {
  def apply(msg: String = "") = new IncorrectTaskStatus(msg)
}

class DatabaseError(val msg: String = "") extends CustomError(ErrorKey.DATABASE_ERROR, msg)

object DatabaseError {
  def apply(msg: String) = new DatabaseError(msg)
}

class MattermostError(val msg: String = "") extends CustomError(ErrorKey.MATTERMOST_ERROR, msg)

object MattermostError {
  def apply(msg: String) = new MattermostError(msg)
}

class GitlabError(val msg: String = "") extends CustomError(ErrorKey.GITLAB_ERROR, msg)

object GitlabError {
  def apply(msg: String) = new GitlabError(msg)
}

class InvalidDetermination(val msg: String = "") extends CustomError(ErrorKey.INVALID_DETERMINATION, msg)

object InvalidDetermination {
  def apply(msg: String) = new InvalidDetermination(msg)
}

class NotSync(val msg: String = "") extends CustomError(ErrorKey.NOT_SYNC, msg)

object NotSync {
  def apply(msg: String) = new NotSync(msg)
}

case object TooComplexQueryError extends Exception("Query is too expensive.")

object Error {

  object ErrorKey extends Enumeration {
    type ErrorKey = Value
    val NOT_FOUND,
    ALREADY_EXISTS,
    IS_NOT_A_MEMBER,
    USERNAME_ALREADY_EXISTS,
    EMAIL_ALREADY_EXISTS,
    WRONG_PASSWORD,
    INVALID_PASSWORD,
    INVALID_EMAIL,
    INVALID_USERNAME,
    INVALID_TOKEN,
    INVALID_OBJECT_ID,
    INVALID_TITLE,
    INVALID_REJECTION_COMMENT,
    AMBIGUOUS_RESULT,
    ALREADY_CONFIRMED,
    ALREADY_LINKED,
    NOT_CONFIRMED,
    NOT_SENT,
    INVALID_NAME,
    UNAUTHENTICATED,
    UNAUTHORIZED,
    INVALID_DESCRIPTION,
    HAS_CONFLICTS,
    INVALID_PROJECT_KEY,
    NO_CHANGES,
    FORBIDDEN,
    LENGTH_EXCEEDED,
    INCORRECT_CONTRIBUTION_STATE,
    INCORRECT_TASK_STATUS,
    DATABASE_ERROR,
    MATTERMOST_ERROR,
    GITLAB_ERROR,
    INVALID_DETERMINATION,
    NON_MAINTAINER,
    NOT_SYNC = Value
  }

}