package models

case class HostProject(
                 host: String,
                 hostProjectId: Int
               )