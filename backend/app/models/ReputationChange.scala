package models

import java.sql.Timestamp
import models.util.TimestampFormat
import play.api.libs.json._

case class ReputationChange(
                              id: Int,
                              is_user: Boolean,
                              is_project: Boolean,
                              project_id: Int,
                              contribution_id: Int,
                              suggestion_id: Option[Int],
                              user_id: Int,
//                              rep_old: Double,
                              rep_change: Double,
//                              rep_new: Double,
                              points: Double,
                              change_type: String,           // ContributionAction.Value: CONTRIBUTE, SUPPORT, SUGGEST
                              contribution_outcome: String,  // ContributionState: ACCEPTED, DECLINED
                              suggestion_outcome: Option[String],
                              created_at: Timestamp
                            )

object ReputationChange {
  implicit val timestampFormat = TimestampFormat
  implicit val reputationChangeFormat = Json.format[ReputationChange]
}
