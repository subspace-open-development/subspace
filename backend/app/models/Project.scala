package models

import java.sql.Timestamp

import models.util.TimestampFormat
import org.gitlab4j.api.models.{Project => GitlabProject}
import play.api.libs.json._
import play.api.libs.functional.syntax._

case class Project(
                    id: Int,
                    host_id: Int,
                    host_project_id: Int,
                    host_user_admin_id: Int,
                    namespace: String,
                    name: String,
                    web_url: String,
                    is_public: Boolean,
                    created_at: Timestamp = new Timestamp(System.currentTimeMillis())
                  )

object Project {
  implicit val timestampFormat = TimestampFormat
  implicit val projectFormat = Json.writes[Project]
}