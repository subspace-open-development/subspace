package models

import play.api.libs.json._

case class UserReputation (
                              user_id: Int,
                              reputation: Double
                            )

object UserReputation {
  implicit val userReputationFormat = Json.format[UserReputation]

  def getReputation(maybeUR: Option[UserReputation]): Double ={
    maybeUR match {
      case Some(rep) => rep.reputation
      case None => 0
    }
  }
}
