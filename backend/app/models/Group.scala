package models

import java.sql.Timestamp

case class Group(
                    id: Int,
                    name: String,
                    owner_id: Int,
                    disabled: Boolean,
                    created_at: Timestamp,
                    updated_at: Timestamp
                  )

