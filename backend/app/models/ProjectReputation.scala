package models

case class ProjectReputation (
                              project_id: Int,
                              reputation: Double
                            )

object ProjectReputation {

}
