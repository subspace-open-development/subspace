package models.auth

import play.api.libs.json.Json

case class StripeTokenCallback (
                                 token_type: String,
                                 stripe_publishable_key: String,
                                 scope: String,
                                 livemode: Boolean,
                                 stripe_user_id: String,
                                 refresh_token: String,
                                 access_token: String,
                               )

object StripeTokenCallback {
  implicit val stripeTokenCallbackFormat = Json.format[StripeTokenCallback]
}