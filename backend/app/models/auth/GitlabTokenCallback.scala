package models.auth

import play.api.libs.json.Json

case class GitlabTokenCallback (access_token: String, token_type: String, id_token: String, scope: Seq[String], expires_in: Long)

object GitlabTokenCallback {
  implicit val gitlabTokenCallbackFormat = Json.format[GitlabTokenCallback]
}