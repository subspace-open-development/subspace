package models.auth

import play.api.libs.json._

case class LoginCallback (access_token: String, token_type: String, expires_in: Long, refresh_token: String)

object LoginCallback {
  implicit val loginCallbackFormat = Json.format[LoginCallback]
}