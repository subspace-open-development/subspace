package models

import java.sql.Timestamp

case class UserManaChange(
                           id: Int,
                           user_id: Int,
                           mana_used: Double,
                           role: String,
                           contributionId: Option[Int],
                           suggestionId: Option[Int],
                           created_at: Timestamp = new Timestamp(System.currentTimeMillis())
                          )


