package models

import org.gitlab4j.api.models.{Group => GitlabGroup}
import play.api.libs.json._

import scala.collection.JavaConverters._
import scala.util.Try

case class AllGitGroups(
                     gitlabGroups: Seq[GitGroup],
                     githubGroups: Seq[GitGroup]
                   )

case class GitGroup(
                     name: String,
                     description: Option[String],
                     icon: Option[String],
                     webUrl: Option[String],
                     numUsers: Option[Int],
                     numProjects: Option[Int]
                  )

object AllGitGroups {
  implicit val gitGroupFormat = Json.format[GitGroup]
  implicit val gitGroupListFormat = Json.format[AllGitGroups]
}

object GitGroup {
  implicit val gitGroupFormat = Json.format[GitGroup]
  implicit val gitGroupListFormat = Json.format[AllGitGroups]

  def fromGitlabGroup(gitlabGroup: GitlabGroup): GitGroup ={
    GitGroup(
      gitlabGroup.getName,
      Try(gitlabGroup.getDescription).toOption,
      Try(gitlabGroup.getAvatarUrl).toOption,
      Try(gitlabGroup.getWebUrl).toOption,
      Some(0),
      Try(gitlabGroup.getProjects.size).toOption
    )
  }
}


