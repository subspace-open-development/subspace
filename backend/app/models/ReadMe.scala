package models

import java.sql.Timestamp

import models.util.TimestampFormat
import play.api.libs.json.Json

case class ReadMe(
                    content: Option[String]
                  )

object ReadMe {
  implicit val timestampFormat = TimestampFormat
  implicit val readMeFormat = Json.format[ReadMe]
}
