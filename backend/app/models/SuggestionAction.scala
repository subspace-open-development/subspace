package models

import models.util.TimestampFormat
import play.api.libs.json.Json

case class SuggestionAction(
                            suggestionId: Int,
                            action: String
                           )

object SuggestionAction {
  implicit val timestampFormat = TimestampFormat
  implicit val userFormat = Json.format[Suggestion]
}