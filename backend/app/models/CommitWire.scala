package models

import models.util.TimestampFormat
import org.gitlab4j.api.models.Commit
import play.api.libs.json.Json

case class CommitWire(
                   id: String,
                   shortId: String,
                   title: String
                 )

object CommitWire {
  implicit val timestampFormat = TimestampFormat
  implicit val commitFormat = Json.format[CommitWire]

  def toWire(commit: Commit): CommitWire ={
    CommitWire(
      commit.getId,
      commit.getShortId,
      commit.getTitle
    )
  }
}