package utils

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


trait TryImplicits {

  /**
    * Contains extension methods for scala.util.Try type
    */
  implicit class RichTry[T](`try`: Try[T]) {
    /**
      * Converts Try to Future.
      *
      * @param ec execution context for the created Future
      */
    def toFuture(implicit ec: ExecutionContext): Future[T] = `try` match {
      case Success(value) => Future(value)
      case Failure(ex) => Future.failed(ex)
    }
  }
}
