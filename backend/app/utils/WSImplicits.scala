package utils

import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSRequest
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

/**
  * Contains the implicit values for working with types from a play WSClient
  */
trait WSImplicits {
  /**
    * Converts the Future[WSRequest#Response] to a Future[JsValue]. Namely, extracts the response payload from a
    * response body.
    *
    * @param wsResponseF a Future[WSRequest#Response] value to be converted
    * @return a Future with a json, which is the response payload.
    */
 implicit def futureWSResponseToFutureJsValue(wsResponseF: Future[WSRequest#Response])
                                             (implicit executionContext: ExecutionContext): Future[(Int, JsValue)] = wsResponseF.map {
   response => (response.status, Json.parse(response.body))
 }
}
