package utils

/**
  * Contains implicit conversions and extension methods for a Scala string.
  */
trait StringImplicits {

  /**
    * Adds extension methods for a given string
    *
    * @param str a scala string to extend
    */
  implicit class ExtendedRichString(str: String) {
    def short: String = s"...${str takeRight 5}"
  }
}
