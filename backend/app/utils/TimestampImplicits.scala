package utils

import java.sql.Timestamp
import scala.language.implicitConversions
import org.joda.time.DateTime

/**
  * Contains implicit conversions for timestamps
  */
trait TimestampImplicits {
  /**
    * Converts DateTime from joda time to java.sql.Timestamp.
    *
    * Useful for saving timestamps to a database
    */
  implicit def jodaDateTimeToTimestamp(dateTime: DateTime): Timestamp = new Timestamp(dateTime.getMillis)

  /**
    * Converts java.util.Date to Timestamp
    *
    * @param date an instance of java.util.Date
    */
  implicit def dateToTimestamp(date: java.util.Date) = new Timestamp(date.getTime)
}
