package utils

object Token {

  def trimToken(token: String): String ={
    token.replace("Bearer", "").trim
  }

}
