package utils

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * Contains extension methods for scala.Option type
  */
trait OptionImplicits {

  /**
    * Defines extension methods on a given Option[T]
    */
 implicit class RichOption[T](option: Option[T]) {

    /**
      * Converts the extended option to Try and returns Failure with a given exception if the Option was not defined.
      *
      * @param exForNone an exception to wrap into Try if an Option was not defined.
      * @return a Try built from a given Option
      */
   def toTry(exForNone: Exception): Try[T] = option match {
     case Some(value) => Success(value)
     case _ => Failure(exForNone)
   }

    /**
      * Converts the extended option to Future and returns Future.failed with a given exception if the Option was not defined.
      *
      * @param exForNone an exception to wrap into a Future if an Option was not defined.
      * @return a Future built from a given Option
      */
   def toFuture(exForNone: Exception)(implicit ec: ExecutionContext): Future[T] = option match {
     case Some(value) => Future.successful(value)
     case _ => Future.failed(exForNone)
   }
 }
}
