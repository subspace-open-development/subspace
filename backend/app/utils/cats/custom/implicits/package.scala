package utils.cats.custom

import cats._
import cats.data.OptionT
import scala.language.higherKinds
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

/**
  * Contains custom implicit conversions/classes for working with typelevel/cats. <br/>
  * <br/>
  * to enable it you should add the following import <br/>
  *
  * <code>import utils.cats.custom.implicits._</code>
  */
package object implicits {

  /**
    * Unpacks OptionT monad transformer wrapper, that is loaded with Future[A]
    *
    * @param optT an OptionT wrapper that should be "unpacked"
    * @tparam A a type that was lifted to an inner Option of a Future[Option]
    * @return Future of Option of A
    */
  implicit def optionTtoFutureOfOption[A](optT: OptionT[Future, A]): Future[Option[A]] = optT.value

  /**
    * Adds a convenient transformation method for transforming Future[Option] to OptionT.
    * Basically, just an alternative for using OptionT.apply method.
    *
    * @param futureOfOption a Future[Option] that should be transformed to OptionT
    * @tparam A a type that was lifted to an inner Option of a Future[Option]
    */
  implicit class TransformableFutureOfOption[A](futureOfOption: Future[Option[A]]) {
    def optT: OptionT[Future, A] = OptionT[Future, A](futureOfOption)
  }

  /**
    * Adds various extension methods to the Future[Option] class.
    *
    * @param futureOfOption a Future[Option] to be extendsd
    * @tparam A  a type that was lifted to an inner Option of a Future[Option]
    */
  implicit class RichFutureOfOption[A](futureOfOption: Future[Option[A]]) {

    /**
      * Converts this Future[Option] to a Future.failed with the given error, if the inner Option of this Future is
      * not defined (None).
      *
      * @param error an error to be wrapped into a Future.failed
      * @param ec an execution context for the Future
      * @return a Future.failed with a given error.
      */
    def failOnNone(error: Throwable)(implicit ec: ExecutionContext): Future[A] = futureOfOption.flatMap {
      case Some(value) => Future.successful(value)
      case _ => Future.failed(error)
    }
  }

  /**
    * Defines a "liftF" method from an OptionT monad transformer on any Future and lifts this future inside a OptionT.
    * Allows to combine both methods, that return a Future[A] and Future[Option[A]] in a single for-comprehension expression.
    *
    * @param future a Future[A] to be transformed
    * @tparam A a type that was lifted inside a Future
    */
  implicit class TransformableFuture[A](future: Future[A]) {
    def liftF(implicit F: Functor[Future]): OptionT[Future, A] = OptionT.liftF[Future, A](future)(F)
  }

  /**
    * Defines a "liftF" method from an OptionT monad transformer on any Future and lifts this future inside a OptionT.
    * Allows to combine both methods, that return a Future[A] and Future[Option[A]] in a single for-comprehension expression.
    *
    * @param future a Future[A] to be transformed
    * @tparam A a type that was lifted inside a Future
    */
  implicit class RichFuture[A](future: Future[A]) {
    def onFail(error: Throwable)(implicit ec: ExecutionContext): Future[A] = future.recoverWith {
      case _: Throwable => Future.failed(error)
    }

    def onFail(wrapErrorFunc: (Throwable) => Future[A])(implicit ec: ExecutionContext): Future[A] = future.recoverWith {
      case cause: Throwable => wrapErrorFunc(cause)
    }
  }

  /**
    * Defines an "optT" method from an OptionT monad transformer on any Option and lifts this Option inside an OptionT
    *
    * @param option an Option[A] to be transformed
    * @tparam A a type that was lifted inside an Option
    */
  implicit class OptTForOption[A](option: Option[A]) {
    def optT[F[_]](implicit A: Applicative[F]): OptionT[F, A] = OptionT.fromOption[F](option)
  }
}
