package utils

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * Should be mixed in a class to provide a logger for a class name.
  */
trait Logger {
  /**
    * A logback logger for a class where this trait was mixed in.
    */
  import play.api.Logger
  val logger = Logger("application")

  /**
    * A wrapper for a method body that automatically adds input/output logging on a given log level
    *
    * @param infoIn renders the given log message BEFORE the method execution as INFO, if provided
    * @param debugIn renders the given log message BEFORE the method execution as DEBUG, if provided
    * @param callback the actual method body. //TODO: Currently, only support Future return values
    * @param infoOut renders the given log message AFTER the method execution as INFO, if provided
    * @param debugOut renders the given log message AFTER the method execution as DEBUG, if provided
    * @param warnOut renders the given log message AFTER the method execution as WARN if a return value was an undefined Option
    * @param errorOut renders the given log message AFTER the method execution as ERROR if a return value was a failed Future
    * @param ec execution context for the Future
    * @tparam T a return type of a method
    */
  def withLogging[T](infoIn: String = null, debugIn: String = null)(callback: => Future[T])
                    (infoOut: T => String = null, debugOut: T => String = null, warnOut: T => String = null,
                     errorOut: Throwable => String = null)
                    (implicit ec: ExecutionContext): Future[T] = {
    if (infoIn != null) logger.info(infoIn)
    if (debugIn != null) logger.debug(debugIn)

    val resultF: Future[T] = callback

    resultF.onComplete {
      case Success(result) =>
        result match {
          case None =>
            if (warnOut != null) logger.warn(warnOut(result))
          case definedResult =>
            if (infoOut != null) logger.info(infoOut(definedResult))
            if (debugOut != null) logger.debug(debugOut(definedResult))
        }
      case Failure(error) =>
        if (errorOut != null) logger.error(errorOut(error))
    }

    resultF
  }

  def funcName(): String ={
    val stackTrace = Thread.currentThread.getStackTrace
      .map(_.getMethodName)
      .filter(n => {
        !n.contains("funcName") && !n.contains("getStackTrace") && !n.contains("withLogging") && !n.contains("adapted")
      })

    var output = ""

    Try(
      output += {
        stackTrace.map(t => {
          t.split('$').filter(_ != "").find(n => {
            !n.contains("anonfun") && !n.contains("getStackTrace")
          }).getOrElse("")
        }).take(1).mkString("|")
      }
    )

    output
  }

  def className(): String ={
    this.getClass.getSimpleName
  }

}
