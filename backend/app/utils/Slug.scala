package utils

object Slug {
  def apply(input:String) = slugify(input)

  def apply(input:String, limit: Int) = slugify(input, Some(limit))

  def slugify(input: String, maybeLimit: Option[Int] = None): String = {
    import java.text.Normalizer

    val tempOutput = Normalizer.normalize(input, Normalizer.Form.NFD)
      .replaceAll("[^\\w\\s-]", "") // Remove all non-word, non-space or non-dash characters
      .replace('-', ' ')            // Replace dashes with spaces
      .trim                         // Trim leading/trailing whitespace (including what used to be leading/trailing dashes)
      .replaceAll("\\s+", "-")      // Replace whitespace (including newlines and repetitions) with single dashes
      .toLowerCase                  // Lowercase the final results

    if (maybeLimit.nonEmpty && input.length > maybeLimit.get){
      val lastIndex = tempOutput.slice(0, maybeLimit.get).lastIndexOf('-')
      tempOutput.slice(0, lastIndex)
    } else {
      tempOutput
    }
  }
}
