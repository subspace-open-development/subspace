package actors

import akka.actor._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Subscribe
import models.User
import services.WebsocketTopicsService

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

object WebsocketActor {
  def props(out: ActorRef, user: User)(
    implicit executionContext: ExecutionContext,
    websocketTopicsService: WebsocketTopicsService
  ) = Props(new WebsocketActor(out, user))
}

class WebsocketActor (out: ActorRef, user: User)(
  implicit executionContext: ExecutionContext,
  websocketTopicsService: WebsocketTopicsService
) extends Actor {

  val mediator = DistributedPubSub(context.system).mediator

  this.context.system.scheduler.schedule(initialDelay = 1.seconds, interval = 5.seconds) {
    subscribeToAllUserTopics()
    this.self ! ""
  }

  def subscribeToAllUserTopics(): Unit ={
    for {
      topics <- websocketTopicsService.getAllTopics(user.id)
    } yield topics.foreach(topic => mediator ! Subscribe(topic, self))
  }

  def receive = {
    case msg: String => out ! actionMessageJson("socket/printMessage", System.currentTimeMillis().toString)
  }

  def actionMessageJson(action: String, message: String): String ={
    s"""{ "action": "$action", "message": "$message" }"""
  }


}