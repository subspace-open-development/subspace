package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.enums.Hosts
import models.{Host, HostUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HostUserRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class HostUserTable(tag: Tag) extends Table[HostUser](tag, "host_users") {

    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def user_id = column[Int]("user_id")
    def host_id = column[Int]("host_id")
    def host_user_id = column[Int]("host_user_id")
    def host_user_email = column[String]("host_user_email")
    def host_user_username = column[String]("host_user_username")
    def host_user_token = column[String]("host_user_token", O.Unique)
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def active_at = column[Timestamp]("active_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, user_id, host_id, host_user_id, host_user_email, host_user_username, host_user_token, created_at, updated_at, active_at) <> ((HostUser.apply _).tupled, HostUser.unapply)
  }

  val table = TableQuery[HostUserTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  def findByHostAndHostId(host_id: Int, host_user_id: Int): Future[Option[HostUser]] = db.run {
    table
      .filter(u => u.host_user_id === host_user_id)
      .filter(u => u.host_id === host_id)
      .result.headOption
  }

  // FIXME: this pulls ALL members for a host, and will not scale...
  def findByHost(host_id: Int): Future[Seq[HostUser]] = db.run {
    table
      .filter(u => u.host_id === host_id)
      .result
  }

  def findByToken(token: String): Future[Option[HostUser]] = db.run {
    table
      .filter(u => u.host_user_token === token)
      .result.headOption
  }

  def updateToken(oldHostUser: HostUser, token: String): Future[HostUser] = {
    val newHostUser = oldHostUser.copy(host_user_token = token)
    for {
      id <- db.run {table.insertOrUpdate(newHostUser) }
    } yield newHostUser
  }

  def create(hostUser: HostUser): Future[Int] = db.run {
    table += hostUser
  }

  def create(
              user_id: Int,
              host_id: Int,
              host_user_id: Int,
              host_user_email: String,
              host_user_username: String,
              host_user_token: String
            ): Future[HostUser] = {
    val timestamp = new Timestamp(System.currentTimeMillis())

    db.run {
//      (table.map(c => (c.user_id, c.host_id, c.host_user_id, c.host_user_email, c.host_user_username, c.host_user_token))
//        .returning(table.map(_.id))
//        .into((info, id) => HostUser(id, info._1, info._2, info._3, info._4, info._5, info._6, timestamp, timestamp, timestamp))
//      ) += (user_id, host_id, host_user_id, host_user_email, host_user_username, host_user_token)
      insertQuery += HostUser(0, user_id, host_id, host_user_id, host_user_email, host_user_username, host_user_token)
    }
  }
}
