package repositories

import com.google.inject.{Inject, Singleton}
import models.UserReputation
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserReputationRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class UserReputationTable(tag: Tag) extends Table[UserReputation](tag, "user_reputations") {
    def user_id = column[Int]("user_id", O.PrimaryKey, O.Unique)
    def reputation = column[Double]("reputation")

    def * = (user_id, reputation) <> ((UserReputation.apply _).tupled, UserReputation.unapply)
  }

  val table = TableQuery[UserReputationTable]

  def findByUserId(user_id: Int): Future[Option[UserReputation]] = db.run {
    table
      .filter(r => r.user_id === user_id)
      .result
      .headOption
  }

  def create(userRep: UserReputation): Future[Int] = db.run {
    table += userRep
  }

  def insertOrUpdate(userRep: UserReputation): Future[Int] = db.run {
    table.insertOrUpdate(userRep)
  }

  def numRealUsers(minRep: Double): Future[Int] = db.run {
    table
      .filter(_.reputation >= minRep)
      .length
      .result
  }

}