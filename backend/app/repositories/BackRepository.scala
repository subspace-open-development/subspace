package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{Back, Host, User, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class BackRepository @Inject()(val userRepository: UserRepository, val userReputationRepository: UserReputationRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class BackTable(tag: Tag) extends Table[Back](tag, "backs") {

    def project_id = column[Int]("project_id", O.PrimaryKey, O.Unique)
    def cont_id = column[Int]("cont_id")
    def user_id = column[Int]("user_id")
    def user_rep = column[Double]("user_rep")
    def mana_used = column[Double]("mana_used")
    def branch = column[String]("branch")
    def commit = column[String]("commit")
    def maintain_approval = column[Boolean]("maintain_approval")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (project_id, cont_id, user_id, user_rep, mana_used, branch, commit, maintain_approval, created_at, updated_at) <> ((Back.apply _).tupled, Back.unapply)
  }

  val table = TableQuery[BackTable]
  val users = userRepository.table
  val userReputations = userReputationRepository.table

  def create(back: Back): Future[Int] = db.run {
    table.insertOrUpdate(back)
  }

  def findByUserIdAndContributionId(userId: Int, contributionId: Int): Future[Option[Back]] = db.run {
    table
      .filter(u => u.cont_id === contributionId)
      .filter(u => u.user_id === userId)
      .result
      .headOption
  }

  def findUsersByContributionId(contributionId: Int): Future[Seq[(User, UserReputation)]] = db.run {
    table
      .filter(u => u.cont_id === contributionId)
      .join(users).on(_.user_id === _.id)
      .map(_._2)
      .join(userReputations).on(_.id === _.user_id)
      .result
  }

  def findByContributionIdAndDate(contributionId: Int, startDate: Timestamp): Future[Seq[Back]] = db.run {
    table
      .filter(_.cont_id === contributionId)
      .filter(_.created_at >= startDate)
      .result
  }


  def getContributionBacks(contributionId: Int): Future[Seq[Back]] = db.run {
    table
      .filter(u => u.cont_id === contributionId)
      .result
  }

  def deleteByUserIdAndContributionId(userId: Int, contributionId: Int): Future[Int] = db.run {
    table
      .filter(u => u.cont_id === contributionId)
      .filter(u => u.user_id === userId)
      .delete
  }

}
