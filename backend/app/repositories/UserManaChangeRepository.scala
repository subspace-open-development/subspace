package repositories

import java.sql.Timestamp
import java.time.Instant

import com.google.inject.{Inject, Singleton}
import models.{UserManaChange, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import services.DynamicsAndRatingService
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserManaChangeRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class UserManaChangeTable(tag: Tag) extends Table[UserManaChange](tag, "user_mana_changes") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def user_id = column[Int]("user_id")
    def mana_used = column[Double]("mana_used")
    def role = column[String]("role")
    def contribution_id = column[Option[Int]]("contribution_id")
    def suggestion_id = column[Option[Int]]("suggestion_id")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, user_id, mana_used, role, contribution_id, suggestion_id, created_at) <> ((UserManaChange.apply _).tupled, UserManaChange.unapply)
  }

  val table = TableQuery[UserManaChangeTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  def create(manaChange: UserManaChange): Future[UserManaChange] = db.run {
    insertQuery += manaChange
  }

  def findByUserIdAndRecency(userId: Int, startDate: Timestamp): Future[Seq[UserManaChange]] = db.run {
    table
      .filter(r => r.user_id === userId)
      .filter(r => r.created_at >= startDate)
      .result
  }

}