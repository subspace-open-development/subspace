package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.JoinRequest
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class JoinRequestRepository @Inject()(val userReputationRepository: UserReputationRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class JoinRequestTable(tag: Tag) extends Table[JoinRequest](tag, "join_requests") {

    def user_id = column[Int]("user_id")
    def project_id = column[Int]("project_id")
    def status = column[String]("status")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def pk = primaryKey("pk_a", (user_id, project_id))

    def * = (user_id, project_id, status, created_at, updated_at) <> ((JoinRequest.apply _).tupled, JoinRequest.unapply)
  }

  val table = TableQuery[JoinRequestTable]

  def findByProjectId(project_id: Int): Future[Seq[JoinRequest]] = db.run {
    table
      .filter(u => u.project_id === project_id)
      .result
  }

  def create(user_id: Int, project_id: Int): Future[Int] = db.run {
    val jr = JoinRequest(user_id, project_id)
    table.insertOrUpdate(jr)
  }

}
