package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.enums.ContributionState
import models.{Contribution, EditContribution, User}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}


@Singleton
class ContributionRepository @Inject()(val userRepository: UserRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  import services.DynamicsAndRatingService._

  class ContributionTable(tag: Tag) extends Table[Contribution](tag, "contributions") {

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def iid = column[Int]("iid")
    def project_id = column[Int]("project_id")
    def task_id = column[Int]("task_id")
    def user_id = column[Int]("user_id")
    def branch = column[String]("branch")
    def commit = column[String]("commit")
    def state = column[String]("state")
    def state_change_at = column[Option[Timestamp]]("state_change_at")
    def state_future = column[Option[String]]("state_future")
    def closed = column[Boolean]("closed")
    def title = column[String]("title")
    def description = column[String]("description")
    def auto_update_with_commits = column[Boolean]("auto_update_with_commits")
    def mana_used = column[Double]("mana_used")
    def user_rep = column[Double]("user_rep")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, iid, project_id, task_id, user_id, branch, commit, state, state_change_at, state_future, closed, title, description, auto_update_with_commits, mana_used, user_rep, created_at, updated_at) <> ((Contribution.apply _).tupled, Contribution.unapply)
  }

  val table = TableQuery[ContributionTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val users = userRepository.table

  def findByProjectIdAndIid(projectId: Int, iid: Int): Future[Option[Contribution]] = db.run {
    table
      .filter(_.project_id === projectId)
      .filter(_.iid === iid)
      .result
      .headOption
  }

  def findByProjectIdAndIid_withUsers(projectId: Int, iid: Int): Future[Option[(Contribution, User)]] = db.run {
    table
      .filter(_.project_id === projectId)
      .filter(_.iid === iid)
      .join(users)
      .on(_.user_id === _.id)
      .result
      .headOption
  }

  def findById(id: Int): Future[Option[Contribution]] = db.run {
    table.filter(_.id === id).result.headOption
  }

  def findAllByIds(ids: Set[Int]): Future[Seq[Contribution]] = db.run {
    table.filter(_.id.inSet(ids)).result
  }

  def deleteById(contributionId: Int): Future[Int] = db.run {
    table
      .filter(_.id === contributionId)
      .delete
  }

  def findById_withUsers(id: Int): Future[Option[(Contribution, User)]] = db.run {
    table
      .filter(_.id === id)
      .join(users)
      .on(_.user_id === _.id)
      .result
      .headOption
  }

  def getAllByProjectId(project_id: Int): Future[Seq[Contribution]] = db.run {
    table.filter(c => c.project_id === project_id).result
  }

  def getAllUndecided(): Future[Seq[Contribution]] = db.run {
    table
      .filterNot(_.state === ContributionState.ACCEPTED)
      .filterNot(_.state === ContributionState.DECLINED)
      .result
  }

  def getAllByProjectId_withUsers(project_id: Int): Future[Seq[(Contribution, User)]] = db.run {
    table
      .filter(_.project_id === project_id)
      .join(users).on(_.user_id === _.id)
      .result
  }

  def list(): Future[Seq[Contribution]] = db.run {
    table.result
  }

  def create(contribution: Contribution): Future[Contribution] = {
    for {
      maxIidContribution <- db.run { table.filter(u => u.project_id === contribution.projectId).sortBy(_.iid.desc).result.headOption }
      newContributionIid = if (maxIidContribution.isDefined) maxIidContribution.get.iid + 1 else 1
      newContribution <- db.run { insertQuery += contribution.copy(iid = newContributionIid) }
    } yield newContribution
  }

  def update(editCont: EditContribution, oldCont: Contribution): Future[Int] = {
    val newCont = editCont.buildTask(oldCont)

    db.run {
      table.insertOrUpdate(newCont)
    }
  }

  def setEvaluation(contribution: Contribution, ce: ContributionEvaluation): Future[Int] = {
    val changeAtTimestamp = if (ce.secondsUntilChange.isDefined){
      Some(new Timestamp(System.currentTimeMillis() + (ce.secondsUntilChange.get * 1000)))
    } else {
      None
    }

    val closed = ce.currentState == ContributionState.ACCEPTED || ce.currentState == ContributionState.DECLINED

    val updatedContribution = contribution.copy(
      closed = closed,
      state = ce.currentState, // FIXME: is this necessary? was the state being changed somewhere else??
      stateFuture = ce.stateAfterChange,
      stateChangeAt = changeAtTimestamp,
    )
    db.run {
      table.insertOrUpdate(updatedContribution)
    }
  }

}
