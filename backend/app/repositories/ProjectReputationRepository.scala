package repositories

import com.google.inject.{Inject, Singleton}
import models.{ProjectReputation, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProjectReputationRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class ProjectReputationTable(tag: Tag) extends Table[ProjectReputation](tag, "project_reputations") {
    def project_id = column[Int]("project_id", O.PrimaryKey, O.Unique)
    def reputation = column[Double]("reputation")

    def * = (project_id, reputation) <> ((ProjectReputation.apply _).tupled, ProjectReputation.unapply)
  }

  val projectReputations = TableQuery[ProjectReputationTable]

  def findByProjectId(project_id: Int): Future[Option[ProjectReputation]] = db.run {
    projectReputations
      .filter(r => r.project_id === project_id)
      .result
      .headOption
  }

  def create(projectRep: ProjectReputation): Future[Int] = db.run {
    projectReputations += projectRep
  }

  def update(projectRep: ProjectReputation): Future[Int] = db.run {
    projectReputations.insertOrUpdate(projectRep)
  }

  def createOrUpdate(projectRep: ProjectReputation): Future[Int] = {
    for {
      maybeProjectRep <- findByProjectId(projectRep.project_id)
      result <- maybeProjectRep match {
        case Some(ur) => update(projectRep)
        case None => create(projectRep)
      }
    } yield result
  }
}