package repositories

import java.sql.Timestamp
import com.google.inject.{Inject, Singleton}
import models.{Project, User}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType
import scala.concurrent.{ExecutionContext, Future}


@Singleton
class ProjectRepository @Inject()(val projectUserRepository: ProjectUserRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class ProjectTable(tag: Tag) extends Table[Project](tag, "projects") {

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def host_id = column[Int]("host_id")
    def host_project_id = column[Int]("host_project_id")
    def host_user_admin_id = column[Int]("host_user_admin_id")
    def namespace = column[String]("namespace")
    def name = column[String]("name")
    def web_url = column[String]("web_url")
    def is_public = column[Boolean]("is_public")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def pk = primaryKey("pk_a", (host_id, host_project_id))

    def * = (id, host_id, host_project_id, host_user_admin_id, namespace, name, web_url, is_public, created_at) <> ((Project.apply _).tupled, Project.unapply)
  }

  val table = TableQuery[ProjectTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val projectUsers = projectUserRepository.table

  def getAll(): Future[Seq[Project]] = db.run {
    table.result
  }

  def getPublicProjectsAndUserCount(limit: Int, offset: Int): Future[Seq[(Project, Int)]] = {
    for {
      projectAndMaybeUserCount <- db.run {
        projectUsers
          .groupBy(_.project_id)
          .map(r => (r._1, r._2.size))
          .joinRight(table.filter(_.is_public === true))
          .on(_._1 === _.id)
          .drop(offset)
          .take(limit)
          .sortBy(_._1.getOrElse((0, 0))._2)
          .result
      }
      projectAndUserCount = projectAndMaybeUserCount.map(x => (x._2, x._1.getOrElse((0, 0))._2))
    } yield {
      projectAndUserCount.sortWith((a, b) => a._2 > b._2)
    }
  }

  def findByHostIdAndHostProjectId(host_id: Int, host_project_id: Int): Future[Option[Project]] = db.run {
    table
      .filter(_.host_id === host_id)
      .filter(_.host_project_id === host_project_id)
      .result
      .headOption
  }

  def findByProjectId(project_id: Int): Future[Option[Project]] = db.run {
    table
      .filter(_.id === project_id)
      .result
      .headOption
  }

  def findByHostIdNamespaceName(hostId: Int, namespace: String, name: String): Future[Option[Project]] = db.run {
    table
      .filter(_.host_id === hostId)
      .filter(_.namespace === namespace)
      .filter(_.name === name)
      .result
      .headOption
  }

  def findByProjectIds(projectIds: Set[Int]): Future[Seq[Project]] = db.run {
    table
      .filter(_.id.inSet(projectIds))
      .result
  }

  def createIfNotExists(p: Project): Future[Project] = {
    val existsFuture = db.run {
      table
        .filter(_.host_id === p.host_id)
        .filter(_.host_project_id === p.host_project_id)
        .exists.result
    }

    for {
      exists <- existsFuture
      projFuture <- exists match {
        case true => db.run {
          table
            .filter(_.host_id === p.host_id)
            .filter(_.host_project_id === p.host_project_id)
            .result.head
        }
        case false => db.run {
          insertQuery += p
        }
      }
    } yield projFuture
  }

}
