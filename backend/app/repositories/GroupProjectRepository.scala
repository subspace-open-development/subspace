package repositories

import java.sql.Timestamp
import com.google.inject.{Inject, Singleton}
import models.{GroupProject, GroupUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class GroupProjectRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class GroupProjectTable(tag: Tag) extends Table[GroupProject](tag, "group_projects") {

    def group_id = column[Int]("group_id")
    def project_id = column[Int]("project_id")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def pk = primaryKey("pk_a", (group_id, project_id))

    def * = (group_id, project_id, created_at) <> ((GroupProject.apply _).tupled, GroupProject.unapply)
  }

  val table = TableQuery[GroupProjectTable]

  def findByGroupId(group_id: Int): Future[Seq[GroupProject]] = db.run {
    table.filter(r => r.group_id === group_id).result
  }

  def findByProjectId(project_id: Int): Future[Seq[GroupProject]] = db.run {
    table.filter(r => r.project_id === project_id).result
  }

}
