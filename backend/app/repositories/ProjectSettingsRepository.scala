package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{ProjectSettings, ProjectUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProjectSettingsRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class ProjectSettingsTable(tag: Tag) extends Table[ProjectSettings](tag, "project_settings") {

    def project_id = column[Int]("project_id", O.PrimaryKey)
    def support_proportion_required = column[Double]("support_proportion_required")
    def threshold_halflife_seconds = column[Int]("threshold_halflife_seconds")
    def seconds_from_commit_until_decline = column[Int]("seconds_from_commit_until_decline")
    def seconds_from_start_until_decline = column[Int]("seconds_from_start_until_decline")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (project_id, support_proportion_required, threshold_halflife_seconds, seconds_from_commit_until_decline, seconds_from_start_until_decline, created_at, updated_at) <> ((ProjectSettings.apply _).tupled, ProjectSettings.unapply)
  }

  val table = TableQuery[ProjectSettingsTable]

  def findByProjectId(project_id: Int): Future[Option[ProjectSettings]] = db.run {
    table
      .filter(r => r.project_id === project_id)
      .result
      .headOption
  }

  def insertOrUpdate(projectSettings: ProjectSettings): Future[Int] = db.run {
    table.insertOrUpdate(projectSettings)
  }

}
