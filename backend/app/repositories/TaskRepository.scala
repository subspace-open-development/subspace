package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{EditTask, Task, TaskWire, User}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TaskRepository @Inject()(val userRepository: UserRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class TaskTable(tag: Tag) extends Table[Task](tag, "tasks") {

    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def iid = column[Int]("iid")
    def parent_id = column[Option[Int]]("parent_id")
    def project_id = column[Int]("project_id")
    def owner_id = column[Int]("owner_id")
    def branch = column[Option[String]]("branch")
    def commit = column[Option[String]]("commit")
    def closed = column[Boolean]("closed")
    def title = column[String]("title")
    def description = column[String]("description")
    def autoRebase = column[Boolean]("auto_rebase")
    def ongoing = column[Boolean]("ongoing")
    def no_direct_contributions = column[Boolean]("no_direct_contributions")
    def auto_merge_up = column[Boolean]("auto_merge_up")
    def require_owner_approval = column[Boolean]("require_owner_approval")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, iid, parent_id, project_id, owner_id, branch, commit, closed, title, description, autoRebase, ongoing, no_direct_contributions, auto_merge_up, require_owner_approval, created_at, updated_at) <> ((Task.apply _).tupled, Task.unapply)
  }

  val table = TableQuery[TaskTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val users = userRepository.table

  def findById(id: Int): Future[Option[Task]] = db.run {
    table.filter(u => u.id === id).result.headOption
  }

  def findByProjectIdAndIid(projectId: Int, iid: Int): Future[Option[Task]] = db.run {
    table.filter(u => u.project_id === projectId).filter(u => u.iid === iid).result.headOption
  }

  def findByProjectIdAndIid_withUser(projectId: Int, iid: Int): Future[Option[(Task, User)]] = db.run {
    table
      .filter(u => u.project_id === projectId)
      .filter(u => u.iid === iid)
      .join(users)
      .on(_.owner_id === _.id)
      .result
      .headOption
  }

  def findById_withUser(id: Int): Future[Option[(Task, User)]] = db.run {
    table
      .filter(u => u.id === id)
      .join(users)
      .on(_.owner_id === _.id)
      .result
      .headOption
  }

  def findChildrenOfTaskWithId(id: Int): Future[Seq[Task]] = db.run {
    table
      .filter(u => u.parent_id === id)
      .result
  }

  def deleteById(id: Int): Future[Int] = db.run {
    table
      .filter(c => c.id === id)
      .delete
  }

  def updateBranch(task: Task, branch: String): Future[Int] = db.run {
    table.insertOrUpdate(task.copy(branch = Some(branch)))
  }

  def create(task: Task): Future[Task] = {
    for {
      maxIidTask <- db.run { table.filter(u => u.project_id === task.projectId).sortBy(_.iid.desc).result.headOption }
      newTaskIid = if (maxIidTask.isDefined) maxIidTask.get.iid + 1 else 1
      newTask <- db.run { insertQuery += task.copy(iid = newTaskIid) }
    } yield newTask
  }

  def update(editTask: EditTask, oldTask: Task): Future[Int] = {
    val newTask = editTask.buildTask(oldTask)

    db.run {
      table.insertOrUpdate(newTask)
    }
  }

  def getTaskTreeWire(projectId: Int): Future[Seq[TaskWire]] = {
    for {
      projectTasks <- db.run {
        table.filter(t => t.project_id === projectId).result
      }
    } yield createTaskWireTreeFromTasks(projectTasks)
  }

  def getTaskListWire(projectId: Int): Future[Seq[TaskWire]] = {
    for {
      projectTasks <- db.run {
        table.filter(t => t.project_id === projectId).result
      }
    } yield projectTasks.map(_.toTaskWire())
  }

  private def createTaskWireTreeFromTasks(tasks: Seq[Task]): Seq[TaskWire] ={
    val taskWires = tasks.map(_.toTaskWire())


    var parents = taskWires.filter(t => t.task.parentId.isEmpty)
    var children = taskWires.filter(t => t.task.parentId.nonEmpty)

    while (children.nonEmpty){
      children = children.filter(c => {
        val parentBooleans = parents.map(p => addChild(c, p))
        parents = parentBooleans.map(_._1)
        !parentBooleans.exists(_._2)
      })
    }

    assert(children.isEmpty, "Not all tasks found a place in the task tree!!")

    parents
  }

  private def addChild(task: TaskWire, tree: TaskWire): (TaskWire, Boolean) ={
    val parentId = tree.task.id
    val tasksParent = task.task.parentId.get

    if (tasksParent == parentId){
      val childrenTree = if (tree.children.isDefined) tree.children.get ++ Seq(task) else Seq(task)
      (tree.copy(children = Some(childrenTree)), true)
    } else if (tree.children.nonEmpty){
      val results = tree.children.get.map(c => addChild(task, c))

      val taskWires = results.map(_._1)
      val wasAdded = results.exists(_._2)

      (tree.copy(children = Some(taskWires)), wasAdded)
    } else {
      (tree, false)
    }
  }

  private def getTreeSize(taskWires: Seq[TaskWire]): Int = {
    taskWires.map(t => {
      if (t.children.nonEmpty){
        getTreeSize(t.children.get)
      } else {
        0
      } + 1
    }).sum
  }

}
