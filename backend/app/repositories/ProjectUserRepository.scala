package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{GroupUser, ProjectUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProjectUserRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class ProjectUserTable(tag: Tag) extends Table[ProjectUser](tag, "project_users") {

    def project_id = column[Int]("project_id")
    def user_id = column[Int]("user_id")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def pk = primaryKey("pk_a", (project_id, user_id))

    def * = (project_id, user_id, created_at) <> ((ProjectUser.apply _).tupled, ProjectUser.unapply)
  }

  val table = TableQuery[ProjectUserTable]

  def findByProjectId(project_id: Int): Future[Seq[ProjectUser]] = db.run {
    table.filter(r => r.project_id === project_id).result
  }

  def findByUserId(user_id: Int): Future[Seq[ProjectUser]] = db.run {
    table.filter(r => r.user_id === user_id).result
  }

  def addProjectUser(projectId: Int, userId: Int): Future[Int] = db.run {
    table.insertOrUpdate(ProjectUser(projectId, userId))
  }

}
