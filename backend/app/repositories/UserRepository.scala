package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{User, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserRepository @Inject() (val userReputationRepository: UserReputationRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class UserTable(tag: Tag) extends Table[User](tag, "users") {

    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def username = column[String]("username")
    def email = column[String]("email", O.Unique)
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, username, email, created_at) <> ((User.apply _).tupled, User.unapply)
  }

  val table = TableQuery[UserTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val userReputations = userReputationRepository.table

  def findById(id: Int): Future[Option[User]] = db.run {
    table.filter(u => u.id === id).result.headOption
  }

  def findByIds(ids: Set[Int]): Future[Seq[User]] = db.run {
    table.filter(_.id.inSet(ids)).result
  }

  def findByIds_withReps(ids: Set[Int]): Future[Seq[(User, Option[UserReputation])]] = db.run {
    table.filter(_.id.inSet(ids))
      .joinLeft(userReputations)
      .on(_.id === _.user_id)
      .result
  }


  def findByEmail(email: String): Future[Option[User]] = db.run {
    table.filter(u => u.email === email).result.headOption
  }

  def create(user: User): Future[Int] = db.run {
    table += user
  }

  def create(username: String, email: String): Future[User] = db.run {
    val user = User(0, username, email)
    insertQuery += user
  }

}
