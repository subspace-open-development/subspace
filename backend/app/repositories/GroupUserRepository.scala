package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{Group, GroupUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class GroupUserRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class GroupUserTable(tag: Tag) extends Table[GroupUser](tag, "group_users") {

    def group_id = column[Int]("group_id")
    def user_id = column[Int]("user_id")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def pk = primaryKey("pk_a", (group_id, user_id))

    def * = (group_id, user_id, created_at) <> ((GroupUser.apply _).tupled, GroupUser.unapply)
  }

  val table = TableQuery[GroupUserTable]

  def findByUserId(user_id: Int): Future[Seq[GroupUser]] = db.run {
    table.filter(r => r.user_id === user_id).result
  }

}
