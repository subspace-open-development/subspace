package repositories

import java.sql.Timestamp
import com.google.inject.{Inject, Singleton}
import models.Group
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class GroupRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class GroupTable(tag: Tag) extends Table[Group](tag, "groups") {

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def owner_id = column[Int]("owner_id")
    def disabled = column[Boolean]("disabled")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, name, owner_id, disabled, created_at, updated_at) <> ((Group.apply _).tupled, Group.unapply)
  }

  val table = TableQuery[GroupTable]

  def getAll(): Future[Seq[Group]] = db.run {
    table.result
  }

  def findById(id: Int): Future[Option[Group]] = db.run {
    table.filter(r => r.id === id).result.headOption
  }

}
