package repositories

import com.google.inject.{Inject, Singleton}
import models.{SshKey, User}
import org.gitlab4j.api.models.{User => GitlabUser}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


@Singleton
class SshKeyRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class SshKeyTable(tag: Tag) extends Table[SshKey](tag, "users") {

    def project_id = column[Int]("project_id", O.PrimaryKey, O.Unique)
    def public_key = column[String]("public_key")
    def private_key = column[String]("private_key")
    def service = column[String]("service") // github, bitbucket, etc.

    def * = (project_id, public_key, private_key, service) <> ((SshKey.apply _).tupled, SshKey.unapply)
  }

  val keys = TableQuery[SshKeyTable]

  def findByProjectId(project_id: Int): Future[Option[SshKey]] = db.run {
    keys.filter(_.project_id === project_id).result.headOption
  }

}
