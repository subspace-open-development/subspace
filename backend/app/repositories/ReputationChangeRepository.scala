package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{ReputationChange, User, util}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType
import utils.Logger
import utils.cats.custom.implicits._
import scala.concurrent.{ExecutionContext, Future}


@Singleton
class ReputationChangeRepository @Inject()(val contributionRepository: ContributionRepository,
                                           dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class ReputationChangeTable(tag: Tag) extends Table[ReputationChange](tag, "reputation_changes") {
    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def is_user = column[Boolean]("is_user")
    def is_project = column[Boolean]("is_project")
    def project_id = column[Int]("project_id")
    def contribution_id = column[Int]("contribution_id")
    def suggestion_id = column[Option[Int]]("suggestion_id")
    def user_id = column[Int]("user_id")
    def rep_change = column[Double]("rep_change")
    def points = column[Double]("points")
    def change_type = column[String]("change_type")
    def contribution_outcome = column[String]("contribution_outcome")
    def suggestion_outcome = column[Option[String]]("suggestion_outcome")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, is_user, is_project, project_id, contribution_id, suggestion_id, user_id, rep_change, points, change_type, contribution_outcome, suggestion_outcome, created_at) <> ((ReputationChange.apply _).tupled, ReputationChange.unapply)
  }

  val table = TableQuery[ReputationChangeTable]

  def findUserChangesByContIdAndUserId(cont_id: Int, user_id: Int): Future[Option[ReputationChange]] = db.run {
    table
      .filter(r => r.contribution_id === cont_id)
      .filter(r => r.user_id === user_id)
      .filter(r => r.is_user === true)
      .result.headOption
  }

  def findUserChangesByContId(cont_id: Int): Future[Seq[ReputationChange]] = db.run {
    table
      .filter(r => r.contribution_id === cont_id)
      .filter(r => r.is_user === true)
      .result
  }

  def findUserChangesByUserId(user_id: Int): Future[Seq[ReputationChange]] = db.run {
    table
      .filter(r => r.user_id === user_id)
      .filter(r => r.is_user === true)
      .result
  }

  def zeroBottonProjectReputationChanges(project_id: Int): Future[Unit] = {
    for {
      reputationTotal <- db.run {
        table
          .filter(r => r.project_id === project_id)
          .filter(r => r.is_project === true)
          .map(_.rep_change)
          .sum
          .result
      }
      _ <- if (reputationTotal.getOrElse(0.0) < 0){
        db.run {
          table
            .filter(r => r.project_id === project_id)
            .filter(r => r.is_project === true)
            .map(_.rep_change)
            .update(0)
        }
      } else Future.successful({})
    } yield Future.successful({})
  }

  def zeroBottomUserReputationChanges(user_id: Int): Future[Unit] = {
    for {
      reputationTotal <- db.run {
        table
          .filter(r => r.user_id === user_id)
          .filter(r => r.is_user === true)
          .map(_.rep_change)
          .sum
          .result
      }
      _ <- if (reputationTotal.getOrElse(0.0) < 0){
        db.run {
          table
            .filter(r => r.user_id === user_id)
            .filter(r => r.is_user === true)
            .map(_.rep_change)
            .update(0)
        }
      } else Future.successful({})
    } yield Future.successful({})
  }

  def zeroAllUserReputationChanges(user_ids: Set[Int]): Future[Set[Unit]] = {
    Future.sequence(user_ids.map(id => zeroBottomUserReputationChanges(id)))
  }

  def findProjectChangesByProjectId(project_id: Int): Future[Seq[ReputationChange]] = db.run {
    table
      .filter(r => r.project_id === project_id)
      .filter(r => r.is_project === true)
      .result
  }

  def create(repChange: ReputationChange): Future[Int] = db.run {
    table += repChange
  }

  def getUserRepChangeSum: Future[Option[Double]] = db.run {
    table
      .filter(r => r.is_user === true)
      .map(_.rep_change)
      .sum
      .result
  }
}