package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{Suggestion, User, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SuggestionRepository @Inject()(val userRepository: UserRepository, val userReputationRepository: UserReputationRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val suggestionLimit = 3

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class SuggestionTable(tag: Tag) extends Table[Suggestion](tag, "suggestions") {

    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def iid = column[Int]("iid")
    def project_id = column[Int]("project_id")
    def cont_id = column[Int]("cont_id")
    def user_id = column[Int]("user_id")
    def user_rep = column[Double]("user_rep")
    def mana_used = column[Double]("mana_used")
    def title = column[String]("title")
    def description = column[String]("description")
    def commit_when_made = column[String]("commit_when_made")
    def commit_when_addressed = column[String]("commit_when_addressed")
    def status = column[String]("status")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, iid, project_id, cont_id, user_id, user_rep, mana_used, title, description, commit_when_made, commit_when_addressed, status, created_at, updated_at) <> ((Suggestion.apply _).tupled, Suggestion.unapply)
  }

  val table = TableQuery[SuggestionTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val users = userRepository.table
  val userReputations = userReputationRepository.table

  def create(suggestion: Suggestion): Future[Suggestion] = {
    for {
      maxIidSuggestion <- db.run {
        table
          .filter(u => u.project_id === suggestion.projectId)
          .filter(u => u.cont_id === suggestion.contId)
          .sortBy(_.iid.desc)
          .result
          .headOption
      }
      newSuggestionIid = if (maxIidSuggestion.isDefined) maxIidSuggestion.get.iid + 1 else 1
      newSuggestion <- db.run { insertQuery += suggestion.copy(iid = newSuggestionIid) }
    } yield newSuggestion
  }

  def changeStatus(suggestion: Suggestion, status: String): Future[Int] = db.run {
    table.insertOrUpdate(suggestion.copy(status = status))
  }

  def findById(id: Int): Future[Option[Suggestion]] = db.run {
    table.filter(r => r.id === id).result.headOption
  }

  def findById_withUser(id: Int): Future[Option[(Suggestion, User, Option[UserReputation])]] = db.run {
    table
      .filter(r => r.id === id)
      .join(users).on(_.user_id === _.id)
      .joinLeft(userReputations).on(_._1.user_id === _.user_id)
      .map(x => (x._1._1, x._1._2, x._2))
      .result
      .headOption
  }

  def findByContributionAndUser(userId: Int, contributionId: Int): Future[Seq[Suggestion]] = db.run {
    table
      .filter(r => r.cont_id === contributionId)
      .filter(r => r.user_id === userId)
      .result
  }

  def getByContributionId(contributionId: Int): Future[Seq[Suggestion]] = db.run {
    table
      .filter(r => r.cont_id === contributionId)
      .result
  }

  def getSuggestionList(projectId: Int): Future[Seq[Suggestion]] = db.run {
    table.filter(t => t.project_id === projectId).result
  }

  def getSuggestionList_withUsers(projectId: Int): Future[Seq[(Suggestion, User, Option[UserReputation])]] = db.run {
    table
      .filter(t => t.project_id === projectId)
      .join(users).on(_.user_id === _.id)
      .joinLeft(userReputations).on(_._1.user_id === _.user_id)
      .map(x => (x._1._1, x._1._2, x._2))
      .result
  }

  def deleteById(suggestionId: Int): Future[Int] = db.run {
    table
      .filter(u => u.id === suggestionId)
      .delete
  }

  def isUserAtSuggestionLimit(userId: Int, contributionId: Int): Future[Boolean] = {
    for {
      numUserSuggestions <- db.run {
        table
          .filter(r => r.cont_id === contributionId)
          .filter(r => r.user_id === userId)
          .length
          .result
      }
    } yield numUserSuggestions >= suggestionLimit
  }

}
