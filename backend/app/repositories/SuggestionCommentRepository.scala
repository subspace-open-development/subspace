package repositories

import java.sql.Timestamp

import com.google.inject.{Inject, Singleton}
import models.{SuggestionComment, User}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SuggestionCommentRepository @Inject()(val userRepository: UserRepository, dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class SuggestionCommentTable(tag: Tag) extends Table[SuggestionComment](tag, "suggestion_comments") {

    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def parent_id = column[Option[Int]]("parent_id")
    def suggestion_id = column[Int]("suggestion_id")
    def author_id = column[Int]("author_id")
    def comment = column[String]("comment")
    def created_at = column[Timestamp]("created_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (id, parent_id, suggestion_id, author_id, comment, created_at, updated_at) <> ((SuggestionComment.apply _).tupled, SuggestionComment.unapply)
  }

  val table = TableQuery[SuggestionCommentTable]
  val insertQuery = table returning table.map(_.id) into ((record, id) => record.copy(id = id))

  val users = userRepository.table

  def create(suggestion: SuggestionComment): Future[Int] = db.run {
    table += suggestion
  }

  def getSuggestionCommentList(suggestionId: Int): Future[Seq[SuggestionComment]] = db.run {
    table
      .filter(t => t.suggestion_id === suggestionId)
      .sortBy(_.id.desc)
      .result
  }

  def getSuggestionCommentList_withUsers(suggestionId: Int): Future[Seq[(SuggestionComment, User)]] = db.run {
    table
      .filter(t => t.suggestion_id === suggestionId)
      .sortBy(_.id.desc)
      .join(users)
      .on(_.author_id === _.id)
      .result
  }

}
