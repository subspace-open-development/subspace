package repositories

import java.sql.Timestamp
import java.time.Instant

import com.google.inject.{Inject, Singleton}
import models.{UserMana, UserReputation}
import play.api.db.slick.DatabaseConfigProvider
import services.DynamicsAndRatingService
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.SqlType

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserManaRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  class UserManaTable(tag: Tag) extends Table[UserMana](tag, "user_mana") {
    def user_id = column[Int]("user_id", O.PrimaryKey, O.Unique)
    def mana = column[Double]("mana")
    def updated_at = column[Timestamp]("updated_at", SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))

    def * = (user_id, mana, updated_at) <> ((UserMana.apply _).tupled, UserMana.unapply)
  }

  val table = TableQuery[UserManaTable]

  def findByUserId(user_id: Int): Future[Option[UserMana]] = db.run {
    table
      .filter(r => r.user_id === user_id)
      .result
      .headOption
  }

  def insertOrUpdate(userMana: UserMana): Future[Int] = db.run {
    table.insertOrUpdate(userMana)
  }

  def getUserMana(userId: Int): Future[UserMana] ={
    val totalPossibleMana = DynamicsAndRatingService.maxMana

    val hoursToRecharge = totalPossibleMana / DynamicsAndRatingService.manaRechargePerHour
    val hourInMs = 60 * 60 * 1000
    val msToRecharge = hoursToRecharge * hourInMs

    for {
      maybeLastMana <- findByUserId(userId)
      newMana = {
        val lastMana = maybeLastMana.getOrElse(UserMana(userId, DynamicsAndRatingService.maxMana))
        val msSinceLastMana = System.currentTimeMillis() - lastMana.updated_at.getTime
        val rechargedMana = (msSinceLastMana / msToRecharge) * DynamicsAndRatingService.maxMana

        UserMana(userId, Math.min(Math.floor(lastMana.mana + rechargedMana), DynamicsAndRatingService.maxMana))
      }
      _ <- insertOrUpdate(newMana)
    } yield {
      newMana
    }
  }

}