package repositories

import com.google.inject.{Inject, Singleton}
import models.Host
import models.enums.Hosts
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HostRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class HostTable(tag: Tag) extends Table[Host](tag, "hosts") {
    def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)
    def name = column[String]("name", O.Unique)

    def * = (id, name) <> ((Host.apply _).tupled, Host.unapply)
  }

  val hosts = TableQuery[HostTable]

  def findById(id: Int): Future[Option[Host]] = db.run {
    hosts.filter(u => u.id === id).result.headOption
  }

  def findByName(name: String): Future[Option[Host]] = db.run {
    hosts.filter(u => u.name === name).result.headOption
  }

  def create(host: Host): Future[Int] = db.run {
    hosts += host
  }
}
