package jobs

import javax.inject.Inject

import akka.actor.ActorSystem
import scala.concurrent.ExecutionContext


class AllScheduledJobs @Inject()(
                                  evaluateOpenContributionsJob: EvaluateContributionsJob,
                                  actorSystem: ActorSystem
                        )(implicit executionContext: ExecutionContext) {

}