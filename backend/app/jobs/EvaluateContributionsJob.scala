package jobs

import javax.inject.Inject
import akka.actor.ActorSystem
import services.DynamicsAndRatingService

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class EvaluateContributionsJob @Inject()(
                                              dynamicsAndRatingService: DynamicsAndRatingService,
                                              actorSystem: ActorSystem
                                            )(implicit executionContext: ExecutionContext) {

  actorSystem.scheduler.schedule(initialDelay = 1.minute, interval = 1.minute) {
    dynamicsAndRatingService.evaluateContributions()
  }
}