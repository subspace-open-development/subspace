# --- !Ups

CREATE TABLE "user_mana_changes" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INT NOT NULL,
  "mana_used" DECIMAL NOT NULL,
  "role" VARCHAR NOT NULL,
  "contribution_id" INT,
  "suggestion_id" INT,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE "user_mana_changes" ADD CONSTRAINT "user_mana_changes_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

ALTER TABLE "contributions" ADD COLUMN mana_used DECIMAL DEFAULT 0;
ALTER TABLE "contributions" ADD COLUMN user_rep DECIMAL DEFAULT 0;

# --- !Downs

ALTER TABLE "user_mana_changes" DROP CONSTRAINT "user_mana_changes_fk_user_id";
DROP TABLE "user_mana_changes";

ALTER TABLE "contributions" DROP COLUMN mana_used;
ALTER TABLE "contributions" DROP COLUMN user_rep;