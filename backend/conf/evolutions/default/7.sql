# --- !Ups

CREATE TABLE "backs" (
  "project_id" INT NOT NULL,
  "cont_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "user_rep" DECIMAL NOT NULL,
  "value" DECIMAL NOT NULL,
  "branch" VARCHAR,
  "commit" VARCHAR,
  "maintain_approval" BOOLEAN DEFAULT TRUE,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY("cont_id", "user_id")
);

ALTER TABLE "backs" ADD CONSTRAINT "backs_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "backs" ADD CONSTRAINT "backs_fk_cont_id" FOREIGN KEY("cont_id") REFERENCES "contributions"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "backs" ADD CONSTRAINT "backs_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "backs" DROP CONSTRAINT "backs_fk_project_id";
ALTER TABLE "backs" DROP CONSTRAINT "backs_fk_cont_id";
ALTER TABLE "backs" DROP CONSTRAINT "backs_fk_user_id";

DROP TABLE "backs";
