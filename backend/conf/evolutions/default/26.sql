# --- !Ups

ALTER TABLE "host_users" ADD CONSTRAINT "host_users_unique_token" UNIQUE (host_user_token);

# --- !Downs

ALTER TABLE "host_users" DROP CONSTRAINT "host_users_unique_token";