# --- !Ups

CREATE TABLE "projects" (
  "id" SERIAL PRIMARY KEY,
  "host_id" INT NOT NULL,
  "host_project_id" INT NOT NULL,
  "host_user_admin_id" INT NOT NULL,
  "namespace" VARCHAR NOT NULL,
  "name" VARCHAR NOT NULL,
  "web_url" VARCHAR NOT NULL,
  "is_public" BOOLEAN DEFAULT FALSE,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "projects" ADD CONSTRAINT "projects_fk_host_id" FOREIGN KEY("host_id") REFERENCES "hosts"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "projects" DROP CONSTRAINT "projects_fk_host_id";

DROP TABLE "projects";
