# --- !Ups

CREATE TABLE "suggestions" (
  "id" SERIAL PRIMARY KEY,
  "iid" INT NOT NULL,
  "project_id" INT NOT NULL,
  "cont_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "user_rep" DECIMAL NOT NULL,
  "importance" DECIMAL NOT NULL,
  "title" VARCHAR NOT NULL,
  "description" VARCHAR DEFAULT NULL,
  "commit_when_made" VARCHAR DEFAULT NULL,
  "commit_when_addressed" VARCHAR DEFAULT NULL,
  "status" VARCHAR NOT NULL DEFAULT 'SUGGESTED',
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "suggestions" ADD CONSTRAINT "suggestions_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "suggestions" ADD CONSTRAINT "suggestions_fk_cont_id" FOREIGN KEY("cont_id") REFERENCES "contributions"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "suggestions" ADD CONSTRAINT "suggestions_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "suggestions" DROP CONSTRAINT "suggestions_fk_project_id";
ALTER TABLE "suggestions" DROP CONSTRAINT "suggestions_fk_cont_id";
ALTER TABLE "suggestions" DROP CONSTRAINT "suggestions_fk_user_id";

DROP TABLE "suggestions";
