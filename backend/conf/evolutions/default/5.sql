# --- !Ups

CREATE TABLE "tasks" (
  "id" SERIAL PRIMARY KEY,
  "iid" INT NOT NULL,
  "parent_id" INT DEFAULT NULL,
  "project_id" INT NOT NULL,
  "owner_id" INT NOT NULL,
  "branch" VARCHAR,
  "commit" VARCHAR,
  "closed" BOOLEAN NOT NULL DEFAULT FALSE,
  "title" VARCHAR NOT NULL,
  "description" VARCHAR,
  "auto_rebase" BOOLEAN NOT NULL DEFAULT TRUE,
  "ongoing" BOOLEAN NOT NULL DEFAULT FALSE,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk_parent_id" FOREIGN KEY("parent_id") REFERENCES "tasks"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk_owner_id" FOREIGN KEY("owner_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "tasks" DROP CONSTRAINT "tasks_fk_parent_id";
ALTER TABLE "tasks" DROP CONSTRAINT "tasks_fk_project_id";
ALTER TABLE "tasks" DROP CONSTRAINT "tasks_fk_owner_id";

DROP TABLE "tasks";
