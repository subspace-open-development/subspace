# --- !Ups

CREATE TABLE "project_reputation_changes" (
  "cont_id" INT,
  "project_id" INT,
  "rep_old" DECIMAL NOT NULL,
  "rep_change" DECIMAL NOT NULL,
  "rep_new" DECIMAL NOT NULL,
  "points" DECIMAL NOT NULL,
  "cont_outcome" VARCHAR NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY("cont_id", "project_id")
);

ALTER TABLE "project_reputation_changes" ADD CONSTRAINT "project_reputation_changes_fk_cont_id" FOREIGN KEY("cont_id") REFERENCES "contributions"("id") on update NO ACTION on delete NO ACTION;
ALTER TABLE "project_reputation_changes" ADD CONSTRAINT "project_reputation_changes_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "project_reputation_changes" DROP CONSTRAINT "project_reputation_changes_fk_cont_id";
ALTER TABLE "project_reputation_changes" DROP CONSTRAINT "project_reputation_changes_fk_project_id";

DROP TABLE "project_reputation_changes";