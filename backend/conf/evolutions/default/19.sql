# --- !Ups

CREATE TABLE "user_mana" (
  "user_id" INT PRIMARY KEY,
  "mana" DECIMAL NOT NULL DEFAULT 0,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE "user_mana" ADD CONSTRAINT "user_mana_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "user_mana" DROP CONSTRAINT "user_mana_fk_user_id";
DROP TABLE "user_mana";
