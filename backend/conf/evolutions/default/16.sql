# --- !Ups
ALTER TABLE "tasks" ADD COLUMN no_direct_contributions BOOLEAN DEFAULT FALSE;
ALTER TABLE "tasks" ADD COLUMN auto_merge_up BOOLEAN DEFAULT TRUE;
ALTER TABLE "tasks" ADD COLUMN require_owner_approval BOOLEAN DEFAULT FALSE;

# --- !Downs
ALTER TABLE "tasks" DROP COLUMN no_direct_contributions;
ALTER TABLE "tasks" DROP COLUMN auto_merge_up;
ALTER TABLE "tasks" DROP COLUMN require_owner_approval;