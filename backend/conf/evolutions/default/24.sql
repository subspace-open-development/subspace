# --- !Ups

ALTER TABLE "contributions" ADD COLUMN state_change_at TIMESTAMP;
ALTER TABLE "contributions" ADD COLUMN state_future VARCHAR;

# --- !Downs

ALTER TABLE "contributions" DROP COLUMN state_future;
ALTER TABLE "contributions" DROP COLUMN state_change_at;