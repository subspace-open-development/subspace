# --- !Ups

CREATE TABLE "suggestion_comments" (
  "id" SERIAL PRIMARY KEY,
  "parent_id" INT DEFAULT NULL,
  "suggestion_id" INT NOT NULL,
  "author_id" INT NOT NULL,
  "comment" VARCHAR NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "suggestion_comments" ADD CONSTRAINT "suggestion_comments_users_fk_author_id" FOREIGN KEY("author_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;
ALTER TABLE "suggestion_comments" ADD CONSTRAINT "suggestion_fk_suggestion_id" FOREIGN KEY("suggestion_id") REFERENCES "suggestions"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "suggestion_comments" ADD CONSTRAINT "suggestion_comments_fk_parent_id" FOREIGN KEY("parent_id") REFERENCES "suggestion_comments"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "suggestion_comments" DROP CONSTRAINT "suggestion_comments_fk_parent_id";
ALTER TABLE "suggestion_comments" DROP CONSTRAINT "suggestion_fk_suggestion_id";
ALTER TABLE "suggestion_comments" DROP CONSTRAINT "suggestion_comments_users_fk_author_id";

DROP TABLE "suggestion_comments";
