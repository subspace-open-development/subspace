# --- !Ups

CREATE TABLE "join_requests" (
  "user_id" INT NOT NULL,
  "project_id" INT NOT NULL,
  "status" VARCHAR NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY("user_id", "project_id")
);

ALTER TABLE "join_requests" ADD CONSTRAINT "join_requests_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "join_requests" ADD CONSTRAINT "join_requests_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "join_requests" DROP CONSTRAINT "join_requests_fk_project_id";
ALTER TABLE "join_requests" DROP CONSTRAINT "join_requests_fk_user_id";

DROP TABLE "join_requests";