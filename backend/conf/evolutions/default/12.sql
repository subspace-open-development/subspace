# --- !Ups

CREATE TABLE "groups" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR NOT NULL,
  "owner_id" INT,
  "disabled" BOOLEAN NOT NULL DEFAULT FALSE,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "groups" ADD CONSTRAINT "groups_fk_owner_id" FOREIGN KEY("owner_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "groups" DROP CONSTRAINT "groups_fk_owner_id";

DROP TABLE "groups";
