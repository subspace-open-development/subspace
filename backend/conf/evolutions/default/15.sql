# --- !Ups

CREATE TABLE "project_users" (
  "project_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY ("project_id", "user_id")
);

ALTER TABLE "project_users" ADD CONSTRAINT "project_users_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "project_users" ADD CONSTRAINT "project_users_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "project_users" DROP CONSTRAINT "project_users_fk_project_id";
ALTER TABLE "project_users" DROP CONSTRAINT "project_users_fk_user_id";

DROP TABLE "project_users";
