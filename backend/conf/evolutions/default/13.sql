# --- !Ups

CREATE TABLE "group_users" (
  "group_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY ("group_id", "user_id")
);

ALTER TABLE "group_users" ADD CONSTRAINT "group_users_fk_group_id" FOREIGN KEY("group_id") REFERENCES "groups"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "group_users" ADD CONSTRAINT "group_users_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "group_users" DROP CONSTRAINT "group_users_fk_group_id";
ALTER TABLE "group_users" DROP CONSTRAINT "group_users_fk_user_id";

DROP TABLE "group_users";
