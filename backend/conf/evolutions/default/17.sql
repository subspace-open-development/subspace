# --- !Ups
ALTER TABLE "suggestions" RENAME COLUMN "importance" TO "mana_used";
ALTER TABLE "backs" RENAME COLUMN "value" TO "mana_used";

# --- !Downs
ALTER TABLE "suggestions" RENAME COLUMN "mana_used" TO "importance";
ALTER TABLE "backs" RENAME COLUMN "mana_used" TO "value";