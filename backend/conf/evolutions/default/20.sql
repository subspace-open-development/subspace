# --- !Ups

ALTER TABLE "contributions" ADD COLUMN state VARCHAR NOT NULL DEFAULT 'OPEN';

ALTER TABLE "user_reputation_changes" DROP COLUMN user_review;

# --- !Downs

ALTER TABLE "contributions" DROP COLUMN state;

ALTER TABLE "user_reputation_changes" ADD COLUMN user_review VARCHAR;