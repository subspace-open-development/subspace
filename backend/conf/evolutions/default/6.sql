# --- !Ups

CREATE TABLE "contributions" (
  "id" SERIAL PRIMARY KEY,
  "iid" INT NOT NULL,
  "project_id" INT NOT NULL,
  "task_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "branch" VARCHAR,
  "commit" VARCHAR,
  "closed" BOOLEAN NOT NULL DEFAULT FALSE,
  "title" VARCHAR NOT NULL,
  "description" VARCHAR,
  "auto_update_with_commits" BOOLEAN NOT NULL DEFAULT TRUE,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "contributions" ADD CONSTRAINT "contributions_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "contributions" ADD CONSTRAINT "contributions_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete NO ACTION;
ALTER TABLE "contributions" ADD CONSTRAINT "contributions_fk_task_id" FOREIGN KEY("task_id") REFERENCES "tasks"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "contributions" DROP CONSTRAINT "contributions_fk_project_id";
ALTER TABLE "contributions" DROP CONSTRAINT "contributions_fk_user_id";
ALTER TABLE "contributions" DROP CONSTRAINT "contributions_fk_task_id";

DROP TABLE "contributions";
