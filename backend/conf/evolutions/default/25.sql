# --- !Ups

ALTER TABLE "reputation_changes" DROP COLUMN rep_old;
ALTER TABLE "reputation_changes" DROP COLUMN rep_new;

# --- !Downs

ALTER TABLE "reputation_changes" ADD COLUMN rep_old DECIMAL;
ALTER TABLE "reputation_changes" ADD COLUMN rep_new DECIMAL;