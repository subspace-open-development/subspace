# --- !Ups

CREATE TABLE "reputation_changes" (
  "id" SERIAL PRIMARY KEY,
  "is_user" BOOLEAN NOT NULL,
  "is_project" BOOLEAN NOT NULL,
  "project_id" INT NOT NULL,
  "contribution_id" INT NOT NULL,
  "suggestion_id" INT,
  "user_id" INT,
  "rep_old" DECIMAL NOT NULL,
  "rep_change" DECIMAL NOT NULL,
  "rep_new" DECIMAL NOT NULL,
  "points" DECIMAL NOT NULL,
  "change_type" VARCHAR NOT NULL,
  "contribution_outcome" VARCHAR NOT NULL,
  "suggestion_outcome" VARCHAR,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "reputation_changes" ADD CONSTRAINT "reputation_changes_fk_contribution_id" FOREIGN KEY("contribution_id") REFERENCES "contributions"("id") on update NO ACTION on delete NO ACTION;
ALTER TABLE "reputation_changes" ADD CONSTRAINT "reputation_changes_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;
ALTER TABLE "reputation_changes" ADD CONSTRAINT "reputation_changes_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "reputation_changes" DROP CONSTRAINT "reputation_changes_fk_contribution_id";
ALTER TABLE "reputation_changes" DROP CONSTRAINT "reputation_changes_fk_user_id";
ALTER TABLE "reputation_changes" DROP CONSTRAINT "reputation_changes_fk_project_id";

DROP TABLE "reputation_changes";