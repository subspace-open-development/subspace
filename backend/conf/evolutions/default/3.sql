# --- !Ups

CREATE TABLE "host_users" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INT NOT NULL,
  "host_id" INT NOT NULL,
  "host_user_id" INT NOT NULL,
  "host_user_email" VARCHAR NOT NULL,
  "host_user_username" VARCHAR NOT NULL,
  "host_user_token" VARCHAR DEFAULT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "active_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "host_users" ADD CONSTRAINT "host_users_fk_host" FOREIGN KEY("host_id") REFERENCES "hosts"("id") on update NO ACTION on delete NO ACTION;
ALTER TABLE "host_users" ADD CONSTRAINT "host_users_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "host_users" DROP CONSTRAINT "host_users_fk_host";
ALTER TABLE "host_users" DROP CONSTRAINT "host_users_fk_user_id";

DROP TABLE "host_users";
