# --- !Ups

CREATE TABLE "hosts" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR NOT NULL
);

INSERT INTO "hosts" (name) VALUES ('GITLAB');
INSERT INTO "hosts" (name) VALUES ('GITHUB');
INSERT INTO "hosts" (name) VALUES ('BITBUCKET');

# --- !Downs

DROP TABLE "hosts";
