# --- !Ups

CREATE TABLE "user_reputations" (
  "user_id" INT PRIMARY KEY,
  "reputation" DECIMAL
);
ALTER TABLE "user_reputations" ADD CONSTRAINT "user_reputations_fk_user_id" FOREIGN KEY("user_id") REFERENCES "users"("id") on update NO ACTION on delete CASCADE;

CREATE TABLE "project_reputations" (
  "project_id" INT PRIMARY KEY,
  "reputation" DECIMAL
);
ALTER TABLE "project_reputations" ADD CONSTRAINT "project_reputations_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete CASCADE;

# --- !Downs

ALTER TABLE "user_reputations" DROP CONSTRAINT "user_reputations_fk_user_id";
DROP TABLE "user_reputations";

ALTER TABLE "project_reputations" DROP CONSTRAINT "project_reputations_fk_project_id";
DROP TABLE "project_reputations";