# --- !Ups

CREATE TABLE "project_settings" (
  "project_id" INT PRIMARY KEY,
  "support_proportion_required" DECIMAL NOT NULL,
  "threshold_halflife_seconds" INT NOT NULL,
  "seconds_from_commit_until_decline" INT NOT NULL,
  "seconds_from_start_until_decline" INT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE "project_settings" ADD CONSTRAINT "project_settings_fk_project_id" FOREIGN KEY("project_id") REFERENCES "projects"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

ALTER TABLE "project_settings" DROP CONSTRAINT "project_settings_fk_project_id";

DROP TABLE "project_settings";