name := "backend"
 
version := "1.0" 
      
lazy val `backend` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"
resolvers += Resolver.jcenterRepo
      
scalaVersion := "2.12.10"

val playVersion = "2.7.3"
val slickVersion = "4.0.0"
val silhouetteVersion = "5.0.4"

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-guice" % playVersion,
  "com.typesafe.play" %% "play-slick" % slickVersion,
  "com.typesafe.play" %% "play-slick-evolutions" % slickVersion,

  "com.typesafe.akka" %% "akka-cluster-tools" % "2.5.26",

  "org.typelevel" %% "cats-core" % "1.1.0",
  "com.pauldijou" %% "jwt-play" % "0.16.0",

  "com.mohiva" %% "play-silhouette" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-persistence" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-password-bcrypt" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-testkit" % silhouetteVersion % Test,

  "org.postgresql" % "postgresql" % "42.2.5",
  "com.h2database" % "h2" % "1.4.197",

  "org.gitlab4j" % "gitlab4j-api" % "4.12.4" exclude("javax.ws.rs", "javax.ws.rs-api"),
  "javax.ws.rs" % "javax.ws.rs-api" % "2.1" artifacts Artifact("javax.ws.rs-api", "jar", "jar"),

  "org.eclipse.jgit" % "org.eclipse.jgit" % "5.3.1.201904271842-r",

  "joda-time" % "joda-time" % "2.10.5",

  cacheApi,
  ehcache,
  ws,
  guice,

  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  specs2 % Test
)