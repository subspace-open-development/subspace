# variables.tf

variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-east-1"
}

variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "myEcsTaskExecutionRole"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

# ======== Backend ========

variable "backend_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "877899260343.dkr.ecr.us-east-1.amazonaws.com/subspace-prod-backend:latest"
}

variable "backend_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 9000
}

variable "backend_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "backend_health_check_path" {
  default = "/"
}

variable "backend_fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "backend_fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "4096"
}

# ======== Frontend ========

variable "frontend_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "877899260343.dkr.ecr.us-east-1.amazonaws.com/subspace-prod-frontend:latest"
}

variable "frontend_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 3000
}

variable "frontend_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "frontend_health_check_path" {
  default = "/login"
}

variable "frontend_fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "512"
}

variable "frontend_fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}
