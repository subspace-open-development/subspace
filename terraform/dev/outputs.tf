# outputs.tf

output "backend_alb_hostname" {
  value = aws_alb.backend.dns_name
}

output "backend_route53_domain" {
  value = aws_route53_record.backend.name
}


output "frontend_alb_hostname" {
  value = aws_alb.frontend.dns_name
}

output "frontend_route53_domain" {
  value = aws_route53_record.frontend.name
}


output "database_endpoint" {
  value = aws_db_instance.subspace.endpoint
}
