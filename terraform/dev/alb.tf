# alb.tf

# ======== Backend ========

resource "aws_alb" "backend" {
  name            = "subspace-backend-load-balancer"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.backend_lb.id]
}

resource "aws_alb_target_group" "backend" {
  name        = "backend-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.backend_health_check_path
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "backend_https" {
  load_balancer_arn = aws_alb.backend.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-1:877899260343:certificate/e10c7671-7839-48e9-999b-978742c5b8aa"

  default_action {
    target_group_arn = aws_alb_target_group.backend.id
    type             = "forward"
  }
}

resource "aws_alb_listener" "backend_http" {
  load_balancer_arn = aws_alb.backend.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# ======== Frontend ========

resource "aws_alb" "frontend" {
  name            = "subspace-frontend-load-balancer"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.frontend_lb.id]
}

resource "aws_alb_target_group" "frontend" {
  name        = "frontend-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.frontend_health_check_path
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "frontend_https" {
  load_balancer_arn = aws_alb.frontend.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-1:877899260343:certificate/e10c7671-7839-48e9-999b-978742c5b8aa"

  default_action {
    target_group_arn = aws_alb_target_group.frontend.id
    type             = "forward"
  }
}

resource "aws_alb_listener" "frontend_http" {
  load_balancer_arn = aws_alb.frontend.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
