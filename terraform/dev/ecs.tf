# ecs.tf

resource "aws_ecs_cluster" "main" {
  name = "subspace-cluster"
}

# ======== Backend ========

data "template_file" "backend" {
  template = file("./templates/ecs/backend.json.tpl")

  vars = {
    backend_image           = var.backend_image
    backend_port            = var.backend_port
    backend_fargate_cpu     = var.backend_fargate_cpu
    backend_fargate_memory  = var.backend_fargate_memory
    aws_region              = var.aws_region
  }
}

resource "aws_ecs_task_definition" "backend" {
  family                   = "subspace-backend-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.backend_fargate_cpu
  memory                   = var.backend_fargate_memory
  container_definitions    = data.template_file.backend.rendered
}

resource "aws_ecs_service" "backend" {
  name            = "subspace-backend-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.backend.arn
  desired_count   = var.backend_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.backend_ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.backend.id
    container_name   = "subspace-backend"
    container_port   = var.backend_port
  }

  depends_on = [aws_alb_listener.backend_http, aws_alb_listener.backend_https, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

# ======== Frontend ========

data "template_file" "frontend" {
  template = file("./templates/ecs/frontend.json.tpl")

  vars = {
    frontend_image          = var.frontend_image
    frontend_port           = var.frontend_port
    frontend_fargate_cpu    = var.frontend_fargate_cpu
    frontend_fargate_memory = var.frontend_fargate_memory
    aws_region              = var.aws_region
  }
}

resource "aws_ecs_task_definition" "frontend" {
  family                   = "subspace-frontend-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.frontend_fargate_cpu
  memory                   = var.frontend_fargate_memory
  container_definitions    = data.template_file.frontend.rendered
}

resource "aws_ecs_service" "frontend" {
  name            = "subspace-frontend-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.frontend.arn
  desired_count   = var.frontend_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.frontend_ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.frontend.id
    container_name   = "subspace-frontend"
    container_port   = var.frontend_port
  }

  depends_on = [aws_alb_listener.frontend_http, aws_alb_listener.frontend_https, aws_iam_role_policy_attachment.ecs_task_execution_role]
}
