[
  {
    "name": "subspace-backend",
    "image": "${backend_image}",
    "cpu": ${backend_fargate_cpu},
    "memory": ${backend_fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/subspace-backend",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${backend_port},
        "hostPort": ${backend_port}
      }
    ]
  }
]
