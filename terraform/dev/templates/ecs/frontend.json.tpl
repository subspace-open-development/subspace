[
  {
    "name": "subspace-frontend",
    "image": "${frontend_image}",
    "cpu": ${frontend_fargate_cpu},
    "memory": ${frontend_fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/subspace-frontend",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${frontend_port},
        "hostPort": ${frontend_port}
      }
    ]
  }
]
