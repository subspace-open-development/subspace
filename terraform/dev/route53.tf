# route53.tf

# ======== Backend ========

resource "aws_route53_record" "backend" {
  zone_id = "Z27A1H46A378X2" # Replace with your zone ID
  name    = "api.dev.subspace.net" # Replace with your name/domain/subdomain
  type    = "A"

  alias {
    name                   = aws_alb.backend.dns_name
    zone_id                = aws_alb.backend.zone_id
    evaluate_target_health = true
  }
}

# ======== Frontend ========

resource "aws_route53_record" "frontend" {
  zone_id = "Z27A1H46A378X2" # Replace with your zone ID
  name    = "app.dev.subspace.net" # Replace with your name/domain/subdomain
  type    = "A"

  alias {
    name                   = aws_alb.frontend.dns_name
    zone_id                = aws_alb.frontend.zone_id
    evaluate_target_health = true
  }
}
