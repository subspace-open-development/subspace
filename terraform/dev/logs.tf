# logs.tf

# ======== Backend ========

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "subspace_backend_log_group" {
  name              = "/ecs/subspace-backend"
  retention_in_days = 30

  tags = {
    Name = "subspace-backend-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "subspace_backend_log_stream" {
  name           = "subspace-backend-stream"
  log_group_name = aws_cloudwatch_log_group.subspace_backend_log_group.name
}

# ======== Frontend ========

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "subspace_frontend_log_group" {
  name              = "/ecs/subspace-frontend"
  retention_in_days = 30

  tags = {
    Name = "subspace-frontend-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "subspace_frontend_log_stream" {
  name           = "subspace-frontend-stream"
  log_group_name = aws_cloudwatch_log_group.subspace_frontend_log_group.name
}
