# rds.tf

resource "aws_security_group" "db_security_group" {
  vpc_id = aws_vpc.main.id
  ingress {
    protocol = "tcp"
    from_port = 5432
    to_port = 5432
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_subnet_group" "default" {
  name       = "main-subnet-grp"
  subnet_ids = [
    aws_subnet.private[0].id,
    aws_subnet.private[1].id,
    aws_subnet.public[0].id,
    aws_subnet.public[1].id,
  ]
}

resource "aws_db_instance" "subspace" {
  allocated_storage    = 100
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "10.10"
  instance_class       = "db.t3.xlarge"
  identifier           = "subspace-db"
  name                 = "play"
  username             = "subspace"
  password             = "subspace!2020"
  parameter_group_name = "default.postgres10"

  vpc_security_group_ids = [
    aws_security_group.db_security_group.id,
  ]

  db_subnet_group_name = aws_db_subnet_group.default.name
}
